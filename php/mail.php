<?php
/**
 * Mail a photo
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

use conf\conf;
use template\template;
use web\url;

require_once "include.inc.php";

$title = translate("E-Mail Photo");

$photo_id = getvar("photo_id");
$html = getvar("html");
$to_name = getvar("to_name");
$to_email = getvar("to_email");
$from_name = getvar("from_name");
$from_email = getvar("from_email");
$subject = getvar("subject");
$message = getvar("message");
$includeurl = getvar("includeurl");

$photo = new photo($photo_id);

$found = $photo->lookup();

if (!$found) {
    $msg = sprintf(translate("Could not find photo id %s."), $photo_id);
}
else {

    if ($_action == "mail") {
        try {
            $mail = new mailMime();
            $hdrs = array (
                "X-Mailer" => "Html Mime Mail Class",
                "X-Zoph-Version" => VERSION
            );
            $headers="";

            $size = getvar("_size");

            if ($size == "full") {
                $filename = $photo->get("name");
                $dir = conf::get("path.images") . "/" . $photo->get("path") . "/";
            } else {
                $filename = MID_PREFIX . "_" . $photo->get("name");
                $dir = conf::get("path.images") . "/" . $photo->get("path") . "/" .
                    MID_PREFIX . "/";
            }
            $file=new file($dir . DIRECTORY_SEPARATOR . $filename);
            if ($html) {
                $html = "<center>\n";
                $html .= "<img src=\"" . $filename . "\"><br>\n";
                $html .= str_replace("\n", "<br>\n", $message);
                if ($includeurl) {
                    $html .= "<a href=\"" . url::get() .
                        "photo.php?photo_id=" . $photo_id . "\">";
                    $html .= sprintf(translate("See this photo in %s"),
                        conf::get("interface.title"));
                    $html .= "</a>";
                }
                $html .= "</center>\n";

                $mail->addHTMLImageFromFile($dir . "/" . $filename, $file->getMime());
                $mail->setHTMLBody($html);
                $mail->setTXTBody($message);
            } else {
                if ($includeurl) {
                    $message .= "\n";
                    $message .= sprintf(translate("See this photo in %s"),
                        conf::get("interface.title"));
                    $message .= ": " . url::get() . "/photo.php?photo_id=" . $photo_id;
                }
                $mail->setTXTBody($message);
                $mail->addAttachmentFromFile($dir . "/" . $filename, $file->getMime());
            }
            $mail->setFrom("$from_name <$from_email>");

            if (strlen(conf::get("feature.mail.bcc")) > 0) {
                $mail->addBcc(conf::get("feature.mail.bcc"));
            }
            $body = $mail->get();
            $hdrs = $mail->headers($hdrs);
            foreach ($hdrs as $header => $content) {
                $headers .= $header . ": " . $content . "\n";
            }
            if (mail($to_email,$subject, $body,$headers)) {
                $msg = translate("Your mail has been sent.");
            } else {
                $msg = translate("Could not send mail.");
            }
        } catch (mailException $e) {
            $msg = $e->getMessage();
        }
    }

}

$from_name = $user->person->getName();
$from_email = $user->person->getEmail();

require_once "header.inc.php";
?>

      <h1>
<?php echo translate("email photo") ?>
      </h1>
<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post">
  <div class="main">
<?php
if (isset($msg)) {
    echo $msg;
}

if ($found && $_action == "compose") {
    $body="";

    $subject = sprintf(translate("A Photo from %s"), conf::get("interface.title")) . ": ";
    $subject.= $photo->get("name");
    $ea = $photo->getEmailArray();

    if ($ea) {
        foreach ($ea as $name => $value) {
            if ($name && $value) {
                $body .= "$name: $value\r\n";
            }
        }
    }
    ?>
    <input type="hidden" name="_action" value="mail">
    <input type="hidden" name="photo_id" value="<?php echo $photo_id ?>">
    <label for="html"><?php echo translate("send as html") ?></label>
    <?php echo template::createYesNoPulldown("html", "1") ?><br>
    <?php echo template::createInput("to_name", $to_name, 32, translate("to (name)"), 24) ?><br>
    <?php echo template::createInputEmail("to_email", $to_email, translate("to (email)")) ?><br>
    <?php echo template::createInput("from_name", $from_name, 32, translate("from (your name)"), 24) ?><br>
    <?php echo template::createInputEmail("from_email", $from_email, translate("from (your email)")) ?><br>
    <?php echo template::createInput("subject", $subject, 64, translate("subject"), 40) ?><br>
    <label for="size"><?php echo translate("send fullsize") ?></label>
    <?php echo template::createPulldown("_size", "mid", array(
        "full" => translate("Yes",0),
        "mid" => translate("No",0))); ?>
    <br>
    <label for="includeurl"><?php echo translate("include URL") ?></label>
    <?php echo template::createYesNoPulldown("includeurl", "1") ?><br>
    <label for="message"><?php echo translate("message:") ?></label><br>
    <textarea name="message" class="email" cols="70" rows="5"><?php echo $body ?></textarea>
    <?php echo $photo->getImageTag(MID_PREFIX) ?>
    <input type="submit" name="_button" value="<?php echo translate("email", 0); ?>">
    <?php
}
?>
  </div>
</form>

<?php
require_once "footer.inc.php";
?>
