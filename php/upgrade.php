<?php
/**
 * Zoph upgrade page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

use conf\conf;
use upgrade\controller;
use web\request;
use web\redirect;

require_once "include.inc.php";
$title = translate("Zoph Upgrade");

if (!$user->isAdmin()) {
    redirect::redirect("zoph.php", "Admin user required");
}

$ctrl = new controller(request::create());
$view = $ctrl->getView();

$headers = $view->getHeaders();
if (is_array($headers)) {
    foreach ($headers as $header) {
        header($header);
    }
    echo $view->view();
} else {
    $title = $view->getTitle();
    require_once("header.inc.php");
    echo $view->view();
    require_once("footer.inc.php");
}
