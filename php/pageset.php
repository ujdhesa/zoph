<?php
/**
 * Show and modify page sets
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 *
 */
require_once "include.inc.php";

use template\template;
use template\block;

$pageset_id = getvar("pageset_id");
$page_id = getvar("page_id");
$pageset = new pageset($pageset_id);
if ($pageset_id) {
    $pageset->lookup();
}
if (!is_null($page_id)) {
    $page = new page($page_id);
}

if (!$user->isAdmin()) {
    $_action="display";
}

if ($_action == "insert") {
    $pageset->set("user", $user->get("user_id"));
} else if ($_action == "moveup") {
    $pageset->moveUp($page);
} else if ($_action == "movedown") {
    $pageset->moveDown($page);
} else if ($_action == "delpage") {
    $pageset->removePage($page);
    $action="display";
} else if ($_action == "addpage") {
    $pageset->addPage($page);
    $action="display";
}
$obj = &$pageset;
$redirect = "pagesets.php";

require_once "actions.inc.php";

if ($_action != "new") {
    $title = $pageset->get("title");
    $returnURL = "page.php?page_id=" . $pageset->getId();
} else {
    $title = translate("Create new pageset");
    $returnURL = "pagesets.php";
}
require_once "header.inc.php";
?>
<?php
if ($action == "confirm") {
    ?>
      <h1><?php echo translate("delete pageset") ?></h1>
        <div class="main">
           <ul class="actionlink">
             <li><a href="pageset.php?_action=confirm&amp;pageset_id=<?php
                echo $pageset->getId() ?>"><?php echo translate("delete") ?>
             </a></li>
             <li><a href="pageset.php?pageset_id=<?php
                echo $pageset->getId() ?>"><?php echo translate("cancel") ?>
             </a></li>
           </ul>
           <?php echo translate("Confirm deletion of this pageset"); ?>
         </div>
    <?php
} else if ($action == "display") {
    ?>
      <h1>
        <ul class="actionlink">
          <li><a href="pageset.php?_action=edit&amp;pageset_id=<?php
            echo $pageset->getId() ?>">
            <?php echo translate("edit") ?>
          </a></li>
          <li><a href="pageset.php?_action=delete&amp;pageset_id=<?php
            echo $pageset->getId() ?>">
            <?php echo translate("delete") ?>
          </a></li>
        </ul>
        <?php echo $title; ?>
      </h1>
      <div class="main">
        <br>
            <?php
            $pageset->lookup();
            echo new block("definitionlist", array(
                "class" => "display pageset",
                "dl"     => $pageset->getDisplayArray()));
            ?>
        <br>
        <h2>
          <?php echo translate("Pages in this pageset"); ?>
        </h2>
    <?php echo page::getTable($pageset->getPages(), $pageset); ?>
        <form action="pageset.php" class="addpage">
          <input type="hidden" name="_action" value="addpage">
          <input type="hidden" name="pageset_id" value="<?php echo $pageset->get("pageset_id") ?>">
          <label for="page_id">
            <?php echo translate("Add a page:") ?>
          </label>
          <?php echo template::createPulldown("page_id", 0, template::createSelectArray(page::getRecords("title"), array("title"), true), true); ?>
          <input type="submit" name="_button" value="<?php echo translate("add",0)?>">
        </form>
        <br>
      </div>
    <?php
} else {
    ?>
    <h1>
        <ul class="actionlink">
            <li>
                <a href="<?= $returnURL ?>">
                    <?php echo translate("return") ?>
                </a>
            </li>
            <li>
                <a href="pageset.php?_action=delete&amp;pageset_id=<?= $pageset->getId() ?>">
                    <?php echo translate("delete") ?>
                </a>
            </li>
        </ul>
        <?php echo $title ?>
    </h1>
    <div class="main">
        <br>
        <form action="pageset.php">
            <input type="hidden" name="_action" value="<?php echo $action ?>">
            <input type="hidden" name="pageset_id" value="<?php
                echo $pageset->get("pageset_id") ?>">
            <?php echo template::createInput("title", $pageset->get("title"), 32, translate("title")) ?><br>
            <label for="show_orig"><?php echo translate("show original page") ?></label>
            <?php echo $pageset->getOriginalDropdown(); ?><br>
            <label for="orig_pos"><?php echo translate("position of original") ?></label>
            <?php echo template::createPulldown("orig_pos",
                $pageset->get("orig_pos"),
                array("top" => translate("Top",0), "bottom" => translate("Bottom",0))) ?><br>
            <input type="submit" value="<?php echo translate($action, 0) ?>">
        </form>
    </div>

    <?php
}
require_once "footer.inc.php";
?>
