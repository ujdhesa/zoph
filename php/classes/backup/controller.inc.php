<?php
/**
 * Controller for backup
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace backup;

use db\backup;
use generic\controller as genericController;
use web\request;


/**
 * Controller for backup
 */
class controller extends genericController {

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display", "backup"
    );

    /** @var string Where to redirect after actions */
    public $redirect="zoph.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        $this->doAction();
    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $this->view=new view\display($this->request);
    }

    /**
     * Do action 'backup'
     * Create a backup
     */
    public function actionBackup() {
        try {
            $backup = new backup($this->request["rootpwd"] ?? null);
            $this->view=new view\backup($this->request, $backup->execute());
        } catch (\Exception $e) {
            $this->view=new view\error($this->request, $e->getMessage());
        }
    }
}
