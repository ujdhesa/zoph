<?php
/**
 * View to start a backup
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace backup\view;

use conf\conf;
use template\block;
use template\form;
use template\template;
use web\request;

/**
 * Display screen to start backup
 */
class display {

    /** @var request hold the request for this view */
    private $request;

    /**
     * Create view
     * @param request web request
     */
    public function __construct(request $request) {
        $this->request=$request;
    }

    /**
     * Get headers - always null, so default headers will be used
     * @return null
     */
    public function getHeaders() {
        return null;
    }

    /**
     * Get view
     * @return template view
     */
    public function view() {
        if (empty($this->request->getServerVar("HTTPS"))) {
            $warning = new block("message", array(
                "class"  => "warning",
                "title" => translate("Warning! Unencrypted connection!"),
                "text"  => translate("You are connected via http, it is not recommended to send your root password over a non-encrypted connection!")
            ));
        }
        $form=new form("form", array(
            "class"         => "backup",
            "warning"       => $warning ?? "",
            "formAction"    => "backup.php",
            "onsubmit"      => null,
            "action"        => "backup",
            "submit"        => translate("start backup"),
        ));

        $form->addInputPassword("rootpwd", translate("root password"), translate("leave empty to perform backup with normal Zoph database user"), 32, 16);


        $tpl=new template("main-nh", array(
            "title"         => $this->getTitle(),
        ));

        $tpl->addBlocks(array($form));
        if (isset($this->request["_return"])) {
            $tpl->addActionlinks(array(
                translate("return")    => $this->request["_return"]
            ));
        }

        return $tpl;
    }

    /**
     * Get title
     * @return string title
     */
    public function getTitle() {
        return translate("Create backup");
    }
}
