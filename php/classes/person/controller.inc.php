<?php
/**
 * Controller for person
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person;

use conf\conf;
use generic\controller as genericController;
use web\request;
use person;
use user;

use personNotAccessibleSecurityException;

/**
 * Controller for person
 */
class controller extends genericController {

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "edit", "insert",
        "new", "update"
    );

    /** @var string Where to redirect after actions */
    public $redirect="person.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        if ($request->_action=="new") {
            $this->setObject(new person());
            $this->doAction();
        } else {
            try {
                $person=$this->getPersonFromRequest();
            } catch (personNotAccessibleSecurityException $e) {
                log::msg($e->getMessage(), log::WARN, log::SECURITY);
                $person=null;
            }

            if ($person instanceof person) {
                $this->setObject($person);
                $this->doAction();
            } else {
                $this->view = "notfound";
            }
        }
    }

    /**
     * Get the person based on the query in the request
     * @throws personNotAccessibleSecurityException
     */
    private function getPersonFromRequest() {
        $user=user::getCurrent();
        if (isset($this->request["name"])) {
            $people = person::getByName($this->request["name"]);
            if ($people && count($people) == 1) {
                $person = array_shift($people);
            }
        } else if (isset($this->request["person_id"])) {
            $person = new person($this->request["person_id"]);
            $person->lookup();
        }
        if ($user->isAdmin() || $person->isVisible()) {
            return $person;
        }
        throw new photoNotAccessibleSecurityException(
            "Security Exception: person " . $person->getId() .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            parent::actionConfirm();
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            parent::actionDelete();
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            $this->view="update";
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($user->canEditOrganizers()) {
            parent::actionUpdate();
        }
        $this->view="display";
    }
}
