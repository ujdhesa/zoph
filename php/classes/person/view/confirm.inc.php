<?php
/**
 * View for confirm delete person
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person\view;

use template\template;
use web\request;
use person;
use photo;
use user;

/**
 * This view displays the "confirm person" page
 */
class confirm {

    /** * @var array request variables */
    private $vars;
    /** * @var request web request */
    private $request;
    /** * @var person */
    private $person;

    /**
     * Create view
     * @param request web request
     * @param person person to operate on
     */
    public function __construct(request $request, person $person) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->person=$person;
    }

    /**
     * Output view
     */
    public function view() {
        $cover = $this->person->getCoverphoto();
        if ($cover instanceof photo) {
            $cover = $cover->getImageTag(THUMB_PREFIX);
        }
        $actionlinks=array(
            translate("confirm")   =>  "person.php?_action=confirm&amp;person_id=" . $this->person->getId(),
            translate("cancel")    =>  "person.php?person_id=" . $this->person->getId()
        );
        return new template("confirm", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $actionlinks,
            "mainActionlinks" => null,
            "obj"           => $this->person,
            "image"         => $cover
        ));

    }

    public function getTitle() {
        return sprintf(translate("Confirm deletion of '%s'"), $this->person->getName());
    }
}
