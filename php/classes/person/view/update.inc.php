<?php
/**
 * View for edit photo page // WIP
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person\view;

use conf\conf;
use geo\map;
use template\template;
use web\request;

use album;
use category;
use person;
use photographer;
use place;
use user;

/**
 * This view displays the person page when editing
 */
class update extends common {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() {
        $actionlinks = array();
        $actionlinks[translate("return")] = "people.php";
        if ($this->request["_action"] != "new") {
            $actionlinks[translate("new")] = "person.php?_action=new";
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() {
        $user = user::getCurrent();

        if ($this->request["_action"] == "new") {
            $action = "insert";
        } else if ($this->request["_action"] == "edit") {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = $this->request["_action"];
        }

        $tpl = new template("editPerson", array(
            "actionlinks"   => $this->getActionlinks(),
            "action"    => $action,
            "person"    => $this->person,
            "title"     => $this->getTitle()
        ));
        return $tpl;
    }

}
