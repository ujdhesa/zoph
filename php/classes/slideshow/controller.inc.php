<?php
/**
 * Controller for slideshow
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace slideshow;

use conf\conf;
use photo\collection;
use web\request;

use log;
use photo;
use user;

/**
 * Controller for slideshow
 */
class controller {
    /** @var request holds request */
    protected   $request;

    public function __construct(request $request) {
        $this->request = $request;

        $offset = $this->request["_off"] ?: 0;
        $_pause = $this->request["_pause"];
        $_random = $this->request["_random"];

        $clean_vars=$this->request->getRequestVarsClean();

        $photoCollection = collection::createFromRequest($this->request);

        $toDisplay = $photoCollection->subset($offset, 1);

        $photoCount=sizeof($photoCollection);

    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $this->view="display";
    }

}
