<?php
/**
 * View for confirm delete photo
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use template\template;
use web\request;
use photo;
use user;

/**
 * This view displays the "confirm delete photo" page
 */
class confirm {

    /** * @var array request variables */
    private $vars;
    /** * @var request web request */
    private $request;
    /** * @var photo */
    private $photo;

    /**
     * Create view
     * @param request web request
     * @param photo photo to operate on
     */
    public function __construct(request $request, photo $photo) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->photo=$photo;
    }

    /**
     * Output view
     */
    public function view() {
        $actionlinks=array(
            translate("confirm")   =>  "photo.php?_action=confirm&amp;photo_id=" . $this->photo->getId() .
                            "&amp;_qs=" . $this->request->getPassedQueryString()->encode(),
            translate("cancel")    =>  "photo.php?" . $this->request->getPassedQueryString()
        );
        return new template("confirm", array(
            "title"     =>  translate("delete photo"),
            "actionlinks"   => $actionlinks,
            "mainActionlinks" => null,
            "obj"           => $this->photo,
            "image"         => $this->photo->getImageTag(MID_PREFIX)
        ));

    }
}
