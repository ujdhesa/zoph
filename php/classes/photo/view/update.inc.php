<?php
/**
 * View for edit photo page // WIP
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use conf\conf;
use geo\map;

use template\block;
use template\template;

use web\request;

use album;
use category;
use person;
use photographer;
use place;
use user;

/**
 * This view displays the "photo" page when editing a photo
 */
class update extends common {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() {
        $actionlinks=parent::getActionLinks();
        $user=user::getCurrent();
        unset($actionlinks[translate("edit")]);
        if (!$user->prefs->get("auto_edit")) {
            $actionlinks[translate("return")]="photo.php?" .  $this->request->getReturnQueryString();
        } else {
            $actionlinks[translate("display")]="photo.php?_action=display&amp;" .  $this->request->getReturnQueryString();
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() {
        $user = user::getCurrent();
        $photo = $this->photo;
        $warning = "";
        $autocomppeople = "true";

        if (!person::getAutocompPref()) {
            $warning = new block("message", array(
                "class" => "warning",
                "text" => translate("Autocomplete for people is needed to add people to a photo")
            ));
            $autocomppeople = "false";
        }

        $tpl = new template("editPhoto", array(
            "warning"           => $warning,
            "autocomppeople"    => $autocomppeople,
            "photo"             => $photo,
            "title"             => $this->getTitle($this->photocount, $this->offset),
            "selection"         => $this->getSelection(),
            "admin"             => (bool) $user->isAdmin(),
            "actionlinks"       => $this->getActionlinks(),
            "return_qs"         => $this->request->getReturnQueryString(),
            "rotate"            => conf::get("rotate.enable"),
            "prev"              => $this->prevURL,
            "up"                => $this->upURL,
            "next"              => $this->nextURL,
            "full"              => $photo->getFullsizeLink($photo->get("name")),
            "size"              => template::getHumanReadableBytes((int) $photo->get("size")),
            "share"             => $this->getShare(),
            "image"             => $photo->getFullsizeLink($photo->getImageTag(MID_PREFIX)),
            "albums"            => $photo->getAlbums($user),
            "categories"        => $photo->getCategories($user),
            "locPulldown"       => place::createPulldown("location_id", $photo->get("location_id")),
            "pgPulldown"        => photographer::createPulldown("photographer_id", $photo->get("photographer_id")),
            "albumPulldown"     => album::createPulldown("_album_id[0]"),
            "catPulldown"       => category::createPulldown("_category_id[0]", ""),
            "zoomPulldown"      => place::createZoomPulldown($photo->get("mapzoom")),
            "show"              => getvar("_show")
        ));

        if (conf::get("maps.provider")) {
            $map=new map();
            $map->setEditable();
            $map->setCenterAndZoomFromObj($photo);
            $map->addMarkers(array($photo), $user);
            $tpl .= $map;
        }
        return $tpl;
    }
}
