<?php
/**
 * View for display photo page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use conf\conf;
use geo\map;
use photo\people as photoPeople;
use template\block;
use template\template;
use web\request;

use calendar;
use photo;
use rating;
use selection;
use Time;
use user;

/**
 * This view displays the "photo" page
 */
class display extends common {

    /**
     * Get comments for the current photo
     * @return block template block
     */
    private function getComments() {
        if (conf::get("feature.comments")) {
            $comments=$this->photo->getComments();
            if ($comments) {
                $commentTpl=new block("comments", array(
                    "comments"  => $comments
                ));
            }
            return isset($commentTpl) ? $commentTpl : null;
        }
    }

    /**
     * Get related photos for the current photo
     * @return block template block
     */
    private function getRelated() {
        $related=$this->photo->getRelated();
        if ($related) {
            $tplRelated=new block("related_photos", array(
                "photo"     => $this->photo,
                "related"   => $related,
                "admin"     => (bool) user::getCurrent()->isAdmin()
            ));
        }
        return isset($tplRelated) ? $tplRelated : null;
    }

    /**
     * Get EXIF information for current photo
     * @return block template block
     */
    private function getExif() {
        if (user::getCurrent()->prefs->get("allexif")) {
            $exif=new block("exif", array(
                "allexif"   => $this->photo->exifToHTML()
            ));
        }
        return isset($exif) ? $exif : null;
    }

    /**
     * Get rating block (display) for current photo
     * @return block template block
     */
    private function getRating() {
        $rating = $this->photo->getRating();
        if ($rating && user::getCurrent()->isAdmin()) {
            $rating=$this->photo->getRatingDetails();
        }
        return $rating;
    }

    /**
     * Get rating block (to rate) for current photo
     * @return block template block
     */
    private function getRatingForm() {
        $user = user::getCurrent();
        if (conf::get("feature.rating") && $user->canRatePhotos()) {
            $ratingForm=new block("formRating", array(
                "ratingPulldown"=> rating::createPulldown("rating", $this->photo->getRatingForUser($user)),
                "photoId"       => $this->photo->getId()
            ));
        }
        return isset($ratingForm) ? $ratingForm : null;
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() {
        $actionlinks=parent::getActionlinks();
        unset($actionlinks[translate("cancel")]);
        unset($actionlinks[translate("return")]);
        return $actionlinks;
    }


    /**
     * Output view
     */
    public function view() {
        $user = user::getCurrent();
        $photo = $this->photo;
        $photo->lookup();

        if (!$user->isAdmin()) {
            $permissions = $user->getPhotoPermissions($photo);
        }

        $camInfo = $user->prefs->get("camera_info") ? $photo->getCameraDisplayArray() : null;

        $calendar = new calendar();
        $calendar->setSearchField("timestamp");

        if ($user->canBrowsePeople()) {
            $people=(new photoPeople($photo))->getInRows();
        }

        $timestamp = new Time($photo->get("timestamp"));

        $tpl = new template("displayPhoto", array(
            "photo"         => $photo,
            "actionlinks"   => $this->getActionlinks(),
            "title"         => $this->getTitle($this->photocount, $this->offset),
            "selection"     => $this->getSelection(),
            "prev"          => $this->prevURL,
            "up"            => $this->upURL,
            "next"          => $this->nextURL,
            "full"          => $full=$photo->getFullsizeLink($photo->get("name")),
            "size"          => template::getHumanReadableBytes((int)$photo->get("size")),
            "share"         => $this->getShare(),
            "image"         => $photo->getFullsizeLink($photo->getImageTag(MID_PREFIX)),
            "people"        => isset($people) ? $people : null,
            "fields"        => $photo->getDisplayArray(),
            "rating"        => $this->getRating(),
            "ratingForm"    => $this->getRatingForm(),
            "albums"        => template::createLinkList($photo->getAlbums()),
            "categories"    => template::createLinkList($photo->getCategories()),
            "timestampURL"  => $calendar->getDateLink($timestamp),
            "timestamp"     => $timestamp->getFormatted(),
            "description"   => trim($photo->get("description")),
            "camInfo"       => $camInfo,
            "related"       => $this->getRelated(),
            "exifdetails"   => $this->getExif(),
            "comments"      => $this->getComments()
        ));

        if (conf::get("maps.provider")) {
            $map = new map();
            $photos=$photo->getNear(100);
            $photos[]=$photo;
            $map->addMarkers($photos, $user);
            $tpl .= $map;
        }

        return $tpl;

    }
}
