<?php
/**
 * Via photo/data, data about a photo can be requested in JSON format
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace photo;

use photo;
use user;
use organizer;

/**
 * @author Jeroen Roos
 * @package Zoph
 */
class data  {
    /**
     * Create new locationLookup object
     * @param string string to parse
     */
    public function __construct(photo $photo) {
        $this->photo = $photo;
    }

    public function getData() {
        $loc  = "";
        $photographer  = "";
        $datetime = $this->photo->getFormattedDateTime();
        $albums = array_map(array("self", "getName"), $this->photo->getAlbums());
        $categories = array_map(array("self", "getName"), $this->photo->getCategories());

        $people = array_map(array("self", "getName"), (new people($this->photo))->getAll());

        if ($this->photo->location) {
            $loc = $this->photo->location->getName();
        }

        if ($this->photo->photographer) {
            $photographer = $this->photo->photographer->getName();
        }

        return array(
            "image"         => $this->photo->getURL(),
            "name"          => $this->photo->getName(),
            "location"      => $loc,
            "title"         => $this->photo->get("title"),
            "datetime"      => implode(" ", $datetime),
            "photographer"  => $photographer,
            "albums"        => $albums,
            "categories"    => $categories,
            "people"        => $people,
            "exifdata"      => $this->photo->getCameraDisplayArray()
        );
    }

    private static function getName(organizer $organizer) {
        return $organizer->getName();
    }

}
?>
