<?php
/**
 * A photo\collection is a collection of photos (@see photo).
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo;

use web\request;

use log;
use photo;
use Timezone;
use user;

/**
 * Collection of photo objects
 * @package Zoph
 * @author Jeroen Roos
 */
class collection extends \generic\collection {

    /**
     * Get an array of all the photo ids in this collection
     * @return array ids
     */
    public function getIds() {
        $ids = array();
        foreach ($this->items as $photo) {
            $ids[] = $photo->getId();
        }
        return $ids;

    }

    /**
     * Remove all photos that have no valid timezone
     *
     * This function is needed for geotagging: for photos without a valid
     * timezone it is not possible to determine the UTC time, needed for geotagging.
     * @return photo\collection photos with valid timezone
     */
    public function removeNoValidTZ() {
        $return=array();
        log::msg("Number of photos before valid timezone check: " .
            count($this->items), log::DEBUG, log::GEOTAG);

        foreach ($this->items as $photo) {
            $photo->lookup();
            $loc=$photo->location;
            if (get_class($loc)=="place") {
                $tz=$loc->get("timezone");
                if (TimeZone::validate($tz)) {
                    $return[]=$photo;
                }
            }
        }
        log::msg("Number of photos after valid timezone check: " . count($return),
            log::DEBUG, log::GEOTAG);
        $this->items=$return;
        return $this;
    }

    /**
     * Remove all photos that have lat/lon set
     * Remove photos that already have lat/lon information set from this collection
     *
     * This function is needed for geotagging, so photos that have lat/lon
     * manually set will not be overwritten
     * @return photo\collection photos with no lat/lon info
     */
    public function removeWithLatLon() {
        $return=array();
        log::msg("Number of photos before overwrite check: " . count($this->items),
            log::DEBUG, log::GEOTAG);
        foreach ($this->items as $photo) {
            $photo->lookup();
            if (!($photo->get("lat") || $photo->get("lon"))) {
                $return[]=$photo;
            }
        }
        log::msg("Number of photos after overwrite check: " . count($return),
            log::DEBUG, log::GEOTAG);

        $this->items=$return;
        return $this;
    }

    /**
     * Get a subset of photos to do geotagging test on
     * This will select a subset of photos containing of the first x, last x and or random x photos
     * from the subset. This is used to give the user a preview of what is going to be geotagged.
     * @param array subset array that can contain "first", "last" and/or "random"
     * @param int number of each to select
     */
    public function getSubsetForGeotagging(array $subset, $count) {
        $begin=0;

        $max=count($this);

        $count = min($max, $count);
        $return = new self;
        if (in_array("first", $subset)) {
            $first=$this->subset(0, $count);
            $max=$max-$count;
            $begin=$count;
            $return = $first;
        }
        if (in_array("last", $subset)) {
            $last=$this->subset(-$count);
            $max=$max-$count;
            $return = $return->merge($last);
        }

        if (in_array("random", $subset) && ($max > 0)) {
            $center=$this->subset($begin, $max);

            $max=count($center);

            if ($max!=0) {
                $random = $center->random($count);
                $return = $return->merge($random);
            }
        }


        return $return->renumber();
    }

    /**
     * Create a new photo\collection from request
     * @param request web request
     */
    public static function createFromRequest(request $request) {
        return static::createFromVars($request->getRequestVarsClean());
    }

    /**
     * Create a new photo\collection from request vars
     * @param array http request vars
     */
    public static function createFromVars(array $vars) {
        $search=new search($vars);
        $photos=photo::getRecordsFromQuery($search->getQuery());
        return static::createFromArray($photos, true);
    }

}
