<?php
/**
 * Controller for web services
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web\service;

use conf\conf;
use web\request;
use geo\locationLookup;
use photo\data as photoData;
use photo\people as photoPeople;
use photo\collection;
use photo;
use person;
use user;
/**
 * Controller for web services
 */
class controller {
    /** @var request holds request */
    private $request;

    /** @var array holds data */
    private $data;

    /** @var array Actions that can be used in this controller */
    private $actions = array(
        "locationLookup",
        "photoData",
        "photoPeople",
        "search"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct($action, request $request) {
        $this->request=$request;
        $this->doAction($action);

    }

    /**
     * Do the action as requested
     */
    public function doAction($action) {
        if (in_array($action, $this->actions)) {
            $function = "action" . ucwords($action);
            $this->$function();
        }
    }

    /**
     * get View
     * each of the actions dictate a subsequent view in the workflow,
     * the view can be called by this function
     * currently, it simply returns a name, in the future an action View object
     * may be returned.
     */
    public function getView() {
        return $this->view;
    }

    public function getData() {
        return $this->data;
    }

    /**
     * Do action 'locationLookup'
     */
    public function actionLocationlookup() {
        $this->view = "json";
        $search = $this->request["search"];
        $server = $this->request->getServerVar("SERVER_NAME");
        $location = new locationLookup($search, $server);
        $this->data = [ "search" => $search, "lat" => $location->getLat(), "lon" => $location->getLong(), "zoom" => $location->getZoom() ];
    }

    /**
     * Do action 'photoData'
     */
    public function actionPhotoData() {
        $this->view = "json";
        $photoId = $this->request["photoId"];
        $photo = new photo($photoId);
        if ($photo->lookup()) {
            $data = new photoData($photo);
            $this->data = $data->getData();
        }
    }

    /**
     * Do action 'photoPeople'
     */
    public function actionPhotoPeople() {
        $this->view = "json";
        $photoId = (int) $this->request["photoId"];
        $personId = (int) $this->request["personId"];
        $photo = new photo($photoId);
        $person = new person($personId);
        $photoPeople = new photoPeople($photo);
        $user=user::getCurrent();

        if (!$photo->isWritableBy($user)) {
            return;
        }

        $action = $this->request["action"];
        switch($action) {
            case "left":
                list($row, $pos) = $photoPeople->getPosition($person);
                $photoPeople->setPosition($person, $pos - 1);
                break;
            case "right":
                list($row, $pos) = $photoPeople->getPosition($person);
                $photoPeople->setPosition($person, $pos + 1);
                break;
            case "up":
                list($row, $pos) = $photoPeople->getPosition($person);
                $photoPeople->setRow($person, $row - 1);
                break;
            case "down":
                list($row, $pos) = $photoPeople->getPosition($person);
                $photoPeople->setRow($person, $row + 1);
                break;
            case "add":
                $row = (int) $this->request["row"];
                $person->addPhoto($photo);
                $photoPeople->setRow($person, $row);
                break;
            case "remove":
                $person->removePhoto($photo);
                break;
            default:
                // Not making any changes, just outputting status quo
                break;
        }
        if ($photo->lookup()) {
            $data = new photoPeople($photo);
            $this->data = $data->getData();
        }
    }

    public function actionSearch() {
        $this->view = "json";
        $photoCollection = collection::createFromRequest($this->request);

        $this->data=$photoCollection->getIds();
        return $this->data;
    }

    private static function getId(photo $photo) {
        return $photo->getId();
    }

}
