<?php
/**
 * A query string is the part of the URL after ?
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web;

/**
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class queryString {
    /** @var holds query string */
    private $qs;

    /** @var holds passed query string */
    private $passedQs;

    /**
     * Create object
     */
    public function __construct($qs, $passedQs = null) {
        $this->qs = $qs;
        $this->passedQs = $passedQs;
    }

    /**
     * Get Query String
     * @return string Query String
     */
    public function __toString() {
        return (string) $this->qs;
    }

    /**
     * Return the query string, urlencoded, it can be passed via an URL
     */
    public function encode() {
        return urlencode(htmlentities($this));
    }

    /**
     * Sometimes a form passes a previous query string as part of the data
     * this is needed to return to the original page. For example, if you have performed
     * a search and click on a photo, you're not simply sent to that photo,
     * but the query string for that photo contains the original search
     * to return to the search after the photo was updated, you need to retrieve that
     * query string through this function.
     */
    public function getPassed() {
        return new self($this->passedQs);
    }

    /**
     * Clean the query string by passing regexes
     * For example removing "_crumb" and "_action":
     * this->cleanQueryString(array("/_crumb=\d+&?/","/_action=\w+&?/"))
     * @param array regex to use for cleaning
     * @return string cleaned query string
     */
    public function regexClean(array $regexes) {
        $qs = (string) $this;
        foreach ($regexes as $regex) {
            $qs = preg_replace($regex, "", $qs);
        }
        return new self($qs);
    }

    /**
     * Get the return query string
     * This could be the passed query string ("_qs") or this function could
     * clean the current query string, removing "_crumb" and "_action"
     */
    public function getReturn() {
        $return=$this->getPassed();
        if (empty((string)$return)) {
            $return = $this->regexClean(array(
                "/_crumb=\d+&?/",
                "/_action=\w+&?/"
            ));
        }
        return new self($return);
    }
}
