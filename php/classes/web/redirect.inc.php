<?php
/**
 * Perform a redirect
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web;

use log;
use template\block;

/**
 * Perform a redirect
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class redirect {
    /**
     * Redirect
     * @param string URL to redirect to
     * @param string message to show as redirect reason
     * keep in mind that the message is only shown when debugging. A normal user will not see them.
     * Not testable because of 'exit' statement.
     * @codeCoverageIgnore
     */
    public static function redirect($url = "zoph.php", $msg = "Access denied") {
        if (!((LOG_SUBJECT & log::REDIRECT) && (LOG_SEVERITY >= log::DEBUG))) {
            header("Location: " . $url);
        } else {
            $tpl = new block("link", array(
                "href"      => $url,
                "target"    => "",
                "link"      => $msg
            ));
            echo $tpl;
        }
        exit;
    }
}
