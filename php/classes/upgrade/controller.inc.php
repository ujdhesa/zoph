<?php
/**
 * Controller for migrations
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use generic\controller as genericController;
use template\block;
use web\request;


/**
 * Controller for migrations
 */
class controller extends genericController {

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display", "upgrade", "rollback"
    );

    /** @var array migrations to be done */
    private $migrations;

    /** @var string Where to redirect after actions */
    public $redirect="zoph.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request, $migrations = null) {
        parent::__construct($request);

        $this->migrations = $migrations ?: new migrations();
        $this->doAction();
    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        if ($this->migrations->check(conf::get("zoph.version"))) {
            $this->view=new view\display($this->request, $this->migrations);
        } else {
            $this->view=new view\notfound($this->request);
        }
    }

    /**
     * Do action 'upgrade'
     */
    public function actionUpgrade() {
        $doneMigrations=array();
        if ($this->migrations->check(conf::get("zoph.version"))) {
            foreach ($this->migrations->get() as $migration) {
                $results = $migration->up();
                $doneMigrations[] = new block("migration", array(
                    "desc"      => $migration->getDesc(),
                    "results"   => $results
                ));
                conf::set("zoph.version", $migration->getTo())->update();
            }
            $this->view=new view\upgrade($this->request, $doneMigrations);
        } else {
            $this->view=new view\notfound($this->request);
        }
    }

    /**
     * Do action 'rollback'
     */
    public function actionRollback() {
        // Not implemented
    }
}
