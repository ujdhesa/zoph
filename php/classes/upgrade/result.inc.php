<?php
/**
 * Database migration results
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use template\block;
use template\template;

/**
 * Database migrations results
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class result {

    /** @var int result::TODO - this migration is pending */
    public const TODO=-1;
    /** @var int result::SUCCESS - this migration was executed succesfully  */
    public const SUCCESS=0;
    /** @var int result::INFO - this migration was executed succesfully, but gave some log info  */
    public const INFO=1;
    /** @var int result::WARNING - this migration was executed succesfully, but gave a warning */
    public const WARNING=2;
    /** @var int result::ERROR - this migration encountered an error */
    public const ERROR=3;
    /** @var int result::UNDEFINED - this migration's status is unknown */
    public const UNDEFINED=99;

    /** @var int store result */
    private $result=99;
    /** @var string store result message */
    private $msg = "";
    /** @var string store result error */
    private $error = "";

    /**
     * Create result object
     * @param int result: result::TODO, result::SUCCESS, result::INFO, result::WARNING or result::ERROR
     * @param string result message
     * @param string|null result error
     */
    public function __construct(int $result, string $msg, string $error=null) {
        $this->result = $result;
        $this->msg = $msg;
        $this->error = $error;
    }

    /**
     * Display result object
     * @return string template output
     */
    public function __toString() {
        switch ($this->result) {
            case self::TODO:
                $status = "todo";
                $icon = template::getImage("icons/migration.png");
                break;
            case self::SUCCESS:
                $status = "ok";
                $icon = template::getImage("icons/ok.png");
                break;
            case self::INFO:
                $status = "info";
                $icon = template::getImage("icons/info.png");
                break;
            case self::WARNING:
                $status = "warning";
                $icon = template::getImage("icons/warning.png");
                break;
            case self::ERROR:
                $status = "error";
                $icon = template::getImage("icons/error.png");
                break;
            default:
                $status = "unknown";
                $icon = template::getImage("icons/unknown.png");
                break;
        }
        return (string) new block("result", array(
            "status"=> $status,
            "icon"  => $icon,
            "msg"   => $this->msg,
            "error" => $this->error
        ));
    }
}
