<?php
/**
 * View for 'no pending migrations found' page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade\view;

use conf\conf;
use template\template;
use web\request;

/**
 * This view displays a "not found" error in case no (pending) migrations were found
 */
class notfound {
    /**
     * Create view
     * @param request web request
     */
    public function __construct(request $request) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
    }

    /**
     * Return headers - always null because the default headers are used
     * @return null
     */
    public function getHeaders() {
        return null;
    }

    /**
     * Output view
     */
    public function view() {
        return new template("upgrade", array(
            "title"         => $this->getTitle(),
            "version"       => VERSION,
            "migrations"    => array(translate("Nothing to do, all upgrades have been done")),
            "button"       => translate("return"),
            "button_url"   => "zoph.php"
        ));
    }

    /**
     * Get the title for this view
     */
    public function getTitle() {
        return translate("No upgrade necessary");
    }

}
