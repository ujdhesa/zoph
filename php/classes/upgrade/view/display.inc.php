<?php
/**
 * View for display pending migrations
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade\view;

use conf\conf;
use upgrade\migrations;
use upgrade\result;
use template\block;
use template\template;
use template\form;
use web\request;

/**
 * Display pending migrations
 */
class display {

    /** @var migrations migrations */
    private $migrations;

    /** @var string title */
    private $title;

    /**
     * Create view
     * @param request web request
     * @param migrations to be displayed
     */
    public function __construct(request $request, migrations $migrations) {
        $this->migrations = $migrations;
        if (sizeof($migrations->get()) == 1) {
            $this->title=$migrations->get()[0]->getDesc();
        }
    }

    /**
     * Get http headers
     * Always returns null as the default headers will be used for this view
     * @return null
     */
    public function getHeaders() {
        return null;
    }

    /**
     * View for displaying pending migrations
     * @return template\template
     */
    public function view() {
        $showMigrations = array();
        foreach ($this->migrations->get() as $migration) {
            $steps = array();
            foreach ($migration->show() as $step) {
                $steps[] = new result(result::TODO, $step);
            }
            $showMigrations[] = new block("migration", array(
                "desc" => $migration->getDesc(),
                "results" => $steps
            ));
        }

        $message=new block("message", array(
            "title"     => translate("backup"),
            "class"     => "warning",
            "text"      => translate("it is highly recommended that you make a backup of your database prior to performing an upgrade")
        ));
        $message->addActionlinks(array(
            translate("make backup")   => "backup.php?_return=upgrade.php"
        ));

        $button = new block("button", array(
            "button"       => translate("start upgrade"),
            "button_url"   => "upgrade.php?_action=upgrade",
            "button_class" => "big green"
        ));

        $tpl=new template("upgrade", array(
            "title"         => conf::get("interface.title"),
            "version"       => VERSION,
            "migrations"    => $showMigrations,
        ));
        $tpl->addBlocks(array($message, $button));
        return $tpl;
    }

    /**
     * Get title
     * @return string title
     */
    public function getTitle() {
        return $this->title ?: translate("Zoph Upgrade");
    }
}
