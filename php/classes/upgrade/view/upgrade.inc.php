<?php
/**
 * View to show migration results
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade\view;

use conf\conf;
use upgrade\migrations;
use upgrade\result;
use template\block;
use template\template;
use web\request;

/**
 * Display executed migrations
 */
class upgrade {

    /** @var migrations migrations */
    private $migrations;

    /** @var string title */
    private $title;

    /**
     * Create view
     * @param request web request
     * @param array results
     */
    public function __construct(request $request, array $migrations) {
        $this->migrations = $migrations;
    }

    /**
     * Get headers, always null, as default headers are used
     * @return null
     */
    public function getHeaders() {
        return null;
    }

    /**
     * Retrun the view
     * @return template\template upgrade view
     */
    public function view() {
        return new template("upgrade", array(
            "title"         => conf::get("interface.title"),
            "version"       => VERSION,
            "migrations"    => $this->migrations,
            "button"       => translate("finish"),
            "button_url"   => "zoph.php"
        ));
    }

    /**
     * Get title
     * @return string title
     */
    public function getTitle() {
        return $this->title ?: translate("Zoph Upgrade");
    }
}
