<?php
/**
 * Database DROP TABLE query
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace db;

/**
 * Database DROP TABLE query
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class drop extends query {

    /** @var string name of the table */
    private $name;

    /**
     * Create a new DROP TABLE query
     * @param string name
     * @param array columns
     */
    public function __construct(string $name) {
        $this->name=$name;
    }

    /**
     * Build the clause
     * @return string clause
     */
    public function __toString() {
        return "DROP TABLE " . db::getPrefix() . $this->name;
    }
}
