<?php
/**
 * XMP Data - Interprete and return XMP data
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace xmp;

use ArrayAccess;
use Iterator;
use Countable;

/**
 * @author Jeroen Roos
 * @package Zoph
 */
class data implements ArrayAccess, Iterator, Countable  {

    private $xmpdata = array();

    /**
     * Create new xmp/data object
     * @param array data to inject
     */
    public function __construct(array $data=null) {
        $this->xmpdata = $data;
    }

    public function getRDFDescription($off) {
        return $this["x:xmpmeta"]["rdf:RDF"]["rdf:Description"][$off];
    }

    /**
     * For ArrayAccess: does the offset exist
     * @param int|string offset
     * @return bool offset exists
     */
    public function offsetExists($off) {
        return (isset($this->xmpdata[$off]));
    }
    /**
     * For ArrayAccess: Get value of parameter
     * @param int|string offset
     * @return mixed value
     */
    public function offsetGet($off) {
        $data = $this->xmpdata[$off] ?? null;
        if (is_array($data)) {
            if (sizeof($data) === 0) {
                return null;
            } else {
                return new self($data);
            }
        } else {
            return $data;
        }
    }

    /**
     * For ArrayAccess: Set value of parameter
     * @param int|string offset
     * @param mixed value
     */
    public function offsetSet($off, $val) {
        $this->xmpdata[$off] = $val;
    }

    /**
     * For ArrayAccess: Unset value of parameter
     * @param int|string offset
     */
    public function offsetUnset($off) {
        if (isset($this->xmpdata[$off])) {
            unset($this->xmpdata[$off]);
        }
    }

    /**
     * For ObjectAccess: Get value of parameter
     * @param int|string offset
     * @return mixed value
     */
    public function __get($off) {
        return $this->offsetGet($off);
    }

    /**
     * For Iterator access - rewind to begining
     */
    public function rewind() {
        reset($this->xmpdata);
    }

    /**
     * For Iterator access - get current
     * @return mix current entry
     */
    public function current() {
        return current($this->xmpdata);
    }

    /**
     * For Iterator access - get key of current entry
     * @return int|string current key
     */
    public function key() {
        return key($this->xmpdata);
    }

    /**
     * For Iterator access - move to next entry and return contents
     * @return mixed next entry
     */
    public function next() {
        return next($this->xmpdata);
    }

    /**
     * For Iterator access - is the current entry valid
     * @return bool valid
     */
    public function valid() {
        return key($this->xmpdata) !== null;
    }

    /**
     * For Countable access - get size of data
     */
    public function count() {
        return count($this->xmpdata);
    }
}
?>
