<?php
/**
 * XMP Decoder - Interprete and return XMP data
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace xmp;

/**
 * @author Jeroen Roos
 * @package Zoph
 */
class decoder implements \Countable {

    /**
     * Create new xmp/decoder object
     * @param array data to inject
     */
    public function __construct(array $data) {
        $this->xmpdata = $data;
    }

    public function getSubjects() {
        if ($this->xmpdata and !empty($this->xmpdata)) {
            return $this->xmpdata[0]->getRDFDescription("dc:subject");
        }
    }

    public function getRating() {
        if ($this->xmpdata && !empty($this->xmpdata)) {
            $rating = (int) $this->xmpdata[0]->getRDFDescription("xmp:Rating");
            if ($rating != 0) {
                return $rating;
            }
        }
    }

    /**
     * Count number of entries in the data
     * for Countable access
     */
    public function count() {
        return sizeof($this->xmpdata);
    }
}

