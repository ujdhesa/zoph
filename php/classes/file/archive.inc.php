<?php
/**
 * Handle archive files
 * For now, it only handles creating ZIP files
 * in the future it will also handle unzipping, untarring etc.
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace file;

use photo\collection as photoCollection;
use log;
use user;
use ZipArchive;


/**
 * Handle archive files
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class archive {
    /** @var string filename */
    private $filename;
    /** @var string filename of tempfile used to create zipfile*/
    private $tempfile;
    /** @var int maximum archive file size */
    private $maxsize = -1;
    /** @var ZipArchive holds created archive */
    private $archive;

    /** @var string archive::ZIP determines archive type */
    const ZIP = "zip";

    /**
     * Create a new archive
     * @param string type of archive, only ZIP is supported now, parameter is ignored
     * @param string filename to create
     * @param int file number, used when archive is growing bigger than "maxsize"
     */
    public function __construct($type, $filename, $filenum) {
        // $type is ignored for now
        $user=user::getCurrent();
        $this->filename=$filename;
        $this->type=$type;

        if ($this->checkZipSupport()) {
            $this->archive = new ZipArchive();
            $this->tempfile = "/tmp/zoph_" . $user->get("user_id") . "_" . $this->filename ."_" . (int) $filenum . ".zip";
            if (file_exists($this->tempfile)) {
                unlink($this->tempfile);
            }
            if ($this->archive->open($this->tempfile, ZipArchive::CREATE)!==true) {
                log::msg("Cannot create temporary ZIP archive, " . $this->tempfile, log::FATAL, log::GENERAL);
            }
        } else {

        }
    }

    /**
     * Set maximum size
     * Adding photos to the archive is stopped when the archive becomes too big
     * this is used to spread over multiple files
     * @param int maximum size in bytes
     */
    public function setMaxSize($bytes) {
        $this->maxsize=(int) $bytes;
    }

    /**
     * Add photos to archive
     * @param photo\collection photos to add
     * @return int file number last added
     */
    public function addPhotos(photoCollection $photos) {
        $zipsize=0;
        $file=0;
        foreach ($photos as $photo) {
            if ($data=@file_get_contents($photo->getFilePath())) {
                $size=strlen($data);
                $zipsize=$zipsize+$size;
                if ($this->maxsize>0 && $zipsize>=$this->maxsize) {
                    break;
                }
                $file++;
                $this->archive->addFromString($photo->get("name"), $data);

            } else {
                echo sprintf(translate("Could not read %s."), $photo->getFilePath()) . "<br>\n";
            }
        }
        if (!$this->archive->close()) {
            log::msg("ZIP file creation failed", log::FATAL, log::GENERAL);
        }
        return $file;
    }

    /**
     * Checks whether ZIP support is enabled in PHP
     * @return bool zip support enabled
     */
    public function checkZipSupport() {
        return class_exists("ZipArchive") or log::msg(translate("You need to have ZIP support in PHP to download zip files"), log::FATAL, log::GENERAL);
    }

}
