<?php
/**
 * View for edit comments
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment\view;

use conf\conf;
use template\form;
use template\template;
use web\request;
use zophCode\smiley;

use comment;
use photo;
use user;

/**
 * This view displays the comment page when editing
 */
class update {
    /** @var web\request holds the request */
    private $request;

    /** @var comment holds the comment */
    private $comment;

    /** @var photo holds the photo this comment belongs to */
    private $photo;

    public function __construct(request $request) {
        $comment=new comment($request["comment_id"]);
        $comment->lookup();
        $this->comment=$comment;
        $this->request=$request;
        $photo = $comment->getPhoto();
        if (!$photo instanceof photo && $comment->getId() == 0) {
            $photo = new photo((int) $request["photo_id"]);
            $photo->lookup();
        }
        $this->photo=$photo;
    }

    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() {
        return array(
            "return"    => "photo.php?photo_id=" . $this->photo->getId(),
            "new"       => "comment.php?_action=new"
        );
    }

    /**
     * Output the view
     */
    public function view() {
        $user = user::getCurrent();
        $photo=$this->photo;

        if ($this->request["_action"] == "new") {
            $action = "insert";
        } else if ($this->request["_action"] == "edit") {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = $this->request["_action"];
        }

        $tpl = new template("main", array(
            "title"             => $this->getTitle(),
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => null,
        ));

        $tpl->addBlock($photo->getImageTag(MID_PREFIX));

        $form = new form("form", array(
            "formAction"    => "comment.php",
            "class"         => "comment",
            "onsubmit"      => null,
            "action"        => $action,
            "submit"        => translate($action, 0)
        ));


        $form->addInputHidden("comment_id", $this->comment->getId());
        $form->addInputHidden("photo_id", $this->photo->getId());
        $form->addInputText("subject", $this->comment->get("subject"), translate("subject"));
        $form->addTextarea("comment", $this->comment->get("comment"), translate("comment"), 80, 8);


        $tpl->addBlock($form);
/*
         <h2><?php echo translate("smileys you can use"); ?></h2>
*/
       $tpl->addBlock(smiley::getOverview());

        return $tpl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() {
        if ($this->comment->getId()==0) {
            return translate("Add comment");
        } else if (trim($this->comment->get("subject"))=="") {
            return translate("comment");
        } else {
            return $this->comment->get("subject");
        }
    }


}
