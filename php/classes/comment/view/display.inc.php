<?php
/**
 * View for display comment
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment\view;

use conf\conf;
use template\block;
use template\template;
use web\request;

use comment;
use user;

/**
 * This view displays a comment
 */
class display {

    /** @var comment holds the comment */
    private $comment;

    public function __construct(request $request) {
        $comment=new comment($request["comment_id"]);
        $comment->lookup();
        $this->comment=$comment;
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() {
        $user=user::getCurrent();
        $actionlinks=array(
            "return"    =>  "photo.php?photo_id=" . $this->comment->getPhoto()->getId()
        );

        if ($user->isAdmin() || $this->comment->isOwner($user)) {
            $actionlinks = array_merge($actionlinks, array(
                "edit"      =>  "comment.php?_action=edit&amp;comment_id=" . $this->comment->getId(),
                "delete"    =>  "comment.php?_action=delete&amp;comment_id=" . $this->comment->getId(),
            ));
        }
        return $actionlinks;
    }


    /**
     * Output view
     */
    public function view() {
        $user=user::getCurrent();

        $tpl=new template("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks($this->getActionlinks());
        $tpl->addBlock($this->comment->getPhoto()->getImageTag(MID_PREFIX));

        $tpl->addBlock(new block("definitionlist", array(
            "class" => "display comment",
            "dl"    => $this->comment->getDisplayArray()
        )));

        return $tpl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() {
        return $this->comment->get("subject");
    }

}
