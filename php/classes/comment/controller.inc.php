<?php
/**
 * Controller for comments
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment;

use conf\conf;
use comment;
use generic\controller as genericController;
use web\request;
use photo;
use user;

use photoNotAccessibleSecurityException;
use userInsufficientRightsSecurityException;
/**
 * Controller for comment
 */
class controller extends genericController {
    /** @var photo photo that this comment belongs to */
    private $photo;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "edit", "insert",
        "new", "update"
    );

    /** @var string Where to redirect after actions (last resort) */
    public $redirect="comments.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);

        $user=user::getCurrent();
        if (!$user->canLeaveComments()) {
            throw new userInsufficientRightsSecurityException("User has no rights to leave comments");
        }

        $comment_id = (int) $request["comment_id"];
        $comment = new comment($comment_id);
        if ($comment_id !== 0) {
            $comment->lookup();
        }

        $photo = $comment->getPhoto();
        if (!$photo instanceof photo && $comment_id == 0) {
            $photo = new photo((int) $request["photo_id"]);
            $photo->lookup();
        }
        $this->photo=$photo;

        if (!$user->getPhotoPermissions($photo) && !$user->canSeeAllPhotos()) {
            throw new photoNotAccessibleSecurityException("User has no access to this photo");
        }
        $this->object=$comment;
        $this->doAction();
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        $user = user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            parent::actionConfirm();
            $this->redirect="photo.php?photo_id=" . $this->photo->getId();
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        $user = user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            parent::actionDelete();
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            $this->view="update";
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($user->isAdmin() || ($user->canLeaveComments() && $this->object->isOwner($user))) {
            unset($this->request["photo_id"]);
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
        }
        $this->view="display";
    }

    /**
     * Do action 'insert'
     */
    public function actionInsert() {
        $user = user::getCurrent();
        if ($user->canLeaveComments()) {
            $comment = new comment();
            $comment->set("user_id", user::getCurrent()->getId());
            $this->setObject($comment);
            unset($this->request["photo_id"]);
            parent::actionInsert();
            $comment->addToPhoto($this->photo);
        }
        $this->redirect="photo.php?photo_id=" . $this->photo->getId();
        $this->view="redirect";
    }
}
