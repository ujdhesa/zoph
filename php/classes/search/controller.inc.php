<?php
/**
 * Controller for searches
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace search;

use generic\controller as genericController;
use search;
use web\request;
use user;

/**
 * Controller for searches
 */
class controller extends genericController {

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array("confirm", "delete", "display", "edit", "insert", "new", "update", "search");

    /** @var string Where to redirect after actions */
    public $redirect="search.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        $_action = $request["_action"];
        parent::__construct($request);
        if (isset($this->request["search_id"])) {
            $search = new search($this->request["search_id"]);
            $search->lookup();
        } else if ($_action=="new") {
            $search=new search();
            $search->setSearchURL($this->request);
            $search->set("owner", user::getCurrent()->getId());
        } else {
            $search=new search();
        }
        $this->setObject($search);
        $this->doAction();
    }

    /**
     * Do action 'search'
     */
    public function actionSearch() {
        $this->view="photos";
    }
}
