<?php
/**
 * Keep track of interface parameters
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos;

use conf\conf;
use photo\collection;
use template\pager;
use web\request;

use illegalValueSecurityException;
use user;

/**
 * Keep track of interface parameters
 */
class params {
    /** @var web\request request used to call this page */
    private $request;

    /** @var int columns */
    public $cols = 0;

    /** @var int rows */
    public $rows = 0;

    /** @var int cels */
    public $cells = 0;

    /** @var int offset */
    public $offset = 0;

    /** @var int sort order */
    public $order = "date";

    /** @var int sort direction */
    public $dir = "asc";

    /** @var int number of photos */
    public $photoCount = 0;

    /** @var int number of photos to display */
    public $displayCount = 0;

    /** @var int number of pages */
    public $pageCount = 0;

    /** @var int show ????*/
    public $show = "";

    /** @var photo\collection holds collection of photos to work with */
    private $collection;

    /** @var photo\collection holds collection of photos to show */
    private $toDisplay;

    /** @var array list of photos in user's lightbox */
    private $lightbox;

    /** @var string title name (either 'Photos' or 'Lightbox' */
    /** @todo possibly also 'Edit photos' or 'Search results' */
    public $name = "Photos";

    /** @var string page title (HTML window title) */
    public $title;

    /** @var string page title (to display in title bar)*/
    public $titleBar;


    /**
     * Create class using web request
     * @param web\request request
     * @throws illegalValueSecurityException
     */
    public function __construct(request $request) {
        $this->request = $request;
        $user=user::getCurrent();
        $this->cols  = (int) ($request["_cols"]    ?? $user->prefs->get("num_cols"));
        $this->rows  = (int) ($request["_rows"]    ?? $user->prefs->get("num_rows"));
        $this->offset= (int) ($request["_off"]     ?? $_off = 0);
        $this->order =        $request["_order"]   ?? conf::get("interface.sort.order");
        $this->dir   =        $request["_dir"]     ?? conf::get("interface.sort.dir");
        $this->show  =        $request["_show"];

        $this->cells = $this->rows * $this->cols;

        if (!preg_match("/^[a-zA-Z_]*$/", $this->order)) {
            throw new illegalValueSecurityException("Illegal characters in _order");
        }

        $this->collection = collection::createFromRequest($request);
        $this->toDisplay = $this->collection->subset($this->offset, $this->cells);
        $this->setLightbox();
        $this->setCounts();
        $this->setTitle();
        $this->setPager();
    }

   /**
    * Set various calculated values
    */
    private function setCounts() {
        $this->displayCount = sizeof($this->toDisplay);
        $this->photoCount=(sizeof($this->collection));
        $this->pageCount = ceil($this->photoCount / $this->cells);
    }

   /**
    * Set pager
    * Create a pager and store it so the view can later retrieve it
    */
    private function setPager() {
        $user=user::getCurrent();
        $this->pager = new pager(
            $this->offset,
            $this->photoCount,
            $this->pageCount,
            $this->cells,
            $user->prefs->get("max_pager_size"),
            $this->request->getRequestVarsClean(),
            "_off"
        );
    }

   /**
    * Set page and titlebar titles
    * Set the titles for this page, based on where in the photo collection we are
    */
    private function setTitle() {
        if ($this->photoCount) {
            $currentPage = floor($this->offset / $this->cells) + 1;

            $num = min($this->cells, $this->displayCount);

            $this->title = sprintf(translate($this->name . " (Page %s/%s)", 0),
                $currentPage, $this->pageCount);
            $this->titleBar = sprintf(translate("photos %s to %s of %s"),
                ($this->offset + 1), ($this->offset + $num), $this->photoCount);
        } else {
            $this->title = translate("No Photos Found");
            $this->titleBar = translate("photos");
        }

    }

   /**
    * Get additional CSS
    * increases the width of the main window in case that is needed to
    * display the configured number of columns
    * @return string CSS code
    */
    public function getStyle() {
        if (!($this->displayCount == 0 || $this->cols <= 4)) {
            $width = ((THUMB_SIZE + 14) * $this->cols) + 25;
            $defaultWidth= conf::get("interface.width");
            if ($width > $defaultWidth || strpos($defaultWidth, "%")) {
                return "body    { width: " . $width . "px; }\n";
            }
        }
    }

   /**
    * Get an array of photo_ids in this user's lightbox
    * @return array
    */
    public function getLightbox() {
        return $this->lightbox;
    }

   /**
    * Get pager
    * retrieve a stored pager
    * @return template\block pager template
    */
    public function getPager() {
        return $this->pager;
    }

   /**
    * Create an array of photo_ids in this user's lightbox
    */
    private function setLightbox() {
        $lightbox = user::getCurrent()->getLightbox();
        if ($lightbox) {
            if ($this->request["album_id"] == $lightbox->getId()) {
                $this->name = "Lightbox";
            }

            $lbPhotos = array_map(
                function($photo) {
                    return $photo->getId();
                }, $lightbox->getPhotos());
            $this->lightbox=$lbPhotos;
        }
    }
}
