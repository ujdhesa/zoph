<?php
/**
 * Helpers for download photos page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use template\block;
use template\form;
use template\template;

/**
 * This class contains some helpers for the download form
 */
class helpers {

    /**
     * Create dropdown for "maximum files per zipfile"
     * @return block pulldown template
     */
    public static function getMaxFilesDropdown() {
        return template::createPulldown("_maxfiles", "100", array(
            10 => 10,
            25 => 25,
            50 => 50,
            75 => 75,
            100 => 100,
            150 => 150,
            200 => 200,
            300 => 300,
            400 => 400,
            500 => 500
        ));
    }

    /**
     * Create dropdown for "maximum size per zipfile"
     * @return block pulldown template
     */
    public static function getMaxSizeDropdown() {
        return template::createPulldown("_maxsize", "50000000", array(
            "5000000" => "5MiB",
            "10000000" => "10MiB",
            "25000000" => "25MiB",
            "50000000" => "50MiB",
            "75000000" => "75MiB",
            "100000000" => "100MiB",
            "150000000" => "150MiB",
            "250000000" => "250MiB",
            "500000000" => "500MiB",
            "650000000" => "650MiB",
            "1000000000" => "1GiB",
            "2000000000" => "2GiB",
            "4200000000" => "4.2GiB"
        ));
    }

}
