<?php
/**
 * Common parts for photos view
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use search;
use user;

use conf\conf;
use photo\collection as photoCollection;
use photos\params;
use web\request;

/**
 * This is a base view for the photos page, that contains common parts for all views
 */
abstract class view {

    /** @var photo\collection photos for this request */
    protected $photos;

    /** @var photo\collection photos to display */
    protected $display;

    /** @var array of photo_ids that are in this user's lightbox */
    protected $lightbox;

    /** * @var array request variables */
    protected $vars;

    /** * @var request web request */
    protected $request;

    /** * @var photos\params interface data for page */
    protected $params;

    /**
     * Create view
     * @param request web request
     */
    public function __construct(request $request, params $params) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->params=$params;
    }

    /**
     * Get actionlinks
     */
    protected function getActionlinks() {
        $user=user::getCurrent();
        $qs=$this->request->cleanQueryString(array("/_crumb=\d+&?/", "/_action=\w+&?/"));
        $actionlinks=array();
        if ($this->vars["_action"] ?? "display" == "search") {
            $search = new search();
            $search->setSearchURL($this->request);
            $actionlinks[translate("save search")] = "search.php?_action=new&_qs=" . $search->getSearchQS();
        }
        if ($user->isAdmin()) {
            $actionlinks[translate("edit")] = "photos.php?_action=edit&" . $qs;
            $actionlinks[translate("geotag")] = "tracks.php?_action=geotag&" . $qs;
        }

        $actionlinks[translate("slideshow")] = "slideshow.php?" . $qs;

        if (conf::get("feature.download") && ($user->get("download") || $user->isAdmin())) {
            $actionlinks[translate("download")] = "photos.php?_action=download&" . $qs;
        }
        return $actionlinks;
    }

    /**
     * Output view
     */
    abstract public function view();

    /**
     * Get the title for this view
     */
    public function getTitle() {
        return $this->params->title;
    }

    /**
     * get Headers
     */
    public function getHeaders() {
        return null;
    }

   /**
    * Set photo collection
    * @param photo\collection photos for this view
    */
    public function setPhotos(photoCollection $photos) {
        $this->photos = $photos;
    }

   /**
    * Set photo collection to be displayed
    * @param photo\collection photos to display
    */
    public function setDisplay(photoCollection $display) {
        $this->display = $display;
    }

   /**
    * Set photoids for the lightbox of the current user
    * @param array|null of photoids
    */
    public function setLightbox(?array $lightbox) {
        $this->lightbox = (array) $lightbox;
    }
}
