<?php
/**
 * View for display photos page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use conf\conf;
use geo\map;
use geo\marker;
use photo\collection as photoCollection;
use template\block;
use template\form;
use template\template;
use web\request;

use user;

/**
 * This view displays the "photos" page
 */
class display extends view {
    /**
     * Output view
     */
    public function view() {

        switch($this->params->dir) {
        case "asc":
            $up = template::getImage("up1.gif");
            $down = template::getImage("down2.gif");
            break;
        case "desc":
            $up = template::getImage("up2.gif");
            $down = template::getImage("down1.gif");
            break;
        }

        $sortupurl = "photos.php?" . http_build_query(
            $this->request->getUpdatedVars("_dir", "asc"));
        $sortdownurl = "photos.php?" . http_build_query(
            $this->request->getUpdatedVars("_dir", "desc"));

        $viewformvars = $this->request->getUpdatedVars(null, null,
            array ("_rows", "_cols", "_order", "_button"));
        $order=template::createPhotoFieldPulldown("_order", $this->params->order);

        $rowsDropdown = template::createIntegerPulldown("_rows", $this->params->rows, 1, 20);
        $colsDropdown = template::createIntegerPulldown("_cols", $this->params->cols, 1, 20);



        $viewform=new form("viewSettings", array(
            "sortup"            => $up,
            "sortdown"          => $down,
            "order"             => $order,
            "rowsDropdown"      => $rowsDropdown,
            "colsDropdown"      => $colsDropdown,
            "sortupurl"         => $sortupurl,
            "sortdownurl"       => $sortdownurl
        ));
        $viewform->addHiddenFields($viewformvars);
        $tpl=new template("displayPhotos", array(
            "title"             => $this->params->titleBar,
            "actionlinks"       => $this->getActionlinks(),
            "viewsettings"      => $viewform,
            "displaycount"      => $this->params->displayCount,
            "cols"              => $this->params->cols,
            "thumbnails"        => $this->getThumbs(),
            "thActionlinks"     => $this->getThumbActionlinks(),
            "pager"             => $this->params->getPager(),
            "map"               => $this->getMap()
        ));

        return $tpl;
    }

   /**
    * Get an array of thumbnails to display
    * @param array of @see template\block
    */
    private function getThumbs() {
        $offset = $this->params->offset;
        $ignore = array("_action", "_photo_id");
        $thumbs = array();
        foreach ($this->display as $photo) {
            if (isset($this->request["_random"])) {
                $thumbs[$offset] = $photo->getThumbnailLink();
            } else {
                $vars = $this->request->getUpdatedVars("_off", $offset, $ignore);
                $qs = http_build_query($vars);
                $thumbs[$offset] = $photo->getThumbnailLink("photo.php?" . $qs);
            }
            $offset++;
        }
        return $thumbs;
    }

   /**
    * Get actionlinks for thumbnails
    * at this moment only to remove from lightbox
    * @return array links
    */
    private function getThumbActionlinks() {
        $offset = $this->params->offset;
        $actionlinks = array();
        foreach ($this->display as $photo) {
            if (array_search($photo->getId(), $this->lightbox) !== false) {
                $photoId = (int) $photo->getId();
                $qs = $this->request->getEncodedQueryString();
                $actionlinks[$offset] = new block("actionlinks", array(
                    "actionlinks" => array(
                        "x" => "photo.php?_action=unlightbox&photo_id=" . $photoId .
                               "&_qs=" . $qs
                    )
                ));
            } else {
                $actionlinks[$offset] = "";
            }
            $offset++;
        }
        return $actionlinks;
    }

   /**
    * Get map to display with this page
    * @return geo\map map template
    */
    private function getMap() {
        if (conf::get("maps.provider")) {
            $map=new map();
            foreach ($this->display as $photo) {
                $photo->lookup();
                $marker=$photo->getMarker();
                if ($marker instanceof marker) {
                    $map->addMarker($marker);
                }
            }
            return $map;
        }
    }
}
