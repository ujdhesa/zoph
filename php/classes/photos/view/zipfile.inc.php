<?php
/**
 * View for download photos page - zipfile
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use conf\conf;
use fileNotFoundException;
use photo;
use photo\collection as photoCollection;
use photos\filenameException;
use photos\params;
use template\block;
use template\form;
use template\template;
use user;
use web\request;

/**
 * This sends the zip file to the browser
 */
class zipfile extends view {
    /** @var string filename of zipfile as downloaded */
    private $filename;

    /** @var string filename of zipfile as stored in /tmp */
    private $zipfile;

    public function __construct(request $request, params $params) {
        parent::__construct($request, $params);

        $user = user::getCurrent();

        $filename=$this->request["_filename"] ?? "zoph";
        if (!preg_match("/^[a-zA-Z0-9_-]+$/", $filename)) {
            throw new filenameException("Invalid filename");
        }

        $filenum=(int) ($this->request["_filenum"] ?? 1);
        $this->filename=$filename . "_" . $filenum . ".zip";

        $this->zipfile="/tmp/zoph_" . (int) $user->getId() . "_" . $this->filename;
    }

    /**
     * Output view
     */
    public function view() {
        if (file_exists($this->zipfile)) {
            readfile($this->zipfile);
            unlink($this->zipfile);
        } else {
            throw new fileNotFoundException("Could not read " . $this->zipfile);
        }
    }

   /**
    * Return customized headers
    * @return array headers
    */
    public function getHeaders() {
        $size = filesize($this->zipfile);
        if (!file_exists($this->zipfile)) {
            throw new fileNotFoundException("Could not read " . $this->zipfile);
        }
        return array(
            "Content-Description: File Transfer",
            "Content-Type: application/zip",
            "Content-Disposition: attachment; filename=\"" . $this->filename . "\"",
            "Content-Transfer-Encoding: binary",
            "Expires: 0",
            "Cache-Control: no-cache",
            "Content-Length: " . $size
        );
    }
}
