<?php
/**
 * View for download photos page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use conf\conf;
use photo;
use photo\collection as photoCollection;
use template\block;
use template\form;
use template\template;
use web\request;

use user;

/**
 * This view displays the "download photos" page
 */
class download extends view {
    /**
     * Output view
     */
    public function view() {
        if (empty($this->photos)) {
            $tpl = new template("main-nh", array(
                "title" => translate("Download")
            ));

            $tpl->addBlock(new block("message", array(
                "class"     => "error",
                "text"      => translate("No photos were found matching your search criteria.")
            )));
        } else {
            $tpl=new template("main-nh", array(
                "title"             => $this->params->titleBar,
            ));

            $form = new form("form", array(
                "warning"       => $this->getInfoBlock(),
                "class"         => "download",
                "formAction"    => "photos.php",
                "action"        => "prepare",
                "submit"        => translate("download"),
                "submit_class"  => "",
                "onsubmit"      => ""
            ));

            $form->addHiddenFields($this->request->getRequestVars(), array("_off", "_action"));

            $form->addInputText("_filename", "zoph", "filename", "Use alphanumeric, - and _. Do not provide an extension.");

            $form->addPulldown("_maxfiles", helpers::getMaxFilesDropdown(), translate("Maximum number of files per zipfile"));
            $form->addPulldown("_maxsize", helpers::getMaxSizeDropdown(), translate("Maximum size per zipfile"));

            $tpl->addBlock($form);
        }

        return $tpl;
    }

    /**
     * Get message block with data about download
     * @return template\block data block
     */
    private function getInfoBlock() {
        return new block("message", array(
            "class" => "info",
            "text"  => sprintf(translate("You have requested the download of %s photos, " .
                        "with a total size of  %s."), sizeof($this->photos),
                        template::getHumanReadableBytes(photo::getFilesize($this->photos)))
        ));
    }


}
