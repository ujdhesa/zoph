<?php
/**
 * View for download photos page - prepare
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use breadcrumb;
use conf\conf;
use photo;
use photo\collection as photoCollection;
use template\block;
use template\form;
use template\template;
use user;
use web\request;

/**
 * This view prepares the zip file and displays the  "next download" form
 */
class prepare extends view {
    /** @var string filename of zipfile as downloaded */
    private $filename;

    /** @var int number current file */
    private $filenum;

    /** @var string filename of zipfile as stored in /tmp */
    private $zipfile;

    /** @var int number of files added to the last zip */
    private $added;

    /**
     * Set the file to be used for download
     * @param string file name
     * @param int file number
     * @param int number of files added in the previous run
     */
    public function setFile(string $filename, int $filenum, int $added) {
        $user = user::getCurrent();

        $this->filename=$filename;
        $this->filenum=$filenum;

        $this->zipfile="/tmp/zoph_" . (int) $user->getId() . "_" . $this->filename . "_" . $filenum . ".zip";

        $this->added = $added;
    }

    /**
     * Output view
     */
    public function view() {
        if (empty($this->display)) {
            return $this->displayError();
        } else {
            $offset = (int) $this->request["_off"] ?? 0;
            $newoffset = $offset . $this->added;

            if ($newoffset < sizeof($this->photos)) {
                return $this->displayNext($newoffset);
            } else {
                return $this->displayNext();
            }
        }
    }

    /**
     * Display error in case no photos were matched for the search criteria
     * @return template\template error display
     */
    private function displayError () {
        $tpl = new template("main-nh", array(
            "title" => translate("Download")
        ));

        $tpl->addBlock(new block("message", array(
            "class"     => "error",
            "text"      => translate("No photos were found matching your search criteria.")
        )));

        return $tpl;
    }

    /**
     * Display next download screen
     * @param int|null offset for next download or null if this is the last download
     * @return template\template display next download
     */
    private function displayNext (int $newoffset = null) {
        $tpl=new template("main-nh", array(
            "title"             => $this->params->titleBar
        ));

        if ($newoffset) {
            $vars = $this->request->getUpdatedVars(array(
                "_off"      => $newoffset,
                "_filenum"  => $this->filenum + 1
            ));

            $msg = new block("message", array(
                "class" => "info",
                "text"  => translate("The zipfile is being created...") . " " .
                    sprintf(translate("Downloaded %s of %s photos."), $newoffset, sizeof($this->photos))
            ));
            $msg->addActionlinks(array(translate("download next file") => "photos.php?" . http_build_query($vars)));
        } else {
            $msg = new block("message", array(
                "class"     => "info",
                "text"      => sprintf(translate("All photos have been downloaded in %s zipfiles."), $this->filenum)
            ));
            $vars = $this->request->getUpdatedVars(array(), null, array("_action", "_off", "_filenum", "_maxfiles", "_maxsize", "_filename"));
            $msg->addActionlinks(array(translate("Go back") => "photos.php?" . http_build_query($vars)));
        }
        $tpl->addBlock($msg);
        $tpl->addBlock(new block("iframe", array(
            "class"         => "download",
            "iframe_src"    => "photos.php?_action=zipfile&_filename=" . $this->filename . "&_filenum=" . $this->filenum,
            "title"         => "download"
        )));

        return $tpl;
    }
}
