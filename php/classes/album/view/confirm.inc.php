<?php
/**
 * View for confirm delete album
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace album\view;

use template\template;
use web\request;
use photo;
use user;
use album;

/**
 * This view displays the "confirm delete album" page
 */
class confirm extends view {


    /**
     * Get actionlinks
     */
    public function getActionlinks() {
        return array(
            translate("confirm")   =>  "album.php?_action=confirm&amp;album_id=" . $this->album->getId(),
            translate("cancel")    =>  "albums.php?parent_album_id=" . $this->album->getId()
        );

    }
    /**
     * Output view
     */
    public function view() {
        return new template("confirm", array(
            "title"     =>  translate("delete album"),
            "actionlinks"   => $this->getActionlinks(),
            "mainActionlinks" => null,
            "obj"           => $this->album,
        ));

    }
}
