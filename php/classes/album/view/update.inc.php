<?php
/**
 * View for edit album page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace album\view;

use conf\conf;
use template\template;
use template\block;
use web\request;
use permissions\view\edit as editPermissions;

use album;
use user;

/**
 * This view displays the album page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() {
        if ($this->request["parent_album_id"] != 0) {
            $returnURL = "albums.php?parent_album_id=" . $this->request["parent_album_id"];
        } else if ($this->album->getId() != 0) {
            $returnURL = "album.php?album_id=" . $this->album->getId();
        } else {
            $returnURL = "albums.php";
        }

        if ($this->request["_action"] == "new") {
            return array(
                translate("return")    => $returnURL
            );
        } else {
            return array(
                translate("return")    => $returnURL,
                translate("new")       => "album.php?_action=new&amp;parent_album_id=" . $this->album->getId(),
                translate("delete")    => "album.php?_action=delete&amp;album_id=" . $this->album->getId()
            );
        }
    }

    /**
     * Output the view
     */
    public function view() {
        $user = user::getCurrent();

        $actionlinks=$this->getActionlinks();

        if ($this->request["_action"] == "new") {
            $action = "insert";

            $permissions=new block("message", array(
                "class" => "info",
                "text" => translate("After this album has been created, groups can be given access to it.")
            ));
        } else if ($this->request["_action"] == "edit") {
            $action = "update";
            $editPermissions=new editPermissions($this->album);
            $permissions=$editPermissions->view();
        } else {
            // Safety net. This should not happen.
            $action = $this->request["_action"];
            $permissions="";
        }

        $tpl = new template("editAlbum", array(
            "actionlinks"   => $actionlinks,
            "action"        => $action,
            "album"         => $this->album,
            "title"         => $this->getTitle(),
            "user"          => $user,
            "permissions"   => $permissions
        ));
        return $tpl;
    }

}
