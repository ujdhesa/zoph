<?php
/**
 * View for display album page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace album\view;

use web\request;
use album;

/**
 * This view redirects the user to albums.php, as the album.php page is only used to for create and edit
 */
class display extends redirect {
    /**
     * Create view
     * @param request web request
     * @param album the album we are dealing with in this page
     */
    public function __construct(request $request, album $album) {
        parent::__construct($request, $album);
        $this->setRedirect("albums.php?parent_album_id=" . $album->getId());
    }
}
