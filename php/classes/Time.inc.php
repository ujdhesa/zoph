<?php
/**
 * Time class, extension of the standard PHP DateTime class
 * Changes the __construct() to validate the timezone.
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

use conf\conf;
use template\block;

/**
 * Time class, extension of the standard PHP DateTime class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class Time extends DateTime {

    /** @var boolean is the date a fallback to the default (today)?
                     if so, we don't want to display it in all cases... */
    private $today = false;

    /**
     * Create Time object
     * @param string Date and time
     * @param string Timezone
     * @return Time time object
     */
    public function __construct($datetime=null, $tz=null) {
        $this->today=empty($datetime);
        try {
            if ($tz instanceof TimeZone && TimeZone::validate($tz->getName())) {
                parent::__construct($datetime,$tz);
            } else {
                parent::__construct($datetime);
            }
        } catch (Exception $e){
            echo "<b>Invalid time</b><br>";
            log::msg("<pre>" . $e->getMessage() . "</pre>", log::DEBUG, log::GENERAL);
        }
    }

    /**
     * Get only the formatted date portion of the datetime
     * @return string date
     */
    public function getDate() {
        return $this->format("Y-m-d");
    }

    /**
     * Get only the formatted date portion of the datetime
     * formatted according to the settings in the configuration
     * @return string date
     */
    public function getFormatted() {
        return $this->format(conf::get("date.format"));
    }

   /**
    * Get a link to the date
    * @param string field to search for instead of the default 'date'
    * @return template/block link
    */
    public function getLink($field = "date") {
        if (!$this->today) {
            return new block("link", array(
                "link"  => $this->getFormatted(),
                "href"  => "calendar.php?date=" . $this->getDate() .
                           "&amp;search_field=" . $field,
                "target" => ""
            ));
        }
    }

}



?>
