<?php
/**
 * Controller for groups
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group;

use generic\controller as genericController;
use group;
use web\request;
use user;

/**
 * Controller for groups
 */
class controller extends genericController {

    /** @var string Where to redirect after actions */
    public $redirect="groups.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        $group = new group($this->request["group_id"]);
        $group->lookup();
        $this->setObject($group);
        $this->doAction();
    }

    protected function actionDisplay() {
        $this->view = new view\display($this->request, $this->object);
    }

    protected function actionDelete() {
        $this->view = new view\confirm($this->request, $this->object);
    }

    protected function actionEdit() {
        $this->view = new view\update($this->request, $this->object);
    }

    protected function actionNew() {
        $this->view = new view\update($this->request, $this->object);
    }

    protected function actionConfirm() {
        parent::actionConfirm();
        $this->view = new view\redirect($this->request, $this->object);
        $this->view->setRedirect("groups.php");
    }

    /**
     * Action: update
     * The update action processes a form as generated after the "edit" action.
     * The subsequently called view displays the object.
     * takes care of adding and removing members of the group
     */
    protected function actionUpdate() {
        $this->object->setFields($this->request->getRequestVars());
        if (isset($this->request["_member"]) && ((int) $this->request["_member"] > 0)) {
            $this->object->addMember(new user((int) $this->request["_member"]));
        }

        if (is_array($this->request["_removeMember"])) {
            foreach ($this->request["_removeMember"] as $user_id) {
                $this->object->removeMember(new user((int) $user_id));
            }
        }
        $this->object->update();
        $this->view = new view\update($this->request, $this->object);
    }

    /**
     * Action: insert
     * The insert action processes a form as generated after the "new" action.
     * The subsequently called view displays a form to make more changes to the group.
     * this is a change from the generic controller, because group access rights can only
     * be modified after insertion.
     */
    protected function actionInsert() {
        parent::actionInsert();
        $this->view = new view\update($this->request, $this->object);
    }
}
