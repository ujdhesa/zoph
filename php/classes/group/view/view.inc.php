<?php
/**
 * Base class for group views
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group\view;

use template\template;
use web\request;
use group;

/**
 * Holds the common parts for the group views
 */
abstract class view {
    /** @var request hold the request for this view */
    protected $request;
    /** @var group the group that is going to be deleted */
    protected $group;

    /**
     * Create view
     * @param request web request
     * @param group group
     */
    public function __construct(request $request, group $group) {
        $this->request = $request;
        $this->group = $group;
    }

    /**
     * Get headers - null, so default headers will be used
     * @return null
     */
    public function getHeaders() {
        return null;
    }

    /**
     * Get title
     * @return string title
     */
    public function getTitle() {
        return $this->group->getName();
    }

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    abstract protected function getActionlinks();

    /**
     * Output view
     * @return template
     */
    abstract public function view();


}
