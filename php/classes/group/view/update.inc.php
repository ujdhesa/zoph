<?php
/**
 * View to update group
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group\view;

use conf\conf;
use group;
use permissions\view\edit as editPermissions;
use template\block;
use template\form;
use template\template;
use web\request;

/**
 * Update screen for group
 */
class update extends view {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() {
        if ($this->request["_action"] == "new") {
            return array(
                translate("delete")    => "group.php?_action=delete&amp;group_id=" . $this->group->getId(),
                translate("return")    => "groups.php"
            );
        } else {
            return array(
                translate("delete")    => "group.php?_action=delete&amp;group_id=" . $this->group->getId(),
                translate("new")       => "group.php?_action=new",
                translate("return")    => "group.php?group_id=" . $this->group->getId()
            );
        }
    }


    /**
     * Get view
     * @return template view
     */
    public function view() {
        $_action = $this->request["_action"];

        $tpl=new template("edit", array(
            "title"             => $this->getTitle(),
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => null,
            "obj"               => $this->group,
        ));

        $form=new form("form", array(
            "formAction"        => "group.php",
            "onsubmit"          => null,
            "action"            => $_action == "new" ? "insert" : "update",
            "submit"            => translate("submit")
        ));

        $form->addInputHidden("group_id", $this->group->getId());

        $form->addInputText("group_name", $this->group->getName(), translate("group name"),
            sprintf(translate("%s chars max"), 32), 32);

        $form->addInputText("description", $this->group->get("description"),
            translate("description"), sprintf(translate("%s chars max"), 128), 128, 32);

        if ($_action != "new") {
            $curMembers=$this->group->getMembers();
            $members=new block("members", array(
                "members"   => $curMembers,
                "group"     => $this->group
            ));
            $form->addBlock($members);

            $tpl->addBlock($form);

            $view=new editPermissions($this->group);
            $tpl->addBlock($view->view());

        } else {
            $tpl->addBlock(new block("message", array(
                "class" => "info",
                "text" => translate("After this group is created it can be given access to albums.")
            )));
            $tpl->addBlock($form);
        }

        return $tpl;
    }

    public function getTitle() {
        if ($this->request["_action"] == "new") {
            return translate("New group");
        } else {
            return parent::getTitle();
        }

    }
}
