<?php
/**
 * View to display group
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group\view;

use conf\conf;
use group;
use template\template;
use web\request;

/**
 * Display screen for group
 */
class display extends view {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() {
        return array(
            translate("edit")      => "group.php?_action=edit&amp;group_id=" . $this->group->getId(),
            translate("delete")    => "group.php?_action=delete&amp;group_id=" . $this->group->getId(),
            translate("new")       => "group.php?_action=new",
            translate("return")    => "groups.php"
        );
    }


    /**
     * Get view
     * @return template view
     */
    public function view() {
        return new template("displayGroup", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $this->getActionlinks(),
            "obj"           => $this->group,
            "view"          => "album",
            "fields"        => $this->group->getDisplayArray(),
            "watermark"     => conf::get("watermark.enable"),
            "permissions"   => $this->group->getPermissionArray()
        ));
    }
}
