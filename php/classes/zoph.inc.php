<?php
/**
 * Zoph class - a class that holds generic information about Zoph
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

use template\template;

/**
 * Zoph class - a class that holds generic information about Zoph
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class zoph {

    /**
     * Get an array with credits
     * @return array array with credits.
     */
    public static function getCredits() {
        return array(
            "Joaquim Azevedo"           => "Portuguese translation",
            "David Baldwin"             => "PHP 4.2 compatibility and fixes",
            "Chris Beauchamp"           => "PostgresSQL diff",
            "Edelhard Becker"           => "Debian packages",
            "Alexandr Bondarev"         => "Russian translation",
            "Roy Bonser"                => "Patches for web import function and major improvements of seach page.",
            "Charles Brunet"            => "Various bugfixes",
            "Nixon Childs"              => "Annotated photos",
            "Francesco Ciattaglia"      => "Italian translation",
            "Mark Cooper"               => "rpm packages, makefile, man page and fixes",
            "Alvaro Gonz&aacute;lez Crespo" => "Spanish translation",
            "Nils Decker"               => "zophEdit script",
            "Antoine Delvaux"           => "Updated French translation, EXIF patch and zoph.org domain",
            "Dominique Dumon"           => "improvements",
            "Mufit Eribol"              => "Turkish translation",
            "Peter Farr"                => "zophImport.pl patches",
            "Pontus Fröding"            => "Bugfixes",
            "<a href=\"http://www.geonames.org\">Geonames project</a>" => "Coordinates to timezone lookup",
            "Donald Gover"              => "bugfixes",
            "Michael Hanke"             => "fixes",
            "Christian Hoenig"          => "improvements",
            "Raimund Hook"              => "Bugfix",
            "Jason (JiCit)"             => "Bugfixes",
            "Neels Jordaan"             => "Afrikaans translation",
            "Francisco Javier F&eacute;lix" => "Updated Spanish translation, various bugfixes",
            "Samuel Keim"               => "email notification, last login &amp; ip, PHP validation, improvements",
            "Ian Kerr"                  => "fixes, Canadian English translation",
            "Krzysztof Kajkowski"       => "Polish translation",
            "S&#322;awomir Kubiak"      => "Updated Polish translation",
            "David Kulp"                => "bugfixes",
            "Pekka Kutinlati"           => "Finnish Translation, bugfix",
            "Tetsuji Kyan"              => "bugfixes",
            "Patrick Lam"               => "Various improvements",
            "Asheesh Laroia"            => "htpasswd authentication, fixes",
            "Johan Linder"              => "Updated Swedish translation",
            "John Lines"                => "location lookup feature",
            "Mat Lee"                   => "Traditional Chinese translation",
            "Haavard Leonardo Lund"     => "Norwegian translation",
            "Matthew MacIntyre"         => "bugfixes",
            "Mikael Magnusson"          => "Swedish translation",
            "<a href=\"http://leafletjs.com\">Leaflet project</a>" => "Leaflet mapping project",
            "Iv&aacute;n S&aacute;nchez Ortega" => "Leaflet Google Plugin",
            "Neil McBride"              => "Adding multiple people at once",
            "Jan Miczaika"              => "importer, multiple ratings",
            "Francisco J. Montilla"     => "bugfixes and improvements",
            "Giles Morant"              => "Movie import script",
            "David Moulton"             => "improvements",
            "<a href=\"https://github.com/nuflix\">Nuflix</a>" => "<a href=\"https://github.com/nuflix/Free-Javascript-Star-Rating-System\">Icons for rating stars</a>",
            "Aaron Parecki"             => "SSL Login",
            "Mario Peter"               => "German translation",
            "&quot;Prince01&quot;"      => "Hebrew translation",
            "Curtis Rawls"              => "Bugfixes",
            "Oliver Seidel"             => "Hierarchical directories in zophImport.pl",
            "Eric Seigne"               => "internationalization, French translation",
            "Sergey Chursin"            => "Russian translation",
            "Jesper Skytte"             => "Danish Translation",
            "Alan Shutko"               => "improvements",
            "Jason Taylor"              => "Various bugfixes",
            "Arjen Tebbenhof"           => "Dutch translation",
            "Hans Verbrugge"            => "Bugfix"
        );
    }
}

