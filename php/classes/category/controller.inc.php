<?php
/**
 * Controller for category
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category;

use conf\conf;
use generic\controller as genericController;
use web\redirect;
use web\request;

use breadcrumb;
use log;
use category;
use rating;
use user;

use categoryNotAccessibleSecurityException;


/**
 * Controller for category
 */
class controller extends genericController {

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "new", "insert", "confirm", "delete", "display", "edit", "update", "coverphoto", "unsetcoverphoto"
    );

    /** @var string Where to redirect after actions */
    public $redirect="categories.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        if ($request->_action=="new") {
            $category=new category();
            if (isset($this->request["parent_category_id"])) {
                $category->set("parent_category_id", $this->request["parent_category_id"]);
            }
            $this->setObject($category);
            $this->doAction();
        } else {
            try {
                $category=$this->getCategoryFromRequest();
            } catch (categoryNotAccessibleSecurityException $e) {
                log::msg($e->getMessage(), log::WARN, log::SECURITY);
                $category=null;
            }
            if ($category instanceof category) {
                $this->setObject($category);
                $this->doAction();
            } else {
                $this->view = "notfound";
            }
        }
    }

    /**
     * Get the category based on the query in the request
     * @throws categoryNotAccessibleSecurityException
     */
    private function getCategoryFromRequest() {
        $user=user::getCurrent();
        if (isset($this->request["category_id"])) {
            $category = new category($this->request["category_id"]);
            $category->lookup();
        } else {
            $category=category::getRoot();
        }
        if ($user->isAdmin() || $user->getCategoryPermissions($category)) {
            return $category;
        }
        throw new categoryNotAccessibleSecurityException(
            "Security Exception: category " . $category->getId() .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        if (user::getCurrent()->canDeletePhotos()) {
            parent::actionConfirm();
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        if (user::getCurrent()->canDeletePhotos()) {
            parent::actionDelete();
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $user = user::getCurrent();
        $this->view="display";
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->view="update";
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'new'
     */
    public function actionInsert() {
        parent::actionInsert();
        $this->redirect="category.php?_action=edit&category_id=" . $this->object->getId();
        $this->view="redirect";
    }

    /**
     * Do action 'new'
     */
    public function actionNew() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            $this->view="update";
        } else {
            $this->view="display";
        }
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
            redirect::redirect("category.php?action=edit&amp;category_id=" . $this->object->getId(), "Update done");
        }
        $this->view="display";
    }

    public function actionCoverphoto() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user) && isset($this->request["coverphoto"])) {
            $this->object->set("coverphoto", (int) $this->request["coverphoto"]);
            $this->object->update();
        }
        $this->view="redirect";
    }

    public function actionUnsetcoverphoto() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->object->set("coverphoto", null);
            $this->object->update();
        }
        $this->view="redirect";
    }

}
