<?php
/**
 * View for confirm delete category
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use template\template;
use web\request;
use photo;
use user;
use category;

/**
 * This view displays the "confirm delete category" page
 */
class confirm extends view {


    /**
     * Get actionlinks
     */
    public function getActionlinks() {
        return array(
            translate("confirm")   =>  "category.php?_action=confirm&amp;category_id=" . $this->category->getId(),
            translate("cancel")    =>  "categories.php?parent_category_id=" . $this->category->getId()
        );

    }
    /**
     * Output view
     */
    public function view() {
        return new template("confirm", array(
            "title"     =>  translate("delete category"),
            "actionlinks"   => $this->getActionlinks(),
            "mainActionlinks" => null,
            "obj"           => $this->category,
        ));

    }
}
