<?php
/**
 * View for redirect page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use web\request;
use web\redirect as webRedirect;
use category;

/**
 * This view redirects the user to another page
 */
class redirect {
    /**
     * @var string the URL to redirect to
     */
    private $redirect;

    /**
     * Create view
     * @param request web request
     * @param category the category we are dealing with in this page
     */
    public function __construct(request $request, category $category) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->category=$category;
    }

    /**
     * Output view
     */
    public function view() {
        webRedirect::redirect($this->redirect, "Redirect");
    }

    /**
     * Set the page to redirect to
     * @param string redirect target
     */
    public function setRedirect($redirect) {
        $this->redirect=$redirect;
    }
}
