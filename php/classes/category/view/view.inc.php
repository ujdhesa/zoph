<?php
/**
 * Common parts for person view
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use template\template;
use web\request;

use user;
use category;

/**
 * This is a base view for the category page, that contains common parts for all views
 */
abstract class view {

    /** * @var array request variables */
    protected $vars;
    /** * @var request web request */
    protected $request;
    /** * @var category */
    protected $category;

    /**
     * Create view
     * @param request web request
     * @param person the person that this page is dealing with
     */
    public function __construct(request $request, category $category) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->category=$category;
    }

    /**
     * Get actionlinks
     */
    abstract protected function getActionlinks();

    /**
     * Output view
     */
    abstract public function view();

    /**
     * Get the title for this view
     */
    public function getTitle() {
        return translate($this->request["_action"] . " category");
    }
}
