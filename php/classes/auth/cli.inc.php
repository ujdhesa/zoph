<?php
/**
 * Check if user is logged in, or perform authentication
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * This file lets a user pass through if one of the following is true:
 * - a valid username/password was given
 * - a $user object was found in the session
 * - a default user has been defined in config.inc.php
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

namespace auth;

use conf\conf;
use user;

class cli implements auth {

    private $user;
    private $lang;

    /**
     * Create auth/cli object
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public function __construct() {
        if (conf::get("interface.user.cli")!=="0") {
            $user=new user(conf::get("interface.user.cli"));
        } else {

            $username=$_SERVER["USER"];
            try {
                $user=user::getByName($username);
            } catch (\userNotFoundException $e) {
                throw new \cliUserNotValidException($username . " is not a valid user");
            }
        }
        $user->lookup();
        $user->lookupPerson();
        $user->lookupPrefs();
        $user->prefs->load();
        $lang=$user->loadLanguage();
        $this->user=$user;
        $this->lang=$lang;
    }

    public function getUser() {
        return $this->user;
    }

    public function getLang() {
        return $this->lang;
    }

    public function getRedirect() {
        return;
    }
}

?>
