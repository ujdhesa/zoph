<?php
/**
 * View for confirm delete color scheme
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme\view;

use template\template;
use template\colorScheme;
use web\request;

/**
 * This view displays the "confirm color scheme" page
 */
class confirm {

    /** * @var array request variables */
    private $vars;
    /** * @var request web request */
    private $request;
    /** * @var colorScheme */
    private $colorScheme;

    /**
     * Create view
     * @param request web request
     * @param person person to operate on
     */
    public function __construct(request $request, colorScheme $colorScheme) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->colorScheme=$colorScheme;
    }

    /**
     * Output view
     */
    public function view() {
        $actionlinks=array(
            "confirm"   =>  "color_scheme.php?_action=confirm&amp;color_scheme_id=" .
                $this->colorScheme->getId(),
            "cancel"    =>  "color_schemes.php"
        );
        return new template("confirm", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $actionlinks,
            "mainActionlinks" => null,
            "obj"           => $this->colorScheme,
        ));

    }

    public function getTitle() {
        return translate("Confirm delete ") . $this->colorScheme->getName();
    }
}
