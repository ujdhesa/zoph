<?php
/**
 * View for display colorScheme page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme\view;

use conf\conf;
use template\colorScheme;
use template\block;
use template\template;
use web\request;

use user;

/**
 * This view displays the "colorScheme" page
 */
class display {
    /** * @var array request variables */
    protected $vars;
    /** * @var request web request */
    protected $request;
    /** * @var colorScheme */
    protected $colorScheme;

    /**
     * Create view
     * @param web\request web request
     * @param template\colorScheme the color scheme that this page is dealing with
     */
    public function __construct(request $request, colorScheme $colorScheme) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->colorScheme=$colorScheme;
    }
    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() {
        if  (user::getCurrent()->isAdmin()) {
            return array(
                translate("edit")   => "color_scheme.php?_action=edit&amp;color_scheme_id=" .
                    $this->colorScheme->getId(),
                translate("copy") => "color_scheme.php?_action=copy&amp;color_scheme_id=" .
                    $this->colorScheme->getId(),
                translate("delete") => "color_scheme.php?_action=delete&amp;color_scheme_id=" .
                    $this->colorScheme->getId(),
                translate("new")    => "color_scheme.php?_action=new"
            );
        } else {
            return array();
        }
    }

    /**
     * Output view
     */
    public function view() {
        $tpl = new template("main", array(
            "title" =>  $this->getTitle()
        ));

        $tpl->addBlock(new block("displayColorScheme", array(
            "colors"    =>  $this->colorScheme->getDisplayArray()
        )));

        $tpl->addActionlinks($this->getActionlinks());

        return $tpl;

    }

    public function getTitle() {
        return $this->colorScheme->getName();
    }
}
