<?php
/**
 * View for edit colorScheme page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme\view;

use template\block;
use template\colorScheme;
use template\form;
use template\template;
use web\request;

use user;

/**
 * This view displays the color scheme page when editing
 */
class update {
    /**
     * Create view
     * @param request web request
     * @param colorScheme the color scheme that this page is dealing with
     */
    public function __construct(request $request, colorScheme $colorScheme) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
        $this->colorScheme=$colorScheme;
    }

    /**
     * Create the actionlinks for this page
     * @return array action links
     */
    protected function getActionlinks() {
        return array(
            "return"    => "color_schemes.php",
        );
    }

    /**
     * Output the view
     */
    public function view() {
        $user = user::getCurrent();

        $action = $this->vars["_action"];
        if ($action == "copy" || $action == "new") {
            $action = "insert";
        } else if ($action == "edit") {
            $action = "update";
        }

        $form = new form("form", array(
            "formAction"    => "color_scheme.php",
            "onsubmit"      => null,
            "action"        => $action,
            "submit"        => translate("save")
        ));

        $form->addInputHidden("color_scheme_id", $this->colorScheme->getId());

        $name = $this->colorScheme->getName();

        $form->addInputText("name", $name, "name", "", 64, 16);

        $form->addBlock(new block("editColorScheme", array(
            "colors"    => $this->colorScheme->getColors()
        )));

        $tpl = new template("main", array(
            "actionlinks"   => $this->getActionlinks(),
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($form);
        return $tpl;
    }

    public function getTitle() {
        if ($this->vars["_action"] == "new") {
            return translate("New Color Scheme");
        }
        return $this->colorScheme->getName();
    }

}
