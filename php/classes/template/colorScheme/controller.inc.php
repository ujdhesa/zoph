<?php
/**
 * Controller for colorScheme
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme;

use conf\conf;
use generic\controller as genericController;
use template\colorScheme;
use web\request;

use user;

/**
 * Controller for colorScheme
 */
class controller extends genericController {

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "edit", "insert",
        "new", "update", "copy"
    );

    private $title;

    /** @var string Where to redirect after actions */
    public $redirect="color_schemes.php";

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        $user=user::getCurrent();
        if (!$user->isAdmin()) {
            $this->actionDisplay();
        } else {
            parent::__construct($request);
            if ($request->_action=="new") {
                $this->setObject(new colorScheme());
                $this->doAction();
            } else {
                $colorScheme = new colorScheme($request["color_scheme_id"]);
                $colorScheme->lookup();
                $this->title=$colorScheme->get("name");
                $this->setObject($colorScheme);
                $this->doAction();
            }
        }
    }

   /**
    * Do action 'copy'
    */
    public function actionCopy() {
        $this->title = translate("Copy Color Scheme");
        $this->object->set("name", "copy of " . $this->object->get("name"));
        $this->object->set("color_scheme_id", 0);
        $this->view = "update";
    }

   /**
    * Do action 'update'
    */
    public function actionUpdate() {
        parent::actionUpdate();
        user::getCurrent()->prefs->load();
    }

}
