<?php
/**
 * Display places
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

use conf\conf;

use template\block;
use template\template;
use web\redirect;

require_once "include.inc.php";

$_view=getvar("_view");
if (empty($_view)) {
    $_view=$user->prefs->get("view");
}
$_autothumb=getvar("_autothumb");
if (empty($_autothumb)) {
    $_autothumb=$user->prefs->get("autothumb");
}

if (!$user->canBrowsePlaces()) {
    redirect::redirect("zoph.php");
}

$parent_place_id = getvar("parent_place_id");
if (!$parent_place_id) {
    $place = place::getRoot();
} else {
    $place = new place($parent_place_id);
}
$place->lookup();

if (!$place->isVisible()) {
    redirect::redirect("places.php");
}

try {
    $selection=new selection($_SESSION, array(
        "coverphoto"    => "place.php?_action=coverphoto&amp;place_id=" . $place->getId() . "&amp;coverphoto=",
        "return"        => "_return=places.php&amp;_qs=parent_album_id=" . $place->getId()
    ));
} catch (photoNoSelectionException $e) {
    $selection=null;
}

$obj=&$place;
$ancestors = $place->getAncestors();
$order = $user->prefs->get("child_sortorder");
$children = $place->getChildren($order);
$totalPhotoCount = $place->getTotalPhotoCount();
$photoCount = $place->getPhotoCount();

$title = $place->get("parent_place_id") ? $place->get("title") : translate("Places");

$ancLinks=array();
if ($ancestors) {
    while ($parent = array_pop($ancestors)) {
        $ancLinks[$parent->getName()] = $parent->getURL();
    }
}


$pagenum = getvar("_pageset_page");

require_once "header.inc.php";

try {
    $pageset=$place->getPageset();
    $page=$place->getPage($request_vars, $pagenum);
    $showOrig=$place->showOrig($pagenum);
} catch (pageException $e) {
    $showOrig=true;
    $page=null;
}

if (conf::get("maps.provider")) {
    $map=new geo\map();
    $map->setCenterAndZoomFromObj($place);
    $marker=$place->getMarker();
    if ($marker instanceof geo\marker) {
        $map->addMarker($marker);
    }
    $map->addMarkers($children);
}

$tpl=new template("organizer", array(
    "page"          => $page,
    "pageTop"       => $place->showPageOnTop(),
    "pageBottom"    => $place->showPageOnBottom(),
    "showMain"      => $showOrig,
    "title"         => $title,
    "ancLinks"      => $ancLinks,
    "selection"     => $selection,
    "coverphoto"    => $place->displayCoverPhoto(),
    "description"   => $place->get("place_description"),
    "view"          => $_view,
    "view_name"     => "Place view",
    "view_hidden"   => null,
    "autothumb"     => $_autothumb,
    "map"           => $map ?? null
));

$actionlinks=array();
if ($user->canEditOrganizers()) {
    $actionlinks=array(
        translate("new") => "place.php?_action=new&amp;parent_place_id=" . (int) $place->getId(),
        translate("edit") => "place.php?_action=edit&amp;place_id=" . (int) $place->getId(),
        translate("delete") => "place.php?_action=delete&amp;place_id=" . (int) $place->getId()
    );
    if ($place->get("coverphoto")) {
        $actionlinks[translate("unset coverphoto")]="place.php?_action=unsetcoverphoto&amp;place_id=" . (int) $place->getId();
    }
}
if ($user->canBrowseTracks()) {
    $actionlinks[translate("tracks")]="tracks.php";
}

$tpl->addActionlinks($actionlinks);

$sortorder = $place->get("sortorder");
$sort = $sortorder ? "&_order=" . $sortorder : "";

$tpl->addBlock(new block("photoCount", array(
    "tpc"       => $place->getTotalPhotoCount(),
    "totalUrl"  => "photos.php?location_id=" . $place->getBranchIds() . $sort,
    "pc"        => $place->getPhotoCount(),
    "url"       => "photos.php?location_id=" . $place->getId() . $sort
)));

$order = $user->prefs->get("child_sortorder");
$children = $place->getChildren($order);

if ($children) {
    $tpl->addBlock(new block("view_" . $_view, array(
        "id" => $_view . "view",
        "items" => $children,
        "autothumb" => $_autothumb,
        "topnode" => true,
        "links" => array(
            translate("view photos") => "photos.php?location_id="
        )
    )));
}


echo $tpl;
require_once "footer.inc.php";
?>
