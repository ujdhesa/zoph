<?php
/**
 * Check if user is logged in, or perform authentication
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * This file lets a user pass through if one of the following is true:
 * - a valid username/password was given
 * - a $user object was found in the session
 * - a default user has been defined in config.inc.php
 * @todo Should be moved inside a class
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

use conf\conf;
use auth\web;
use auth\cli;
use web\redirect;
use web\request;
use web\session;

$error="";
$_action="display";

if (!defined("CLI")) {
    $session= new session();
    $session->start();
    $request=request::create();
    $auth = new web($session, $request);

    if ($request["_action"] == "logout") {
        $auth->logout();
    }
} else {
    $auth = new cli();
}

$user = $auth->getUser();
$lang = $auth->getLang();

if ($user instanceof user) {
    user::setCurrent($user);
    $_action = $request["_action"];
}
if ($auth->getRedirect()) {
    redirect::redirect($auth->getRedirect());
}

?>
