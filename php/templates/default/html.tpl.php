<?php
/**
 * Template for full page;
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

if (!ZOPH) {
    die("Illegal call");
}
?>

<!DOCTYPE html>
<html lang="<?= $tpl_lang ?>" class="<?php echo $tpl_html_class ?>">
    <head>
        <?php foreach ($tpl_js as $js): ?>
            <script type='text/javascript' src="<?= $js ?>"></script>
        <?php endforeach ?>
        <?php foreach ($tpl_css as $css): ?>
            <link type='text/css' rel="stylesheet" href="<?= $css ?>">
        <?php endforeach ?>
        <?php if (!empty($tpl_style)):  ?>
            <style>
                <?= $tpl_style ?>
            </style>
        <?php endif ?>
        <?php if (!empty($tpl_script)):  ?>
            <script type='text/javascript'>
                <?= $tpl_script ?>
            </script>
        <?php endif ?>
        <title><?= $tpl_title ?></title>
    </head>
    <body>
        <?= $tpl_body; ?>
    </body>
</html>
