<?php
/**
 * Template for slideshow page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 * @todo This template still creates the h1 and div main tags itself.
 *       this should be done by the main template;
 */

use conf\conf;
use template\template;

if (!ZOPH) { die("Illegal call"); }
?>
    <h1>
        <?php echo translate("slideshow"); ?>
    </h1>
    <div class="main">
        <div class="photoframe">
            <img alt="fullscreen" id="butFullscreen">
            <nav class="slideshow">
                <div id="butPrev"></div>
                <div id="butPause"></div>
                <div id="butNext"></div>
            </nav>
            <img alt="photo" id="photo" class="photo">
        </div>
        <div id="annotate" class="slide-annotate">
            <dl class="photodata">
                <dt class="icon filename"><?= translate("filename") ?></dt>
        <dd id="slide-name"></dd>
                <dt class="icon title"><?= translate("title") ?></dt>
                <dd id="slide-title"></dd>
                <dt class="icon datetime"><?= translate("datetime") ?></dt>
        <dd id="slide-datetime"></dd>
                <dt class="icon photographer"><?= translate("photographer") ?></dt>
                <dd id="slide-photographer"></dd>
                <dt class="icon location"><?= translate("location") ?></dt>
                <dd id="slide-location"></dd>
                <dt class="icon people"><?= translate("people") ?></dt>
                <dd>
                  <ul id="slide-people"></ul>
        </dd>
                <dt class="icon album"><?= translate("albums") ?></dt>
        <dd>
                  <ul id="slide-albums"></ul>
        </dd>
                <dt class="icon category"><?= translate("categories") ?></dt>
        <dd>
                  <ul id="slide-categories"></ul>
        </dd>
        </dl>
            <h3 class="exif"><?= translate("EXIF Data") ?></h3>
            <dl id="slide-exifdata" class="exif"></ol>
        </div>
    </div>
    <script type="text/javascript">
        <?= $tpl_javascript ?>
    </script>
