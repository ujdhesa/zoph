<?php
/**
 * Template for edit category page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}

use conf\conf;
use template\template;

?>
<h1>
    <?= $this->getActionlinks($tpl_actionlinks) ?>
    <?= $tpl_title ?>
</h1>
<div class="main">
    <?= template::showJSwarning() ?>
    <form action="category.php" method="POST">
        <input type="hidden" name="_action" value="<?= $tpl_action ?>">
        <input type="hidden" name="category_id" value="<?= $tpl_category->get("category_id") ?>">
        <?= template::createInput("category", $tpl_category->get("category"), 64, translate("Category name"), 40) ?><br>
        <label for="parent_category_id">
            <?= translate("Parent category") ?>
        </label>
        <?php if ($tpl_category->isRoot()): ?>
            <?= translate("Categories") ?>
        <?php else: ?>
            <?= category::createPulldown("parent_category_id", $tpl_category->get("parent_category_id")) ?>
        <?php endif ?>
        <?= template::createInput("category_description", $tpl_category->get("category_description"), 128, translate("Category description"), 40) ?><br>
        <label for="parent_category_id">
            <?= translate("Page set") ?>
        </label>
        <?= template::createPulldown("pageset", $tpl_category->get("pageset"),
                        template::createSelectArray(pageset::getRecords("title"), array("title"), true)) ?>
        <?= template::createInput("sortname", $tpl_category->get("sortname"), 32, translate("Sort name"), 20) ?><br>
        <label for="sortorder">
            <?= translate("Category sort order") ?>
        </label>
        <?= template::createPhotoFieldPulldown("sortorder", $tpl_category->get("sortorder")) ?>



        <input type="submit" value="<?= translate($tpl_action, 0) ?>">
    </form>
</div>
