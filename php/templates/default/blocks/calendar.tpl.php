<?php
/**
 * Template to display a map.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

if (!ZOPH) { die("Illegal call"); }
?>
<nav class="calendarhdr">
  <ul>
    <li class="prev">
      <a href="<?= $tpl_prev ?>">
        <?= translate("Prev") ?>
      </a>
    </li>
    <li class="date">
      <h2><?= $tpl_header ?></h2>
    </li>
    <li class="next">
      <a href="<?= $tpl_next ?>">
        <?= translate("Next") ?>
      </a>
    </li>
  </ul>
</nav>
<ul class="thumbs calendar">
  <?php foreach ($tpl_days as $day): ?>
    <li class="thumb_album <?= $day["class"] ?>">
      <?php if ($day["link"]): ?>
        <a href="<?= $day["link"] ?>">
      <?php endif; ?>
      <?php if ($day["photo"]): ?>
        <?= $day["photo"] ?>
      <?php endif; ?>
      <div class="name">
        <?= $day["date"] ?>
        <?php if ($day["photocount"] > 0) : ?>
          <span class="photocount">
            (<?= $day["photocount"] ?>)
          </span>
        <?php endif; ?>
      </div>
      <?php if ($day["link"]): ?>
        </a>
      <?php endif; ?>
    </li>
  <?php endforeach; ?>
</ul>
