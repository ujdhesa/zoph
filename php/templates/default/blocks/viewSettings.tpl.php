<?php
/**
 * Template for view settings (rows/colums, sort order) used on photos page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}

use template\template;

?>
<form class="viewsettings" action="photos.php" method="GET">
    <?= $this->displayBlocks(); ?>
    <div id="sortorder">
        <?= translate("order by", 0) ?>
        <?= $tpl_order ?>
    </div>
    <div id="updown">
        <a href="<?= $tpl_sortupurl ?>">
            <img class="up" alt="sort ascending" src="<?= $tpl_sortup ?>">
        </a>
        <a href="<?= $tpl_sortdownurl ?>">
            <img class="down" alt="sort descending" src="<?= $tpl_sortdown ?>">
        </a>
    </div>
    <div id="rowscols">
        <?= $tpl_rowsDropdown ?>
        <?= translate("rows") ?>
        <?= $tpl_colsDropdown ?>
        <?= translate("cols") ?>
        <input type="submit" name="_button" value="<?= translate("go", 0) ?>">
    </div>
</form>
<br>
