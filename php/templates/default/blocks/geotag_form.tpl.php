<?php
/**
 * Template for geotagging settings
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

use template\template;
?>
        <form class="geotag" action="tracks.php">
            <p>
                <?= sprintf(translate("Geotagging will make Zoph use GPS tracks to " .
                "determine the location where a photo was taken. You should import a " .
                "GPX file using the import function before using the Geotagging option. " .
                "Zoph will try to geotag %s photos."), $tpl_photoCount); ?>
            </p>
            <p>
                <?php foreach ($tpl_hidden as $key => $value): ?>
                <input type="hidden" name="<?= $key; ?>" value="<?= $value; ?>">
                <?php endforeach; ?>
            </p>
            <fieldset class="geotag">
                <legend><?= translate("Options"); ?></legend>
                <label for="maxtime">
                    <?= translate("Maximum time") ?>
                </label>
                <input type="text" id="maxtime" name="_maxtime" value="300">
                <?= translate("seconds"); ?>
                <div class="inputhint">
                  <?= translate("Maximum time difference between photo " .
                    "and GPS timestamp"); ?>
                </div><br>
                <label for="validtz"><?= translate("Valid timezone"); ?></label>
                <fieldset class="checkboxlist">
                    <legend><?= translate("Valid timezone"); ?></legend>

                    <input type="checkbox" id="validtz" name="_validtz" value="1" checked>
                        <?= translate("Only photos with a valid timezone"); ?><br>
                </fieldset>
                <label for="overwrite"><?= translate("Overwrite"); ?></label>
                <fieldset class="checkboxlist">
                    <legend><?= translate("Overwrite"); ?></legend>
                    <input type="checkbox" id="overwrite" name="_overwrite" value="1">
                      <?= translate("Overwrite existing geo-information"); ?><br>
                </fieldset>
                <label for="tracks"><?= translate("Tracks"); ?></label>
                <fieldset class="checkboxlist">
                    <legend><?= translate("Tracks"); ?></legend>
                    <input type="radio" name="_tracks" id="tracks" value="all" checked>
                    <?= translate("All tracks"); ?><br>
                    <input type="radio" name="_tracks" id="tracks2" value="specific">
                    <?= translate("Specific track") . ": " ?>
                    <?= template::createPulldown("_track", "",
                        template::createSelectArray($tpl_tracks, array("name"))) ?>
                </fieldset>
            </fieldset>
            <fieldset class="geotag">
                <legend><?= translate("Interpolation"); ?></legend>
                <label for="interpolate">
                    <?= translate("Interpolation") ?>
                </label>
                <fieldset class="checkboxlist">
                    <legend><?= translate("Interpolation"); ?></legend>
                    <input type="radio" name="_interpolate" id="interpolate" value="yes" checked>
                    <?= translate("Interpolate between points"); ?><br>
                    <input type="radio" name="_interpolate" id="interpolate2" value="nearest">
                    <?= translate("Use nearest point");  ?>
                </fieldset>
                <label for="intmaxdist">
                    <?= translate("Maximum distance") ?>
                </label>
                <input type="text" name="_int_maxdist" id="intmaxdist" value="1">
                <?= template::createPulldown("_entity","km", array(
                    "km"    => "km",
                    "miles" => "miles")) ?>
                <div class="inputhint">
                    <?= translate("Do not interpolate if distance between points is " .
                        "more than this"); ?>
                </div><br>
                <label for="intmaxtime">
                    <?= translate("Maximum time") ?>
                </label>
                <input type="text" name="_int_maxtime" id="intmaxtime" value="300">
                <?= translate("seconds"); ?>
                <div class="inputhint">
                    <?= translate("Do not interpolate if time between points is more " .
                        "than this"); ?>
                </div>
            </fieldset>
            <fieldset class="geotag">
                <legend><?= translate("Test"); ?></legend>
                <div class="formtext">
                    <?= translate("To ensure that the geotagging operation goes well, " .
                        "you can check the results of the geotagging before storing them in " .
                        "the database."); ?>
                </div>
                <label for="test">
                    <?= translate("Photos to test") ?>
                </label>
                <fieldset class="checkboxlist">
                    <legend><?= translate("Test"); ?></legend>
                    <input type="checkbox" name="_test[]" id="test" value="first">
                    <?= translate("first"); ?><br>
                    <input type="checkbox" name="_test[]" id="test2" value="last">
                    <?= translate("last"); ?><br>
                    <input type="checkbox" name="_test[]" id="test3" value="random">
                    <?= translate("random"); ?><br>
                </fieldset>
                <label for="testcount">
                    <?= translate("Number of each") ?>
                </label>
                <input type="text" id="testcount" name="_testcount" value="5">
            </fieldset>
            <br>
        <p>
            <input type="submit" value="<?= translate("geotag") ?>">
        </p>
    </form>
