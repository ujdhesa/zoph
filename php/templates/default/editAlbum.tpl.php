<?php
/**
 * Template for edit album page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}

use conf\conf;
use template\template;

?>
<h1>
    <?= $this->getActionlinks($tpl_actionlinks) ?>
    <?= $tpl_title ?>
</h1>
<div class="main">
    <?= template::showJSwarning() ?>
    <form action="album.php" method="POST">
        <input type="hidden" name="_action" value="<?= $tpl_action ?>">
        <input type="hidden" name="album_id" value="<?= $tpl_album->get("album_id") ?>">
        <?= template::createInput("album", $tpl_album->get("album"), 64, translate("Album name"), 40) ?><br>
        <label for="parent_album_id">
            <?= translate("Parent album") ?>
        </label>
        <?php if ($tpl_album->isRoot()): ?>
            <?= translate("Albums") ?>
        <?php else: ?>
            <?= album::createPulldown("parent_album_id", $tpl_album->get("parent_album_id")) ?>
        <?php endif ?>
        <?= template::createInput("album_description", $tpl_album->get("album_description"), 128, translate("Album description"), 40) ?><br>
        <label for="parent_album_id">
            <?= translate("Page set") ?>
        </label>
        <?= template::createPulldown("pageset", $tpl_album->get("pageset"),
                        template::createSelectArray(pageset::getRecords("title"), array("title"), true)) ?>
        <?= template::createInput("sortname", $tpl_album->get("sortname"), 32, translate("Sort name"), 20) ?><br>
        <label for="sortorder">
            <?= translate("Album sort order") ?>
        </label>
        <?= template::createPhotoFieldPulldown("sortorder", $tpl_album->get("sortorder")) ?>



        <input type="submit" value="<?= translate($tpl_action, 0) ?>">
    </form>
</div>
<div class="permissions">
    <?= $tpl_permissions ?>
</div>
