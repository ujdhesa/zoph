<?php
/**
 * Template for photo page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}

use conf\conf;
use template\template;

?>
<h1>
    <?= $this->getActionlinks($tpl_actionlinks) ?>
    <?= $tpl_title ?>
</h1>
<?php if ($tpl_selection): ?>
    <?= $tpl_selection ?>
<?php endif ?>

<div class="main">
    <?= template::showJSwarning() ?>
    <input type="hidden" name="photo_id" value="<?= $tpl_photo->getId() ?>">
    <nav class="photohdr">
        <ul>
            <!-- The following li's are formatted a bit odd, because they are hidden
                 using CSS :empty pseudoclass, which will trigger on whitespace too
                 so the if's are placed in such a way that when the if resolves to
                 false, there will be no whitespace and CSS hides it.
                 @todo, possibly we could see if we can move the styling to the <a>
                 element?
            // !-->
            <li class="prev"><?php if ($tpl_prev): ?>
                <a href="<?= $tpl_prev ?>">
                    <?= translate("Prev") ?>
                </a>
            <?php endif ?></li>
            <li class="up"><?php if ($tpl_up): ?>
                <a href="<?= $tpl_up ?>">
                    <?= translate("Up") ?>
                </a>
            <?php endif ?></li>
            <li class="next"><?php if ($tpl_next): ?>
                    <a href="<?= $tpl_next ?>">
                        <?= translate("Next") ?>
                    </a>
            <?php endif ?></li>
        </ul>
    </nav>
    <div class="photodata">
        <?= $tpl_full ?>:
        <?= $tpl_photo->get("width") ?> x <?= $tpl_photo->get("height") ?>,
        <?= $tpl_size ?>
    </div>
    <ul class="tabs">
        <?= $tpl_share ?>
        <?= $tpl_exifdetails ?>
    </ul>
    <?= $tpl_image ?>
    <?php if ($tpl_people): ?>
        <ul class="peoplerows">
            <?php foreach ($tpl_people as $row): ?>
                <li><ul class="peoplelinks">
                    <?php foreach ($row as $person): ?>
                        <li><a href="<?= $person->getURL() ?>"><?= $person->getShortName() ?></a></li>
                    <?php endforeach ?>
                </ul></li>
            <?php endforeach ?>
        </ul>
    <?php endif ?>
    <dl class="display photo">
        <?php foreach ($tpl_fields as $key => $val): ?>
            <?php if ($val): ?>
                <dt><?= e($key) ?></dt>
                <dd><?= $val ?></dd>
            <?php endif ?>
        <?php endforeach ?>
        <dt><?php echo translate("rating") ?></dt>
        <dd>
            <?= $tpl_rating ?>
            <?= $tpl_ratingForm ?>
        </dd>
        <?php if ($tpl_albums): ?>
            <dt><?= translate("albums") ?></dt>
            <dd><?= $tpl_albums ?></dd>
        <?php endif ?>
        <?php if ($tpl_categories): ?>
            <dt><?= translate("categories") ?></dt>
            <dd><?= $tpl_categories ?></dd>
        <?php endif ?>
        <dt><?= translate("last modified") ?></dt>
        <dd><a href="<?= $tpl_timestampURL ?>"><?= $tpl_timestamp ?></a></dd>
        <?php if ($tpl_description): ?>
            <div class="photodesc">
                <?= $tpl_description ?>
            </div><br>
        <?php endif ?>
        <?php if ($tpl_camInfo): ?>
            <?php foreach ($tpl_camInfo as $key => $val): ?>
                <?php if ($val): ?>
                    <dt><?= translate($key) ?></dt>
                    <dd><?= $val ?></dd>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif ?>
    </dl>
    <br>
</div>
<?php if ($tpl_related): ?>
    <?= $tpl_related ?>
<?php endif ?>
<?php if ($tpl_comments): ?>
    <div class="comments">
        <?= $tpl_comments ?>
    </div>
<?php endif ?>
