<?php
/**
 * Template for import page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 * @todo This template still creates the h1 and div main tags itself.
 *       this should be done by the main template;
 */

use conf\conf;
use template\template;

if (!ZOPH) { die("Illegal call"); }
?>
    <script type="text/javascript">
        <?= $tpl_javascript; ?>
    </script>

    <h1>
        <?= translate("import photos"); ?>
    </h1>
    <div class="main">
        <noscript>
          <div class="message warning">
            <img alt="warning" class="icon" src="<?= template::getImage("icons/warning.png") ?>">
            <?= translate("This page needs Javascript switched on and will not " .
                "function without it."); ?><br>
          </div>
       </noscript>
        <div class="import_uploads">
            <h2><?= translate("Upload photo",0);?></h2>
            <?= $tpl_upload ?>
        </div>
        <div id="import_details" class="import_details">
            <h2>
                <ul class="actionlink">
                    <li><a href="#" onClick="clr('import_details_text'); return false">
                        <?= translate("clear")?></a>
                    </li>
                </ul>
                <?= translate("Details",0);?>
            </h2>
            <div id="import_details_text">
            </div>
        </div>
        <div id="import_thumbs" class="import_thumbs">
            <h2>
                <?= translate("Uploaded photos",0);?>
            </h2>
            <div id="import_thumbnails">
                <ul class="actionlink">
                    <li><a href="#" onClick="zImport.selectAll(); return false">
                        <?= translate("select all")?></a>
                    </li>
                    <li><a href="#" onClick="zImport.toggleSelection(); return false">
                        <?= translate("toggle selection")?></a>
                    </li>
                    <li><a href="#" onClick="zImport.deleteSelected(); return false">
                        <?= translate("delete selected")?></a>
                    </li>
                </ul>
                <br>
            </div>
        </div>
        <div class="import">
            <h2><?= translate("Import",0);?></h2>
            <form id="import_form" class="import" onSubmit="zImport.importPhotos(); return false;">
                <?= template::createInput("_path", "", 64, translate("path"), 40) ?>
                <?php if (conf::get("import.dated")): ?>
                    <span class="inputhint">
                        <?= translate("Dated directory will be appended") ?>
                    </span>
                <?php endif ?>
                <br>
                <label for="album"><?= translate("albums") ?></label>
                <fieldset class="multiple">
                    <?= album::createPulldown("_album_id[0]") ?>
                </fieldset>
                <label for="category"><?= translate("categories") ?></label>
                <fieldset class="multiple">
                    <?= category::createPulldown("_category_id[0]") ?>
                </fieldset>
                <?= template::createInput("title", "", 64, translate("title"), 40) ?><br>
                <label for="location"><?= translate("location") ?></label>
                <?= place::createPulldown("location_id") ?><br>
                <?= template::createInput("view", "", 64,  translate("view"), 40) ?><br>
                <?= template::createInputDate("date", "", translate("date")) ?><br>
                <label for="rating"><?= translate("rating") ?></label>
                <?= rating::createPulldown() ?>
                <label for="people"><?= translate("people") ?></label>
                <fieldset class="multiple">
                    <?= person::createPulldown("_person_id[0]") ?>
                </fieldset>
                <label for="photographer"><?= translate("photographer") ?></label>
                <?= photographer::createPulldown("photographer_id") ?><br>
                <?= template::createInputNumber("level", "", translate("level"), 1, 10) ?><br>
                <label for="extrafields"><?= translate("extra fields") ?></label><br>
                <span class="inputhint"><?= translate("These settings will override EXIF data!") ?></span><br>
                <fieldset class="formhelper-multiple">
                    <fieldset class="import-extrafields">
                        <?= template::createImportFieldPulldown("_field[]", "") ?>
                        <?= template::createInput("field[]", "", 64, null, 32) ?>
                    </fieldset>
                </fieldset>
                <label for="description"><?= translate("description") ?></label>
                <textarea name="description" cols="40" rows="4"></textarea><br>
                <input id="import_submit" type="submit"
                    value="<?= translate("import", 0) ?>">
            </form>
        </div>
