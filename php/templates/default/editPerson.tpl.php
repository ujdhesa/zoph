<?php
/**
 * Template for edit person page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}

use conf\conf;
use template\template;

?>
<h1>
    <?= $this->getActionlinks($tpl_actionlinks) ?>
    <?= $tpl_title ?>
</h1>
<div class="main">
    <?= template::showJSwarning() ?>
    <form action="person.php" method="POST">
        <input type="hidden" name="_action" value="<?= $tpl_action ?>">
        <input type="hidden" name="person_id" value="<?= $tpl_person->get("person_id") ?>">
        <?= template::createInput("first_name", $tpl_person->get("first_name"), 32, translate("first name"), 32) ?>
        <?= template::createInput("middle_name", $tpl_person->get("middle_name"), 32, translate("middle name"), 32) ?>
        <?= template::createInput("last_name", $tpl_person->get("last_name"), 32, translate("last name"), 32) ?>
        <?= template::createInput("called", $tpl_person->get("called"), 16, translate("called"), 16, sprintf(translate("%s chars max"), "16")) ?>
        <label for="gender"><?= translate("gender") ?></label>
        <?= template::createPulldown("gender", $tpl_person->get("gender"),
        array("1" => translate("male",0), "2" => translate("female",0))) ?><br>
        <?= template::createInputDate("dob", $tpl_person->get("dob"), translate("date of birth")) ?><br>
        <?= template::createInputDate("dod", $tpl_person->get("dod"), translate("date of death")) ?><br>
        <?= template::createInputEmail("email", $tpl_person->get("email"), translate("email")) ?><br>
        <label for="home_id"><?= translate("home") ?></label>
        <?= place::createPulldown("home_id", $tpl_person->get("home_id")) ?><br>
        <label for="work_id"><?= translate("work") ?></label>
        <?= place::createPulldown("work_id", $tpl_person->get("work_id")) ?><br>
        <label for="mother_id"><?= translate("mother") ?></label>
        <?= person::createPulldown("mother_id", $tpl_person->get("mother_id")) ?><br>
        <label for="father_id"><?= translate("father") ?></label>
        <?= person::createPulldown("father_id", $tpl_person->get("father_id")) ?><br>
        <label for="spouse"><?= translate("spouse") ?></label>
        <?= person::createPulldown("spouse_id", $tpl_person->get("spouse_id")) ?><br>
        <label for="pageset"><?= translate("pageset") ?></label>
        <?= template::createPulldown("pageset", $tpl_person->get("pageset"),
          template::createSelectArray(pageset::getRecords("title"), array("title"), true)) ?><br>
        <label for="notes"><?= translate("notes") ?></label>
        <textarea name="notes" cols="40" rows="4">
        <?= $tpl_person->get("notes") ?>
        </textarea><br>
        <input type="submit" value="<?= translate($tpl_action, 0) ?>">
    </form>
</div>
