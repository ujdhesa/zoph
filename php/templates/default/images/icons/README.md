Share Icon
==========

The 'share' icon was taken from the 'share icon' project.
http://shareicons.com.

share-icon-24x24.png

BC Tango KDE Icons
==================
These icons were taken from the BC Tango KDE iconset which can
be downloaded from
http://www.kde-look.org/content/show.php/BC+Tango+KDE?content=50036.

license for these icons:
Creative Commons attribution share-alike:
http://creativecommons.org/licenses/by-sa/2.5/

- 16x16/actions/1downarrow.png	1downarrow.png
- 16x16/actions/1rightarrow.png	1rightarrow.png
- 16x16/actions/ktremove.png	remove.png
- 16x16/actions/forward.png	right.png
- 16x16/actions/back.png		left.png
- 16x16/actions/up.png		up.png
- 16x16/actions/down.png		down.png
- 16x16/actions/list-add.png	add.png
- 22x22/actions/attach.png	link.png
- 22x22/actions/favorites.png 	rating.png
- 22x22/apps/office-calendar.png  date.png
- 22x22/apps/web-browser.png	geo-place.png
- 22x22/categories/package_engineering.png	modified.png
- 22x22/devices/camera.png	geo-photo.png
- 22x22/filesystems/folder.png	folder.png
- 22x22/mimetypes/html.png	html.png
- 22x22/mimetypes/image.png	photo.png
- 64x64/actions/1leftarrow.png	prev.png
- 64x64/actions/1rightarrow.png	next.png
- 64x64/actions/button_cancel.png error.png
- 64x64/actions/configure.png 	configure.png
- 64x64/actions/contents.png 	album.png
- 64x64/actions/filesave.png 	backup.png
- 64x64/actions/history.png 	datetime.png
- 64x64/actions/messagebox_warning.png warning.png
- 64x64/actions/messagebox_info.png info.png
- 64x64/actions/player_pause.png 	pause.png
- 64x64/actions/player_play.png 	play.png
- 64x64/actions/window_fullscreen.png fullscreen.png
- 64x64/actions/window_fullscreen.png unpack.png
- 64x64/actions/window_nofullscreen.png resize.png
- 64x64/actions/window_nofullscreen.png nofullscreen.png
- 64x64/apps/browser.png		location.png
- 64x64/apps/date.png		date.png
- 64x64/apps/ktorrent.png		tracks.png
- 64x64/apps/systemsettings.png	prefs.png
- 64x64/apps/system-users.png	groups.png
- 64x64/apps/system-users.png	people.png
- 64x64/apps/userconfig.png	users.png
- 64x64/apps/photographer.png	photographer.png
- 64x64/mimetypes/camera.png	settings.png
- 64x64/mimetypes/camera.png	favicon.png
- 64x64/mimetypes/contents2.png	pagesets.png
- 64x64/mimetypes/misc.png	title.png
- 64x64/mimetypes/document.png	pages.png
- 64x64/mimetypes/zip.png		archive.png
- 64x64/mimetypes/image.png	photobig.png
- 64x64/filesystems/folder_image_blue.png	folderphoto.png
- 64x64/filesystems/folder_image_blue.png	category.png

Pastel Icons
============
https://codefisher.org/pastel-svg/

The Pastel SVG icon set Created by Michael Buckley is licensed under the

Creative Commons Attribution NonCommercial Share Alike 4.0 http://creativecommons.org/licenses/by-nc-sa/4.0/

- 24/report-picture.png	exif.png
- 48/accept.png ok.png
- 48/help.png unknown.png
- 48/database-add.png migration.png
