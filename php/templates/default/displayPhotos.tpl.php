<?php
/**
 * Template for photos page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}

use conf\conf;
use template\template;

?>
<h1>
    <?= $this->getActionlinks($tpl_actionlinks) ?>
    <?= $tpl_title ?>
</h1>
<div class="main">
    <?php if ($tpl_displaycount >= 0): ?>
        <?= $tpl_viewsettings ?>
    <?php else: ?>
        <?= translate("No photos were found matching your search criteria.") ?>
    <?php endif ?>
    <?php $counter = 0 ?>
    <?php foreach ($tpl_thumbnails as $offset => $thumbnail): ?>
        <?php $counter++ ?>
        <div class="thumbnail">
            <?= $thumbnail ?>
            <?= $tpl_thActionlinks[$offset] ?>
        </div>
        <?php if ($counter % $tpl_cols == 0): ?>
            <br>
        <?php endif ?>
    <?php endforeach ?>
    <br>
    <?= $tpl_pager ?>
</div>
<?= $tpl_map ?>
