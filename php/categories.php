<?php
/**
 * Show categories
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 *
 */
use template\block;
use template\template;

require_once "include.inc.php";

$_view=getvar("_view");
if (empty($_view)) {
    $_view=$user->prefs->get("view");
}
$_autothumb=getvar("_autothumb");
if (empty($_autothumb)) {
    $_autothumb=$user->prefs->get("autothumb");
}

$parent_category_id = getvar("parent_category_id");
if (!$parent_category_id) {
    $category = category::getRoot();
} else {
    $category = new category($parent_category_id);
}

try {
    $selection=new selection($_SESSION, array(
        "coverphoto"    => "category.php?_action=coverphoto&amp;category_id=" . $category->getId() . "&amp;coverphoto=",
        "return"        => "_return=categories.php&amp;_qs=parent_category_id=" . $category->getId()
    ));
} catch (photoNoSelectionException $e) {
    $selection=null;
}

$pagenum = getvar("_pageset_page");

$category->lookup();
$obj=&$category;
$ancestors = $category->getAncestors();

$photoCount = $category->getPhotoCount();
$totalPhotoCount = $category->getTotalPhotoCount();

$title = $category->get("parent_category_id") ?
    $category->get("category") : translate("Categories");

$ancLinks=array();
if ($ancestors) {
    while ($parent = array_pop($ancestors)) {
        $ancLinks[$parent->getName()] = $parent->getURL();
    }
}

require_once "header.inc.php";

try {
    $pageset=$category->getPageset();
    $page=$category->getPage($request_vars, $pagenum);
    $showOrig=$category->showOrig($pagenum);
} catch (pageException $e) {
    $showOrig=true;
    $page=null;
}

$tpl=new template("organizer", array(
    "page"          => $page,
    "pageTop"       => $category->showPageOnTop(),
    "pageBottom"    => $category->showPageOnBottom(),
    "showMain"      => $showOrig,
    "title"         => $title,
    "ancLinks"      => $ancLinks,
    "selection"     => $selection,
    "coverphoto"    => $category->displayCoverPhoto(),
    "description"   => $category->get("category_description"),
    "view"          => $_view,
    "view_name"     => "Category view",
    "view_hidden"   => null,
    "autothumb"     => $_autothumb
));

$actionlinks=array();

if ($user->canEditOrganizers()) {
    $actionlinks=array(
        translate("edit") => "category.php?_action=edit&amp;category_id=" . (int) $category->getId(),
        translate("new") => "category.php?_action=new&amp;parent_category_id=" . (int) $category->getId(),
        translate("delete") => "category.php?_action=delete&amp;category_id=" . (int) $category->getId()
    );
    if ($category->get("coverphoto")) {
        $actionlinks["unset coverphoto"]="category.php?_action=unsetcoverphoto&amp;category_id=" . (int) $category->getId();
    }
}

$tpl->addActionlinks($actionlinks);

$sortorder = $category->get("sortorder");
$sort = $sortorder ? "&_order=" . $sortorder : "";

$tpl->addBlock(new block("photoCount", array(
    "tpc"       => $category->getTotalPhotoCount(),
    "totalUrl"  => "photos.php?category_id=" . $category->getBranchIds() . $sort,
    "pc"        => $category->getPhotoCount(),
    "url"       => "photos.php?category_id=" . $category->getId() . $sort
)));

$order = $user->prefs->get("child_sortorder");
$children = $category->getChildren($order);

if ($children) {
    $tpl->addBlock(new block("view_" . $_view, array(
        "id" => $_view . "view",
        "items" => $children,
        "autothumb" => $_autothumb,
        "topnode" => true,
        "links" => array(
            translate("view photos") => "photos.php?category_id="
        )
    )));
}


echo $tpl;

require_once "footer.inc.php";
?>
