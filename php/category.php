<?php
/**
 * Define and modify categories
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

use category\controller;
use template\block;
use web\redirect;
use web\request;

require_once "include.inc.php";

$request = request::create();
$controller = new controller($request);
if (!user::getCurrent()->canEditOrganizers()) {
    redirect::redirect("zoph.php", "User cannot edit organizers");
}

$category=$controller->getObject();
switch ($controller->getView()) {
case "confirm":
    $view=new category\view\confirm($request, $category);
    $title=translate("delete category");
    break;
case "insert":
case "update":
    $view=new category\view\update($request, $category);
    break;
case "redirect":
    $view=new category\view\redirect($request, $category);
    $view->setRedirect($controller->redirect);
    $title = translate("Redirect");
    echo $view->view();
    exit;
    break;
case "notfound":
    $view=new person\view\notfound($request);
    break;
case "display":
default:
    $title = translate("Display category");
    $view=new category\view\display($request, $category);
    echo $view->view();
    exit;
    break;
}

$title = $view->getTitle();
require_once "header.inc.php";
echo $view->view();
require_once "footer.inc.php";

?>
