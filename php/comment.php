<?php
/**
 * Define or edit comments
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

use conf\conf;
use comment\controller;
use template\template;
use web\redirect;
use web\request;

require_once "include.inc.php";

if (!conf::get("feature.comments")) {
    redirect::redirect("zoph.php", "Comment feature disabled");
}

$request = request::create();
try {
    $controller = new controller($request);
} catch (securityException $e) {
    $tpl=new template("error", array(
        "lang"          => language::getCurrentISO(),
        "title"         => "Security Error",
        "message"       => $e->getMessage(),
    ));
    $tpl->addActionlinks(array("return" => "zoph.php"));
    echo $tpl;
    exit(99);
}
switch ($controller->getView()) {
case "confirm":
    $view=new comment\view\confirm($request);
    break;
case "display":
    $view=new comment\view\display($request);
    break;
case "insert":
case "update":
    $view=new comment\view\update($request);
    break;
case "redirect":
    $view=new comment\view\redirect($request);
    $view->setRedirect($controller->redirect);
    echo $view->view();
    exit;
    break;
}

$title=$view->getTitle();
require_once("header.inc.php");

$title = $view->getTitle();
echo $view->view();

require_once("footer.inc.php");

?>
