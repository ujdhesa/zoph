
// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var zJSON=function() {
    var retry;
    var resp;

    function getData(object, search, response) {
        var http=new XMLHttpRequest();
        resp = response;
        if (object=='locationLookup') {
            var url="service/locationLookup.php?search=" + escape(search);
        } else if (object=='photoData') {
            var url="service/photoData.php?photoId=" + escape(search);
        } else if (object=='search') {
            var url="service/search.php" + search;
        } else if (object=='photoPeople') {
            var url="service/photoPeople.php?" + search;
        } else {
            return;
        }


        if (http) {
            http.open("GET", url, true);
            http.onreadystatechange=function() {
               httpResponse(http, object);
            };
            http.send(null);
        } else {
            // try again in 500 ms
            clearTimeout(retry);
            retry=setTimeout("JSON.getData('" + object + "','" + search + "')", 500);
        }
    }

    function httpResponse(http, object) {
        if (http.readyState == 4) {
            if(http.status == 200) {
                resp.httpResponse(object, http.response);
            }
        }
    }

    return {
        getData:getData,
        httpResponse:httpResponse
    };
}();
