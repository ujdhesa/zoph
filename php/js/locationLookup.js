// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var locationLookup=function() {
    function init() {
        var pos=document.getElementById('_locationLookup');
        if (pos) {
            pos.onkeyup=change;
            pos.onmouseup=change;
            pos.setAttribute("autocomplete", "off");
        }
    }

    function httpResponse(object, jsondata) {
        var event = new Event("change");
        var lat=document.getElementById("lat");
        var lon=document.getElementById("lon");
        var zoom=document.getElementById("mapzoom");
        
        var data = JSON.parse(jsondata);
        
        lat.value = data.lat;
        lon.value = data.lon;
        if (data.zoom != null) {
            zoom.value = data.zoom;
        }
        lat.dispatchEvent(event);
    }


    function change() {
        var oldtext = "";
        var obj=document.getElementById(this.id);

        var value=obj.value;
        if(oldtext!=value) {
            zJSON.getData("locationLookup", value, locationLookup);
            oldtext=value;
        }
    }

    return {
        init:init,
        httpResponse:httpResponse,
    };
}();

if(window.addEventListener) {
    window.addEventListener("load",locationLookup.init,false);
}
