// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var slideshow=function() {
    var ids = [];
    var counter = 0;

    var timeout;

    var startX;
    var startY;

    function init() {
        document.getElementById("butFullscreen").onclick=slideshow.fullscreen;
        document.getElementById("butPrev").onclick=slideshow.prev;
        document.getElementById("butPause").onclick=slideshow.pause;
        document.getElementById("butNext").onclick=slideshow.next;
        zJSON.getData("search", window.location.search , slideshow);
    }

    function httpResponse(object, jsondata) {
                
        var data = JSON.parse(jsondata);

        if (object == "search") {
            ids = data;
            slideshow.run();

        } else if (object == "photoData") {
            slideshow.display(data);
        }
        
    }

    function run() {
        if (counter >= ids.length) {
            counter = 0;
        }
        var photo=zJSON.getData("photoData", ids[counter], slideshow);
        counter++;
	// global var duration is set in slideshow/display/view, from the user's prefs
        timeout = window.setTimeout(slideshow.run, duration * 1000);
    }

    function display(data) {
        var people=[];

        var photo = document.getElementById("photo");
        let photonew=photo.cloneNode(true);
        photonew.srcset = data.image + " 2x, " + data.image + " 1x";
        photonew.src = data.image;
        photo.parentNode.replaceChild(photonew, photo);

        var name = document.getElementById("slide-name");
        name.innerHTML=data.name;
        var title = document.getElementById("slide-title");
        title.innerHTML=data.title;
        var photographer = document.getElementById("slide-photographer");
        photographer.innerHTML=data.photographer;
        var loc = document.getElementById("slide-location");
        loc.innerHTML=data.location;
        var datetime = document.getElementById("slide-datetime");
        datetime.innerHTML=data.datetime;

        array2ul(data.people, "slide-people");
        array2ul(data.albums, "slide-albums");
        array2ul(data.categories, "slide-categories");

        let exifdl = document.getElementById("slide-exifdata");
        while(exifdl.childNodes.length > 0) {
            exifdl.childNodes[0].parentNode.removeChild(exifdl.childNodes[0]);
        }
        for (let exif in data.exifdata) {
            let contents = data.exifdata[exif];
            if(contents.trim()) {
                let dt=document.createElement("dt");
                let dd=document.createElement("dd");
                dt.innerHTML=exif;
                dd.innerHTML=contents;
                exifdl.appendChild(dt);
                exifdl.appendChild(dd);
                // insert a space so wrapping will occur here and
                // not between a dt and a dd
                exifdl.innerHTML+=" ";
            }
        }

        // Remove and re-add annotate block to trigger animation
        let annotate=document.getElementById("annotate");
        let annotatenew=annotate.cloneNode(true);
        annotate.parentNode.replaceChild(annotatenew, annotate);
        
    }

    function array2ul(json, id) {
        // Remove current
        let current = document.getElementById(id).childNodes;
        while(current.length > 0) {
            current[0].parentNode.removeChild(current[0]);
        }
        for (let element in json) {
            json2li(json[element], id);
        }
    }

    function json2li(jsondata, id) {
        let li=document.createElement("li");
        li.innerHTML=jsondata;
        let element=document.getElementById(id);
        element.appendChild(li);
    }

    function fullscreen() {
        if(document.fullscreenEnabled) {
          getElementsByClass("main")[0].requestFullscreen();
        }
        document.getElementById("butFullscreen").onclick=slideshow.exitFullscreen;
    }
    
    function exitFullscreen() {
        if(document.fullscreenEnabled) {
          document.exitFullscreen();
        }
        document.getElementById("butFullscreen").onclick=slideshow.fullscreen;
    }

    function touch(e) {
        //e.preventDefault();
        var touches = e.changedTouches;
        // Check if there is only one finger on the screen
        if (touches.length == 1) {
            startX = touches[0].pageX;
            startY = touches[0].pageY;
        }
    }

    function touchend(e) {
        //e.preventDefault();
        var touches = e.changedTouches;
        // Check if there is only one finger on the screen
        if (touches.length == 1) {
            var endX = touches[0].pageX;
            var endY = touches[0].pageY;

            // Move at least 25% of the screen or 250px if the screen is wider than 1000px;
            //moveX = Math.min(Screen.width / 4, 250);
            var moveX = 250;
            if (Math.abs(startX - endX) > moveX) {
                if ((startX - endX) < 0) {
                    prev();
                } else {
                    next();
                }
            }
        }
    }

    function prev() {
        if (timeout) {
            window.clearTimeout(timeout);
        }
        counter-=2;
        if (counter < 0) {
            counter=ids.length - 1;
        }
        run();
    }

    function pause() {
        window.clearTimeout(timeout);
        document.getElementById("butPause").onclick=slideshow.unpause;
        document.getElementById("butPause").style.backgroundImage="url('" + icons["play"] + "')";

    }

    function unpause() {
        document.getElementById("butPause").onclick=slideshow.pause;
        document.getElementById("butPause").style.backgroundImage="url('" + icons["pause"] + "')";
        run();

    }

    function next() {
        if (timeout) {
            window.clearTimeout(timeout);
        }
        run();
    }

    return {
        init:init,
        httpResponse:httpResponse,
        run:run,
        display:display,
        fullscreen:fullscreen,
        exitFullscreen:exitFullscreen,
        touch:touch,
        touchend:touchend,
        next:next,
        prev:prev,
        pause:pause,
        unpause:unpause
    };
}();

if(window.addEventListener) {
    window.addEventListener("load", slideshow.init, false);
    window.addEventListener("touchstart", slideshow.touch, false);
    window.addEventListener("touchend", slideshow.touchend, false);
}
