
// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var photoPeople=function() {

    var peopleInPhoto;

    function init() {
        var div = document.getElementById("editPeople");
        let photoId=document.querySelectorAll('[name="photo_id"]')[0].value;
        if(div && photoId) {
            zJSON.getData("photoPeople", "photoId=" + photoId, photoPeople);
        }
    
    }
    function transform() {
        let rows = [];
        let personrows = document.getElementsByClassName("personrows")[0];
        let pc = personrows.childNodes;
        
        let photoId=document.querySelectorAll('[name="photo_id"]')[0].value;

        for(var c of pc) {
            if (c.nodeName=="LI") {
                rows.push(c);
            }
        }
        let r=0;
        for(var row of rows) {
            let people = [];
            r++;
            let peoplerow = row.firstElementChild.childNodes;

            let add=document.createElement("div");
            add.className="add";
            add.setAttribute("onClick", "photoPeople.addPerson(" + photoId + ", " + r + ")");
            row.insertBefore(add, row.firstElementChild);
            
            for (const person of peoplerow) {
                if (person.nodeName=="LI") {
                    people.push(person);

                }
            }

            for (const p in people) {
                var id=people[p].attributes["data-id"].nodeValue;

                if (p != 0) {  
                    let left=document.createElement("div");
                    left.className="left";
                    left.setAttribute("onClick", "photoPeople.movePerson(" + id+ " ,'left')");
                    people[p].insertBefore(left, people[p].firstElementChild);
                }
                
                if( (parseInt(p) + 1) != people.length) {
                    let right=document.createElement("div");
                    right.className="right";
                    right.setAttribute("onClick", "photoPeople.movePerson(" + id+ " ,'right')");
                    people[p].insertBefore(right, people[p].firstElementChild);
                }
                let up=document.createElement("div");
                up.className="up";
                up.setAttribute("onClick", "photoPeople.movePerson(" + id+ " ,'up')");
                people[p].insertBefore(up, people[p].firstElementChild);
                
                let down=document.createElement("div");
                down.className="down";
                down.setAttribute("onClick", "photoPeople.movePerson(" + id+ " ,'down')");
                people[p].insertBefore(down, people[p].firstElementChild);
                
                let remove=document.createElement("div");
                remove.className="remove";
                remove.setAttribute("onClick", "photoPeople.movePerson(" + id+ " ,'remove')");
                people[p].insertBefore(remove, people[p].firstElementChild);

            }
        }

    }

    function httpResponse(object, json) {
        removePeople();
        addPeopleFromJSON(json.trim());
    }

    function removePeople() {
        let div = document.getElementById("editPeople");
        //personrows = document.getElementsByClassName("personrows")[0];
        while (div.hasChildNodes()) {
            div.removeChild(div.childNodes[0]);
        }
    }

    function addPerson(photoId, row) {
        let div=document.createElement("div");
        div.id="addPeopleModal"
        div.className="modal";
        if(autocomppeople) {
            div.innerHTML="<h1>" + zTranslate.get("Add person") + "<img src='" + icons["remove"] + "' onClick='photoPeople.closeModal()'></h1><fieldset name='person_id[0]' class='multiple'><input type=hidden id=person_id[0]><input type='text' class='autocomplete' id='_person_id[0]'></fieldset><input type=button onClick='photoPeople.handleAddPeople(" + photoId + "," + row + ")' value='" + zTranslate.get("add") + "'>";
            document.body.appendChild(div);
            autocomplete.init();
        } else {
            div.innerHTML="<h1>" + zTranslate.get("Add person") + "<img src='" + icons["remove"] + "' onClick='photoPeople.closeModal()'></h1>" + zTranslate.get("Autocomplete is switched off");
            document.body.appendChild(div);
        }
    }
    
    function handleAddPeople(photoId, row) {
        var ids = [];
        var modal = document.getElementById("addPeopleModal");
        var people=modal.querySelectorAll('[type="hidden"]');

        for (var person in people) {
            var id=parseInt(people[person].value);
            if(photoPeople.peopleInPhoto.indexOf(id) != -1) {
                var error = zTranslate.get("Person %s is already in this photo.");
                error = error.replace("%s", id.toString());
                zError.push(error);
                id = 0;
            }
            if(id != 0) {
                zJSON.getData("photoPeople", "personId=" + id + "&photoId=" + photoId + "&row=" + row + "&action=add", photoPeople);
            }
        }
        modal.parentElement.removeChild(modal);

        
    }

    function closeModal() {
        let modal = document.getElementById("addPeopleModal");
        modal.parentElement.removeChild(modal);

    }
    
    function addPeopleFromJSON(json) {
        photoPeople.peopleInPhoto = [];
        let div = document.getElementById("editPeople");
        let personrows = document.createElement("ul");
        personrows.className="personrows";
        
        let parsed = JSON.parse(json);
        let people = parsed.people;
        if (people.length == 0) {
            people.push("");
        }
        for (let row in people) {
            let li = document.createElement("li");    
            let ul = document.createElement("ul");    
            ul.className="people";
            for (let person of people[row]) {
                let personli = document.createElement("li");
                personli.setAttribute("data-id", person.id);
                photoPeople.peopleInPhoto.push(person.id);
                let a = document.createElement("a");
                a.href = person.url;
                a.innerHTML=person.name;
                personli.appendChild(a);
                ul.appendChild(personli);
            }
            li.appendChild(ul);
            personrows.appendChild(li)
        }
        div.appendChild(personrows);
        transform();
    }        

    function movePerson(id, dir) {
        let photoId=document.querySelectorAll('[name="photo_id"]')[0].value;
        zJSON.getData("photoPeople", "personId=" + id + "&photoId=" + photoId + "&action=" + dir, photoPeople);
    }

    return {
        init:init,
        movePerson:movePerson,
        addPerson:addPerson,
        handleAddPeople:handleAddPeople,
        httpResponse:httpResponse,
        closeModal:closeModal,
        peopleInPhoto:peopleInPhoto
    };
}();

if(window.addEventListener) {
    window.addEventListener("load",photoPeople.init,false);
}

