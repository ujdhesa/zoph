<?php
/**
 * Define and modify a group of users
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

use conf\conf;

use group\controller as groupController;

use template\block;
use template\form;
use template\template;

use web\redirect;
use web\request;


require_once "include.inc.php";

if (!user::getCurrent()->isAdmin()) {
    redirect::redirect("zoph.php", "Admin user required");
}

$controller = new groupController(request::create());
$view = $controller->getView();

$headers = $view->getHeaders();
if (is_array($headers)) {
    foreach ($headers as $header) {
        header($header);
    }
    echo $view->view();
} else {
    $title = $view->getTitle();
    require_once("header.inc.php");
    echo $view->view();
    require_once("footer.inc.php");
}
