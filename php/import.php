<?php
/**
 * This file is the controller part of the import functions
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */
use conf\conf;

use template\block;
use template\template;

use web\redirect;

require_once "include.inc.php";
if ((!conf::get("import.enable")) || (!$user->isAdmin() && !$user->get("import"))) {
    redirect::redirect("zoph.php", "Import feature disabled or insufficient rights");
}

// Detect upload larger than upload_max_filesize.
//if (isset($_GET["upload"]) && $_GET["upload"]==1 && $_POST==null) {
//    echo import\web::handleUploadErrors(UPLOAD_ERR_INI_SIZE);
//    die();
//}
$_action=getvar("_action");

$title = translate("Import");

if (empty($_action)) {
    require_once "header.inc.php";
}

session_write_close();

if (empty($_action)) {
    $javascript=
        "translate=new Array();\n" .
        "translate['retry']='" .trim(translate("retry", false)) . "';\n" .
        "translate['delete']='" .trim(translate("delete", false)) . "';\n" .
        "translate['import']='" .trim(translate("import", false)) . "';\n" .
        "parallel=" . (int) conf::get("import.parallel")  . ";\n";
    if (conf::get("import.upload")) {
        $upload=new block("uploadform", array(
            "action"    => "import.php?_action=upload",
        ));
    } else {
        $upload=translate("Uploading photos has been disabled in configuration.");
    }

    $tpl=new template("import", array(
        "upload" => $upload,
        "javascript" => $javascript,
    ));
    echo $tpl;
    include "footer.inc.php";
} else if ($_action=="upload") {
    if (conf::get("import.upload") && $_FILES["file"]) {
        $file=$_FILES["file"];
        import\web::processUpload($file);
    }
} else if ($_action=="process") {
    $file=getvar("file");
    import\web::processFile($file);
} else if ($_action=="retry") {
    $file=getvar("file");
    import\web::retryFile($file);
} else if ($_action=="delete") {
    $file=getvar("file");
    import\web::deleteFile($file);
} else if ($_action=="import") {
    $files=import\web::getFileList($request_vars["_import_image"]);
    import\web::photos($files, $request_vars);
}
