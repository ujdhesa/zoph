<?php
/**
 * Photos controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use photos\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the search controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photosControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test getPhotos and getDisplay methods
     * @dataProvider getRequestGETs
     */
    public function testGetPhotosDisplay($GET, $expPhotos, $expDisplay) {
        $request=new request(array(
            "GET"   => $GET,
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getPhotos();

        $this->assertInstanceOf("photo\collection", $photos);
        $act = array();
        foreach ($photos as $photo) {
            $act[]=$photo->getId();
        }
        $this->assertEquals($expPhotos, $act);

        $display=$controller->getDisplay();

        $this->assertInstanceOf("photo\collection", $display);
        $act = array();
        foreach ($display as $photo) {
            $act[]=$photo->getId();
        }
        $this->assertEquals($expDisplay, $act);

    }

    /**
     * Test Edit
     */
    public function testActionEdit() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "edit",
                "album_id"  => 5,
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getPhotos();

        $this->assertInstanceOf("photo\collection", $photos);
        $this->assertCount(3, $photos);

        $view = $controller->getView();
        $this->assertInstanceOf('photos\view\edit', $view);

        $tpl = $view->view();
        $this->assertInstanceOf('template\template', $tpl);
    }

    /**
     * Test Update
     */
    public function testActionUpdate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "update",
                "album_id"  => 5,
                "_action__3"    => "update",
                "__location_id__3"  => 7,
                "_album__3"   => array(4)
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getPhotos();

        $this->assertInstanceOf("photo\collection", $photos);
        $this->assertCount(3, $photos);

        $view = $controller->getView();
        $this->assertInstanceOf('photos\view\edit', $view);

        $tpl = $view->view();
        $this->assertInstanceOf('template\template', $tpl);

        $photo = new photo(3);
        $photo->lookup();
        $locId=$photo->get("location_id");
        $this->assertEquals(7, $locId);

        $albums=$photo->getAlbums();
        $album_ids=array();
        foreach ($albums as $album) {
            $album_ids[]=$album->getId();
        }
        sort($album_ids);

        $this->assertEquals(array(4,5,6), $album_ids);


        // Restore previous state
        $photo->set("location_id", 4);
        $photo->removeFrom(new album(4));
        $photo->update();
    }

    /**
     * Test Update with _all
     */
    public function testActionUpdateAll() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "update",
                "album_id"  => 5,
                "_action__3"    => "update",
                "_action__4"    => "update",
                "_action__5"    => "update",
                "__location_id__all"  => 8,
                "__location_id__3"  => 6,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getPhotos();

        foreach ([3=>6, 4=>8, 5=>8] as $id => $location) {
            $photo = new photo($id);
            $photo->lookup();
            $locId=$photo->get("location_id");
            $this->assertEquals($location, $locId);
        }

        // Restore previous state
        foreach ([3=>4, 4=>5, 5=>5] as $id => $location) {
            $photo = new photo($id);
            $photo->lookup();
            $photo->set("location_id", $location);
            $photo->update();
        }
    }

    /**
     * Test Update with _all and _overwrite and Skip
     */
    public function testActionUpdateAllOverwriteSkip() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_overwrite"        => 1,
                "_action"           => "update",
                "album_id"          => 5,
                "_action__3"        => "update",
                "_action__4"        => "update", // nothing set for id 6, so it will skip.
                "__location_id__all"=> 8,
                "__location_id__3"  => 6,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getPhotos();

        foreach ([3=>8, 4=>8, 5=>5] as $id => $location) {
            $photo = new photo($id);
            $photo->lookup();
            $locId=$photo->get("location_id");
            $this->assertEquals($location, $locId);
        }

        // Restore previous state
        foreach ([3=>4, 4=>5, 5=>5] as $id => $location) {
            $photo = new photo($id);
            $photo->lookup();
            $photo->set("location_id", $location);
            $photo->update();
        }
    }

    public function testGetLightbox() {
        user::setCurrent(new user(7));
        $request=new request(array(
            "GET"   => array(
                "album_id" => 7
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getLightbox();

        $this->assertEquals(array(6), $photos);

        user::setCurrent(new user(1));
    }

    public function testGetParams() {
        $request=new request(array(
            "GET"   => array(
                "album_id" => 1,
                "_cols" => 10,
                "_rows" => 5
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $params=$controller->getParams();

        $this->assertEquals(10, $params->cols);
        $this->assertEquals(5, $params->rows);
        $this->assertEquals("date", $params->order);
    }

    /**
     * Test Download not allowed
     */
    public function testActionDownloadNotAllowed() {
        // should already be off, but just to make sure
        conf::set("feature.download", 0);

        $request=new request(array(
            "GET"   => array(
                "_action"   => "download",
                "album_id"  => 5,
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();
        $this->assertInstanceOf('photos\view\display', $view);
    }

    /**
     * Test Download - Prepare
    /**
     * Test Download
     */
    public function testActionDownload() {
        // enable feature
        conf::set("feature.download", 1);

        $request=new request(array(
            "GET"   => array(
                "_action"   => "download",
                "album_id"  => 5,
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getPhotos();

        $this->assertInstanceOf("photo\collection", $photos);
        $this->assertCount(3, $photos);

        $view = $controller->getView();
        $this->assertInstanceOf('photos\view\download', $view);

        $tpl = $view->view();
        $this->assertInstanceOf('template\template', $tpl);

        // disable feature, return to default
        conf::set("feature.download", 0);
    }

    /**
     * Test Download - Prepare
     */
    public function testActionPrepare() {
        // enable feature
        conf::set("feature.download", 1);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "prepare",
                "album_id"  => 5,
                "_filename" => "test",
                "_maxfiles" => "2",
                "_maxsize"  => "500000"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photos=$controller->getPhotos();

        $this->assertInstanceOf("photo\collection", $photos);
        $this->assertCount(3, $photos);

        $view = $controller->getView();
        $this->assertInstanceOf('photos\view\prepare', $view);

        $tpl = $view->view();
        $this->assertInstanceOf('template\template', $tpl);

        $this->assertFileExists("/tmp/zoph_1_test_1.zip");

        // disable feature, return to default
        conf::set("feature.download", 0);
    }

    public function getActions() {
        return array(
            array("display", 'photos\view\display'),
            array("edit", 'photos\view\edit'),
            array("update", 'photos\view\edit'),
            array("nonexistant", 'photos\view\display')
        );
    }

    public function getRequestGETs() {
        return array(
            array(
                array("album_id" => "2"),
                array(1,7),
                array(1,7)
            ), array(
                array("category_id" => "10"),
                array(6,7,8,9),
                array(6,7,8,9)
            ), array (
                array(
                    "album_id" => "2,3,4",
                    "_off"      => "2",
                    "_cols"      => "2",
                    "_rows"      => "1"
                ),
                array(1,2,7,8,9,10),
                array(7,8)
            ), array(
                array(
                    "album_id" => "5",
                    "_order" => "date",
                    "_dir"      => "desc",
                    "_cols"      => "1",
                    "_rows"      => "1"
                ),
                array(5,4,3),
                array(5)
            )
        );
    }
}
