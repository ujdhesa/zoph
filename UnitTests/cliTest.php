<?php
/**
 * Test CLI interface
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use auth\validator;
use cli\cli;
use conf\conf;
use PHPUnit\Framework\TestCase;

/**
 * Test CLI interface
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class cliTest extends TestCase {

    /**
     * Test import without additional parameters
     */
    public function testBasicImport() {
        $files=$this->getFilelist(array(
            "PHOTO-01.JPG",
            "PHOTO-02.JPG",
            "PHOTO-03.JPG"));
        $this->doCleanup($files);

        $testdata=$this->getFilenames();
        foreach ($testdata as $testimg) {
            helpers::createTestImage($testimg[0], $testimg[1], $testimg[2], $testimg[3]);
        }

        $cli="zoph --instance " . INSTANCE . " /tmp/PHOTO-01.JPG " .
        "/tmp/PHOTO-02.JPG /tmp/PHOTO-03.JPG";

        $this->runCLI($cli);

        foreach ($files as $file) {
            $this->assertFileExists($file);
        }

        $this->doCleanup($files);

    }

    /**
     * Test import with album, category, photographer and location
     */
    public function testOrganizedImport() {
        $files=$this->getFilelist(array(
            "PHOTO-01.JPG",
            "PHOTO-02.JPG",
            "PHOTO-03.JPG"));
        $this->doCleanup($files);

        $testdata=$this->getFilenames();
        foreach ($testdata as $testimg) {
            helpers::createTestImage($testimg[0], $testimg[1], $testimg[2], $testimg[3]);
        }

        $cli="zoph --instance " . INSTANCE . " --album Album_1 " .
            "--category blue --photographer Brian_May --person Jimi_Hendrix " .
            "--location Netherlands /tmp/PHOTO-01.JPG " .
            "/tmp/PHOTO-02.JPG /tmp/PHOTO-03.JPG";

        $this->runCLI($cli);

        for ($i=1; $i<=3; $i++) {
            $photos=photo::getByName("PHOTO-0" . $i . ".JPG");
            $photo=array_pop($photos);
            $photo->lookup();
            $this->assertInstanceOf("photo", $photo);
            $albums=$photo->getAlbums();
            $this->assertEquals(1, sizeof($albums));
            $album=array_pop($albums);
            $this->assertInstanceOf("album", $album);
            $this->assertEquals(2, $album->getId());

            $categories=$photo->getCategories();
            $this->assertEquals(1, sizeof($categories));
            $category=array_pop($categories);
            $this->assertInstanceOf("category", $category);
            $this->assertEquals(5, $category->getId());

            $people=(new photo\people($photo))->getAll();
            $this->assertEquals(1, sizeof($people));
            $person=array_pop($people);
            $this->assertInstanceOf("person", $person);
            $this->assertEquals(3, $person->getId());

            $photographer=$photo->getPhotographer();
            $this->assertInstanceOf("person", $photographer);
            $this->assertEquals(2, $photographer->getId());

            $place=$photo->getLocation();
            $this->assertInstanceOf("place", $place);
            $this->assertEquals(3, $place->getId());
        }
        foreach ($files as $file) {
            $this->assertFileExists($file);
        }

        $this->doCleanup($files);
        foreach (array("2013.02.01", "2013.02.02", "2013.02.03") as $dir) {
            $this->cleanDirs($dir);
        }
    }

    public function testDatedDirs() {
        $files=$this->getFilelist(array(
            "2013.02.01" => "PHOTO-01.JPG",
            "PHOTO-02.JPG",
            "2013/02/03" => "PHOTO-03.JPG"));
        $this->doCleanup($files);

        $testdata=$this->getFilenames();
        foreach ($testdata as $testimg) {
            helpers::createTestImage($testimg[0], $testimg[1], $testimg[2], $testimg[3]);
        }

        $cli="zoph --instance " . INSTANCE .
            " --datedDirs --nohier " .
            " /tmp/PHOTO-01.JPG ";

        $this->runCLI($cli);

        $cli="zoph --instance " . INSTANCE .
            " --no-dateddirs " .
            " /tmp/PHOTO-02.JPG ";

        $this->runCLI($cli);

        $cli="zoph --instance " . INSTANCE .
            " -H " .
            " /tmp/PHOTO-03.JPG ";

        $this->runCLI($cli);

        foreach ($files as $file) {
            $this->assertFileExists($file);
        }

        // Test --update with full path
        $cli='zoph --instance ' . INSTANCE .
            ' --update --album Album_21 2013.02.01/PHOTO-01.JPG';
        $this->runCLI($cli);

        $photos=photo::getByName("PHOTO-01.JPG");
        $photo=array_pop($photos);
        $photo->lookup();
        $albums=$photo->getAlbums();
        $album=array_pop($albums);
        $this->assertInstanceOf("album", $album);
        $this->assertEquals(8, $album->getId());

        // Test --update with only filename

        $oldpath=getcwd();
        chdir(conf::get("path.images") . "/2013/02/03");
        $cli='zoph --instance ' . INSTANCE .
            ' --update --album Album_21 PHOTO-03.JPG';
        $this->runCLI($cli);
        chdir($oldpath);

        $photos=photo::getByName("PHOTO-03.JPG");
        $photo=array_pop($photos);
        $photo->lookup();
        $albums=$photo->getAlbums();
        $album=array_pop($albums);
        $this->assertInstanceOf("album", $album);
        $this->assertEquals(8, $album->getId());

        $this->doCleanup($files);
        foreach (array("2013.02.01", "", "2013/02/03") as $dir) {
            $this->cleanDirs($dir);
        }
    }

    /**
     * Test --update with photo_ids
     */
    public function testUpdateWithID() {
        // Check current state of testphoto
        $photos=photo::getByName("TEST_0001.JPG");
        $photo=array_pop($photos);
        $photo->lookup();
        $this->assertInstanceOf("photo", $photo);
        $origPhoto = clone $photo;

        $albums=$photo->getAlbums();
        $albumIds=array_map(function($a) { return $a->getId(); }, $albums);
        $this->assertEquals([2,3], $albumIds);

        $cli='zoph --instance ' . INSTANCE .
            ' --update --useIds --album Album_21 ' . $photo->getId();

        $this->runCLI($cli);

        $photo->lookup();
        $albums=$photo->getAlbums();
        $albumIds=array_map(function($a) { return $a->getId(); }, $albums);

        $this->assertEquals([2,3,8], $albumIds);

        $photo->removeFrom(new album(8));
        $origPhoto->update();
        conf::set("import.cli.useids", false);
    }

    /**
     * Test Create Album with no --parent
     */
    public function testCreateAlbumNoParent() {
        $this->expectException(cliNoParentException::class);
        $cli="zoph --instance " . INSTANCE .
            " --new --album Test_new_album ";

        $this->runCLI($cli);
    }

    /**
     * Test Create Category with no --parent
     */
    public function testCreateCategoryNoParent() {
        $this->expectException(cliNoParentException::class);
        $cli="zoph --instance " . INSTANCE .
            " --new --category Test_new_category ";

        $this->runCLI($cli);
    }

    /**
     * Test Create Place with no --parent
     */
    public function testCreatePlaceNoParent() {
        $this->expectException(cliNoParentException::class);
        $cli="zoph --instance " . INSTANCE .
            " --new --place Test_new_place ";

        $this->runCLI($cli);
    }

    /**
     * Test Create Album with non-existent --parent
     */
    public function testCreateAlbumNonExistentParent() {
        $this->expectException(albumNotFoundException::class);
        $cli="zoph --instance " . INSTANCE .
            " --new --parent NonExistent --album Test_new_album ";

        $this->runCLI($cli);
    }

    /**
     * Test Create Category with non-existent --parent
     */
    public function testCreateCategoryNonExistentParent() {
        $this->expectException(categoryNotFoundException::class);
        $cli="zoph --instance " . INSTANCE .
            " --new --parent NonExistent --category Test_new_category ";

        $this->runCLI($cli);
    }

    /**
     * Test Create Place with non-existent --parent
     */
    public function testCreatePlaceNonExistentParent() {
        $this->expectException(placeNotFoundException::class);
        $cli="zoph --instance " . INSTANCE .
            " --new --parent NonExistent --place Test_new_place ";

        $this->runCLI($cli);
    }

    /**
     * Test Create Album
     */
    public function testCreateAlbum() {
        $cli="zoph --instance " . INSTANCE .
            " --new --parent Album_1 --album Test_new_album ";

        $this->runCLI($cli);

        $albums=album::getByName("Test new album");
        $album=array_shift($albums);
        $album->lookup();
        $this->assertInstanceOf("album", $album);
        $parent=$album->get("parent_album_id");
        $this->assertEquals(2, $parent);
        $album->delete();
    }

    /**
     * Test Create Category
     */
    public function testCreateCategory() {
        $cli="zoph --instance " . INSTANCE .
            " --new --parent Blue --category Test_new_category ";

        $this->runCLI($cli);

        $cats=category::getByName("Test new category");
        $cat=array_shift($cats);
        $cat->lookup();
        $this->assertInstanceOf("category", $cat);
        $parent=$cat->get("parent_category_id");
        $this->assertEquals(5, $parent);
        $cat->delete();
    }

    /**
     * Test Create Place
     */
    public function testCreatePlace() {
        $cli="zoph --instance " . INSTANCE .
            " --new --parent Netherlands --place Test_new_place ";
        $this->runCLI($cli);

        $places=place::getByName("Test new place");
        $place=array_shift($places);
        $place->lookup();
        $this->assertInstanceOf("place", $place);
        $parent=$place->get("parent_place_id");
        $this->assertEquals(3, $parent);
        $place->delete();
    }

    /**
     * Test Create User
     */
    public function testCreateUser() {
        $cli="zoph --instance " . INSTANCE .
            " --user add --username CLITEST --password VerySecret";
        $this->runCLI($cli);

        $validator = new validator("CLITEST", "VerySecret");
        $user = $validator->validate();

        $this->assertInstanceOf("user", $user);

        $this->assertFalse($user->isAdmin());

        return $user;
    }

    /**
     * Test Make User Admin
     * @depends testCreateUser
     */
    public function testUpgradeUser(user $user) {
        $cli="zoph --instance " . INSTANCE .
            " --user update --username CLITEST --admin";
        $this->runCLI($cli);

        $user->lookup();
        $this->assertTrue($user->isAdmin());
        return $user;
    }

    /**
     * Test Make Admin User
     * @depends testUpgradeUser
     */
    public function testDowngradeUser(user $user) {
        $cli="zoph --instance " . INSTANCE .
            " --user update --username CLITEST --no-admin";
        $this->runCLI($cli);

        $user->lookup();
        $this->assertFalse($user->isAdmin());
        return $user;
    }

    /**
     * Test Delete User
     * @depends testDowngradeUser
     */
    public function testDeleteUser(user $user) {
        $cli="zoph --instance " . INSTANCE .
            " --user delete --username CLITEST -f";
        $this->runCLI($cli);

        $this->expectException(userNotFoundException::class);
        user::getByName("CLITEST");
    }

    /**
     * Test Add Access
     */
    public function testAddAccess() {
        $cli="zoph --instance " . INSTANCE .
            " --user update --username brian --access download";
        $this->runCLI($cli);

        $user = user::getByName("brian");
        $this->assertInstanceOf("user", $user);

        $this->assertEquals(1, $user->get("download"));

        return $user;
    }

    /**
     * Test Revoke Access
     * @depends testAddAccess
     */
    public function testRevokeAccess(user $user) {
        $cli="zoph --instance " . INSTANCE .
            " --user update --username brian --no-access download";
        $this->runCLI($cli);
        $user->lookup();
        $this->assertInstanceOf("user", $user);

        $this->assertEquals(0, $user->get("download"));
    }

    /**
     * Test Unknown Access right
     */
    public function testUnknownAccess() {
        $this->expectException(userUnknownAccessRightException::class);
        $cli="zoph --instance " . INSTANCE .
            " --user update --username brian --access unlimited";
        $this->runCLI($cli);
    }

    /**
     * Test Add Group
     */
    public function testAddGroup() {
        $cli="zoph --instance " . INSTANCE .
            " --user update --username brian --group Beatles";
        $this->runCLI($cli);

        $user = user::getByName("brian");
        $this->assertInstanceOf("user", $user);

        $groups=$user->getGroups();
        $groupIds=array_map(function($g) { return $g->getId(); }, $groups);
        sort($groupIds);
        $this->assertEquals([1,2,4], $groupIds);
        return $user;
    }

    /**
     * Test Remove group
     * @depends testAddGroup
     */
    public function testRemoveGroup(user $user) {
        $cli="zoph --instance " . INSTANCE .
            " --user update --username brian --remove-group Beatles";
        $this->runCLI($cli);

        $groups=$user->getGroups();
        $groupIds=array_map(function($g) { return $g->getId(); }, $groups);
        sort($groupIds);
        $this->assertEquals([1,4], $groupIds);
    }

    /**
     * Test Change Password
     */
    public function testChangePassword() {
        $cli="zoph --instance " . INSTANCE .
            " --user update --username brian --password 1234";
        $this->runCLI($cli);

        $validator = new validator("brian", "1234");
        $user = $validator->validate();

        $this->assertInstanceOf("user", $user);

        $user->set("password", NULL);
        $user->update();
    }

    /**
     * Test Unknown Group
     */
    public function testUnknownGroup() {
        $this->expectException(groupNotFoundException::class);
        $cli="zoph --instance " . INSTANCE .
            " --user update --username brian -G drummers";
        $this->runCLI($cli);
    }

    /**

    /**
     * Test Create Duplicate User
     */
    public function testCreateExistingUser() {
        $cli="zoph --instance " . INSTANCE .
            " --user add --username CLITEST --password 'VerySecret'";
        $this->runCLI($cli);

        $user = user::getByName("CLITEST");
        $this->assertInstanceOf("user", $user);

        $this->expectException(userAlreadyExistsException::class);
        $this->runCLI($cli);
    }

    /**
     * Test Create Admin User
     */
    public function testCreateAdminUser() {
        // Clean existing user
        $user = user::getByName("CLITEST");
        $user->delete();

        $cli="zoph --instance " . INSTANCE .
            " --user add --admin --username CLITEST --password 'VerySecret'";
        $this->runCLI($cli);

        $user = user::getByName("CLITEST");
        $this->assertInstanceOf("user", $user);

        $this->assertTrue($user->isAdmin());

        $user->delete();
    }

    /**
     * Test Create User without supplying password
     */
    public function testCreateUserWithNoPassword() {
        $this->expectException(cliNoPasswordException::class);
        $cli="zoph --instance " . INSTANCE .
            " --user add --username CLITEST";
        $this->runCLI($cli);

        $user = user::getByName("CLITEST");
        $this->assertNotInstanceOf("user", $user);
    }

    /**
     * Test --user without supplying command
     */
    public function testUserWithNoCommand() {
        $this->expectException(cliNoUserCommandException::class);
        $cli="zoph --instance " . INSTANCE .
            " --user --username 'CLITEST'";
        $this->runCLI($cli);
    }

    /**
     * Test calling with --help argument
     */
    public function testHelp() {
        $cli="zoph --instance " . INSTANCE . " --help";

        ob_start();
            $this->runCLI($cli);
        $output=ob_get_clean();
        $this->assertMatchesRegularExpression("/zoph.+\nUsage:.+\nOPTIONS:\n.+/", $output);
    }

    /**
     * Test calling with --version argument
     */
    public function testVersion() {
        $cli="zoph --instance " . INSTANCE . " --version";

        ob_start();
            $this->runCLI($cli);
        $output=ob_get_clean();
        $this->assertMatchesRegularExpression("/Zoph v.+, released [0-9]{1,2}-[0-9]{1,2}-[0-9]{2,4}/", $output);
    }

    private function doCleanup($files) {
        foreach ($files as $file) {
            $photos=photo::getByName(basename($file));
            if (isset($photos[0])) {
                $photos[0]->delete();
            }
            @unlink($file);
        }
    }

    private function getFileList($files) {
        $filelist=array();

        $prefixes=array(
            "",
            THUMB_PREFIX,
            MID_PREFIX
        );

        foreach ($files as $dir => $file) {
            if (is_int($dir)) {
                $dir="";
            } else {
                $dir.="/";
            }

            foreach ($prefixes as $prefix) {
                if (!empty($prefix)) {
                    $filename=conf::get("path.images") . "/" . $dir . $prefix . "/" .
                        $prefix . "_" . $file;
                } else {
                    $filename=conf::get("path.images") . "/" . $dir . $file;
                }

                $filelist[]=$filename;
            }
        }
        return $filelist;
    }

    private function cleanDirs($dir) {
        $prefixes=array(
            "",
            THUMB_PREFIX,
            MID_PREFIX
        );
        foreach ($prefixes as $prefix) {
            if (!empty($prefix)) {
                @rmdir(conf::get("path.images") . "/" . $dir . "/" . $prefix);
            }
        }
        @rmdir(conf::get("path.images") . "/" . $dir);
    }

    private function runCLI($cli) {
        $cli_array=explode(" ", trim($cli));
        $args=str_replace("_", " ", $cli_array);
        $admin=new user(1);
        $admin->lookup();
        $admin->lookupPerson();
        $admin->lookupPrefs();
        $cli=new cli($admin, 6, $args);
        $cli->run();
    }

    private function getFilenames() {
        return array(
            array ("PHOTO-01.JPG", "blue", "yellow", array(
                "DateTimeOriginal" => "2013-02-01 13:00:00")),
            array ("PHOTO-02.JPG", "blue", "yellow", array(
                "DateTimeOriginal" => "2013-02-02 13:00:00")),
            array ("PHOTO-03.JPG", "blue", "yellow", array(
                "DateTimeOriginal" => "2013-02-03 13:00:00"))
        );
    }
}
