<?php
/**
 * Unittests for XMP Data class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

use PHPUnit\Framework\TestCase;

use xmp\data as xmpData;

require_once "testSetup.php";

/**
 * Test XMP Data class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class xmpDataTest extends TestCase {


    public function testArrayAccess() {
        $data = array(
            "test"  => "testdata",
            "more"  => "test",
            "and"   => "more"
        );

        $xmpdata = new xmpData($data);

        $this->assertInstanceOf("xmp\\data", $xmpdata);
        $this->assertEquals("testdata", $xmpdata["test"]);

        $this->assertFalse(isset($xmpdata["unset"]));
        $this->assertTrue(isset($xmpdata["and"]));

        unset($xmpdata["and"]);
        $this->assertFalse(isset($xmpdata["and"]));
        $this->assertCount(2, $xmpdata);
        $xmpdata["new"] = "new data";

        $this->assertCount(3, $xmpdata);

    }

    public function testObjectAccess() {
        $data = array(
            "test"  => "testdata",
            "more"  => "test",
            "and"   => "more"
        );

        $xmpdata = new xmpData($data);
        $this->assertEquals("testdata", $xmpdata->test);
    }

    public function testIteratorAccess() {
        $data = array(
            "test"  => "testdata",
            "more"  => "test",
            "and"   => "more"
        );

        $xmpdata = new xmpData($data);

        $result = array();
        foreach ($xmpdata as $item) {
            $result[] = $item;
        }

        $expected = array("testdata", "test", "more");
        $this->assertEquals($expected, $result);
    }

    public function testGetRDFDescription() {
        $data = array(
            "x:xmpmeta" => array(
                "rdf:RDF" => array(
                    "rdf:Description" => array(
                        "exif:CameraMake"   => "Zoph",
                        "exif:CameraModel"  => "SuperZoom 2000",
                        "dc:subject" =>  array(
                            "XMP Test",
                            "XMP Data"
                        ),
                        "xmp:Rating"    => 10
                    )
                )
            )
        );

        $xmpdata = new xmpData($data);

        $this->assertEquals("Zoph", $xmpdata->getRDFDescription("exif:CameraMake"));

        $this->assertInstanceOf("xmp\\data", $xmpdata->getRDFDescription("dc:subject"));

        $expectedSubjects = array("XMP Test", "XMP Data");

        $subjects=$xmpdata->getRDFDescription("dc:subject");
        $actualSubjects = array();
        foreach ($subjects as $subject) {
            $actualSubjects[] = $subject;
        }

        $this->assertEquals($expectedSubjects, $actualSubjects);
    }
}
