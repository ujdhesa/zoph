<?php
/**
 * Phots View Download test (download feature)
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photos\view\download;
use photos\params;
use photo\collection;
use PHPUnit\Framework\TestCase;
use web\request;
use conf\conf;

/**
 * Test the photos view download class (download feature)
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photosViewDownloadTest extends TestCase {

    /**
     * Test creating a View
     */
    public function testCreate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);

        $download = new download($request, $params);
        $this->assertInstanceOf('\photos\view\download', $download);
        return $download;
    }

    /**
     * Test view
     *
     * @depends testCreate
     */
    public function testView(download $download) {
        $tpl=$download->view();
        $this->assertInstanceOf('\template\template', $tpl);
    }

    /**
     * Test create view with photos
     */
    public function testCreateWithPhotos() {
        $request=new request(array(
            "GET"   => array(
                "album_id" => 5,
                "_action"   => "download",
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);

        $collection = collection::createFromRequest($request);


        $download = new download($request, $params);
        $download->setPhotos($collection);
        $download->setDisplay($collection);

        $this->assertInstanceOf('\photos\view\download', $download);
        return $download;
    }

    /**
     * Test view with photos
     *
     * @depends testCreateWithPhotos
     */
    public function testViewWithPhotos(download $download) {
        $tpl=$download->view();
        $this->assertInstanceOf('\template\template', $tpl);

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        // Checking for a few strings in the template, to make sure it built correctly:
        $this->assertStringContainsString('<input type="hidden" name="album_id" value="5">', $act);
        $this->assertStringContainsString('You have requested the download of 3 photos, with a total size of 112.1KiB.', $act);
    }
}
