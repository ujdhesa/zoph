<?php
/**
 * Photo controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photo\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the photo controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photoControllerTest extends TestCase {

    /**
     * Test display action
     */
    public function testActionDisplay() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "photo_id"  => "1"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $this->assertEquals("display", $controller->getView());

    }

    /**
     * Test update action
     */
    public function testActionUpdate() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "update",
                "photo_id"  => "1",
                "title"     => "Updated by testActionUpdate"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $this->assertEquals("redirect", $controller->getView());

        $photo=new photo(1);
        $photo->lookup();

        $this->assertEquals("Updated by testActionUpdate", $photo->get("title"));
    }

}
