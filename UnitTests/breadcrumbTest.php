<?php
/**
 * A Unit Test for the breadcrumb object.
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use PHPUnit\Framework\TestCase;

/**
 * Test class for breadcrumbs.
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class breadcrumbTest extends TestCase {

    public function testAddCrumbs() {

        breadcrumb::add(new breadcrumb("test", "test.html"));
        breadcrumb::add(new breadcrumb("test2", "test2.html"));
        breadcrumb::add(new breadcrumb("test3", "test3.html?what=ever"));
        breadcrumb::add(new breadcrumb("test4", "test4.html"));

        $this->assertEquals("test4.html", breadcrumb::getLast()->getURL());
        $this->assertEquals("test4", breadcrumb::getLast()->getTitle());
        breadcrumb::eat();
        $this->assertEquals("test3.html?what=ever", breadcrumb::getLast()->getURL());
        $this->assertEquals("test3", breadcrumb::getLast()->getTitle());
        breadcrumb::eat(1);
        $this->assertEquals("test.html", breadcrumb::getLast()->getURL());
        $this->assertEquals("test", breadcrumb::getLast()->getTitle());
        breadcrumb::eat();
        $this->assertNull(breadcrumb::getLast());
    }

    /**
     * Test creating breadcrumbs from a request
     * This tests the 'normal behaviour'
     */
    public function testCreate() {
        $_SERVER = array(
                        "REQUEST_URI"   => "photos.php",
                        "PHP_SELF"      => "/photos.php"
                    );
        breadcrumb::create("photos", "display");
        $this->assertEquals("photos.php?_crumb=1", breadcrumb::getLast()->getURL());
        $this->assertEquals("photos", breadcrumb::getLast()->getTitle());
    }

    /**
     * Test creating breadcrumbs from a request
     * 'edit' is not an action that creates a breadcrumb, so the breadcrumb is null
     */
    public function testCreateEdit() {
        // delete breadcrumbs
        breadcrumb::eat(0);
        $_SERVER = array(
                        "REQUEST_URI"   => "photo.php?_action=edit",
                        "PHP_SELF"      => "/photo.php"
                    );
        breadcrumb::create("photos", "edit");
        $this->assertNull(breadcrumb::getLast());
    }

    /**
     * Test creating breadcrumbs from a request
     * with autoedit, the edit action that does create a breadcrumb
     */
    public function testCreateAutoEdit() {
        $user=user::getCurrent();
        $user->prefs->set("auto_edit", true);
        $_SERVER = array(
                        "REQUEST_URI"   => "photo.php?_action=display",
                        "PHP_SELF"      => "/photo.php"
                    );
        breadcrumb::create("photo", "edit");
        $this->assertEquals("photo.php?_crumb=1&amp;_action=display", breadcrumb::getLast()->getURL());
        $this->assertEquals("photo", breadcrumb::getLast()->getTitle());
    }
}
?>
