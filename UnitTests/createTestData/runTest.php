#!/usr/bin/php
<?php
/*
 * This test fills the database with testdata
 * You should normally not need to use this, as an XML-file with
 * testdata is included with the unittests. If you require to make
 * changes to that data, this script can be used.
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

define("TEST", true);
define("INSTANCE", "zophgit");
require_once "testData.php";
require_once "testImage.php";
require_once "createTest.php";
set_include_path(get_include_path() . PATH_SEPARATOR . getcwd() . "/php");
set_include_path(get_include_path() . PATH_SEPARATOR . getcwd() . "/php/classes");

require_once "autoload.inc.php";

require_once "include.inc.php";

use conf\conf;
$lang=new language("en");

user::setCurrent(new user(1));

$path=conf::set("path.images", getcwd() . "/.images");

createTestData::run();
