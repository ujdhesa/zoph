<?php
/**
 * Test Web Authentication
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use web\session;
use web\request;
use auth\web as auth;
use auth\validator;
use conf\conf;
use PHPUnit\Framework\TestCase;

/**
 * Test Web authentication
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class authWebTest extends TestCase {
    /** @var string keep original version so it can be restored */
    private $version;

    /**
     * Make sure that pending migrations do not block login
     */
    protected function setUp() : void {
        $this->version = conf::get("zoph.version");
        conf::set("zoph.version", "v99.99.99.999");
    }

    protected function tearDown() : void {
        conf::set("zoph.version", $this->version);
    }

    /**
     *
     */
    public function testAlreadyLoggedIn() {
        $session=new session();
        $brian = new user(2);
        $brian->lookup();
        $session["user"] = $brian;

        $auth = new auth($session, request::create());
        $user = $auth->getUser();

        $this->assertEquals(2, $user->getId());
        $this->assertEquals("brian", $user->getName());

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    public function testLogIn() {
        // users have no passwords set in the database, so first, we set a password
        $freddie = new user(5);
        $freddie->lookup();
        $freddie->set("password", validator::hashPassword("Quicksilver"));
        $freddie->update();
        $freddie->lookupPrefs();
        $freddie->prefs->set("language", "nl");
        $freddie->prefs->update();

        unset($freddie);
        user::unsetCurrent();

        $request=new request(
            array("POST" =>
                array(
                    "uname" => "freddie",
                    "pword" => "Quicksilver"
                )
            )
        );
        $session=new session();
        $auth = new auth($session, $request);

        $user = $auth->getUser();

        $this->assertEquals(5, $user->getId());
        $this->assertEquals("freddie", $user->getName());

        $lang=$auth->getLang();
        $this->assertInstanceOf("language", $lang);
        $this->assertEquals("nl", $lang->iso);

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    /**
     * Test failed logon
     */
    public function testLogInFailed() {
        // users have no passwords set in the database, so first, we set a password
        $freddie = new user(5);
        $freddie->set("password", validator::hashPassword("Quicksilver"));
        $freddie->update();

        unset($freddie);
        user::unsetCurrent();

        $request=new request(
            array("POST" =>
                array(
                    "uname" => "freddie",
                    "pword" => "SilverQuick"
                )
            )
        );
        $session=new session();
        $auth = new auth($session, $request);

        $user = $auth->getUser();

        $this->assertNull($user);
        $this->assertStringContainsString("PWDFAIL", $auth->getRedirect());

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    /**
     * Test redirect to logon screen
     */
    public function testRedirectToLogon() {
        $_SERVER["REQUEST_URI"]="album.php?album_id=1";
        user::unsetCurrent();

        $session=new session();
        $auth = new auth($session, request::create());

        $user = $auth->getUser();

        $this->assertNull($user);
        $this->assertStringContainsString("logon.php", $auth->getRedirect());
        $this->assertStringNotContainsString("../", $auth->getRedirect());
        $this->assertStringContainsString("album.php", $auth->getRedirect());

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    /**
     * Test redirect to logon screen (from service page)
     */
    public function testRedirectToLogonService() {
        $_SERVER["REQUEST_URI"]="/service/photoData.php";

        $session=new session();
        $auth = new auth($session, request::create());

        $user = $auth->getUser();

        $this->assertNull($user);
        $this->assertStringContainsString("../logon.php", $auth->getRedirect());
        $this->assertStringContainsString("photoData.php", $auth->getRedirect());

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    /**
     * Test redirect after logon
     */
    public function testRedirectAfterLogon() {
        // users have no passwords set in the database, so first, we set a password
        $freddie = new user(5);
        $freddie->set("password", validator::hashPassword("Quicksilver"));
        $freddie->update();

        unset($freddie);
        user::unsetCurrent();

        $request=new request(
            array("POST" =>
                array(
                    "uname" => "freddie",
                    "pword" => "Quicksilver",
                    "redirect" => "album.php?_action=display&album_id=1"
                )
            )
        );
        $session=new session();
        $auth = new auth($session, $request);

        $this->assertStringContainsString("album.php?_action=display", $auth->getRedirect());

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    /**
     * Test redirect after logon - with 'illegal' action
     */
    public function testRedirectAfterLogonIllegal() {
        // users have no passwords set in the database, so first, we set a password
        $freddie = new user(5);
        $freddie->set("password", validator::hashPassword("Quicksilver"));
        $freddie->update();

        unset($freddie);
        user::unsetCurrent();

        $request=new request(
            array("POST" =>
                array(
                    "uname" => "freddie",
                    "pword" => "Quicksilver",
                    "redirect" => "album.php?_action=confirm&album_id=1"
                )
            )
        );
        $session=new session();
        $auth = new auth($session, $request);

        $this->assertStringContainsString("album.php?_action=display", $auth->getRedirect());

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    /**
     * Test logon of anonymous user - for background image logon screen
     */
    public function testAnonymousLogonBG() {
        // This can only be called from image.php, faking that:
        if (!defined("IMAGE_PHP")) {
            define("IMAGE_PHP", true);
        }

        // Fake BG image request
        define("IMAGE_BG", true);

        $session=new session();
        $auth = new auth($session, request::create());

        $user = $auth->getUser();
        $this->assertInstanceOf("anonymousUser", $user);

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

    /**
     * Test logon of anonymous user - for image sharing
     */
    public function testAnonymousLogonSharing() {
        // This can only be called from image.php, faking that:
        if (!defined("IMAGE_PHP")) {
            define("IMAGE_PHP", true);
        }

        // Image sharing needs to be defined:
        conf::set("share.enable", true);

        $request=new request(
            array("POST" =>
                array(
                    "hash" => "f99cf2d2afb683cc6cfa3a9c06ece08e1c5ea360"
                )
            )
        );
        $session=new session();
        $auth = new auth($session, $request);

        $user = $auth->getUser();
        $this->assertInstanceOf("anonymousUser", $user);

        // log in admin user for remainder of tests
        user::setCurrent(new user(1));
    }

}
