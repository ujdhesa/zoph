<?php
/**
 * Upgrade controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use upgrade\controller;
use upgrade\migrations;
use PHPUnit\Framework\TestCase;
use web\request;


/**
 * Test the migration controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class upgradeControllerTest extends TestCase {
    /** @var string store version, so it can be restored afterwards */
    private static $version;
    /** @var migrations store migrations mock class */
    private static $migrations;

    public static function setUpBeforeClass(): void {
        require_once("upgradeHelper.php");
        static::$version=conf::get("zoph.version");
        conf::set("zoph.version", "v99.99.99.995");
        static::$migrations=new migrations("../UnitTests/migrations");
        static::$migrations->register(new upgrade\controllerTestMockup());
    }

    public static function tearDownAfterClass(): void {
        conf::set("zoph.version", static::$version)->update();
    }

    /**
     * Test action display
     */
    public function testActionDisplay() {
        $request=new request(array(
            "GET"   => array("_action" => "display"),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request, static::$migrations);
        $view = $controller->getView();
        $this->assertInstanceOf('\upgrade\view\display', $view);
        $this->assertStringContainsString("upgradeControllerTest", (string) $view->view());
        $this->assertEquals("upgradeControllerTest", $view->getTitle());
    }

    /**
     * Test action upgrade
     */
    public function testActionUpgrade() {
        $request=new request(array(
            "GET"   => array("_action" => "upgrade"),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request, static::$migrations);
        $view = $controller->getView();
        $this->assertInstanceOf('\upgrade\view\upgrade', $view);
        $this->assertStringContainsString("upgradeControllerTest", (string) $view->view());
        $this->assertEquals("Zoph Upgrade", $view->getTitle());
    }

    /**
     * Test action display - with no migrations to run
     */
    public function testActionDisplayNotFound() {
        $request=new request(array(
            "GET"   => array("_action" => "display"),
            "POST"  => array(),
            "SERVER" => array()
        ));
        conf::set("zoph.version", "v99.99.99.996");

        $controller = new controller($request, static::$migrations);
        $view = $controller->getView();
        $this->assertInstanceOf('\upgrade\view\notfound', $view);
        $this->assertStringContainsString("Nothing to do", (string) $view->view());
        $this->assertStringContainsString("No upgrade necessary", $view->getTitle());
    }

    /**
     * Test action upgrade - with no migrations to run
     */
    public function testActionUpgradeNotFound() {
        $request=new request(array(
            "GET"   => array("_action" => "upgrade"),
            "POST"  => array(),
            "SERVER" => array()
        ));
        conf::set("zoph.version", "v99.99.99.996");

        $controller = new controller($request, static::$migrations);
        $view = $controller->getView();
        $this->assertInstanceOf('\upgrade\view\notfound', $view);
    }
}

