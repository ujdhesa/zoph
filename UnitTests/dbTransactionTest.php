<?php
/**
 * Test the TRANSACTION queries
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use db\select;
use db\db;
use db\param;
use db\insert;
use db\delete;
use db\clause;
use db\transaction;
use db\exception as dbException;

use PHPUnit\Framework\TestCase;

/**
 * Test class that tests the database classes
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class dbTransactionTest extends TestCase {
    /**
     * Test running a transaction
     */
    public function testTransactionQuery() {

        $comments = new insert("comments");
        $comments->addParam(new param(":user_id", 1, PDO::PARAM_INT));
        $comments->addParam(new param(":subject", "test", PDO::PARAM_STR));
        $comments->addParam(new param(":comment", "test", PDO::PARAM_STR));
        $comments->addParam(new param(":ipaddr", "test.zoph.org", PDO::PARAM_STR));

        $phComments = new insert("photo_comments");
        $phComments->addParam(new param(":photo_id", 1, PDO::PARAM_INT));

        $tr = new transaction();
        $commentId = $tr->doQuery($comments);
        $phComments->addParam(new param(":comment_id", $commentId, PDO::PARAM_INT));
        $tr->doQuery($phComments);
        $tr->commit();

        $comments = new select("comments");
        $comments->where(new clause("comment_id = :commentid"));
        $comments->addParam(new param(":commentid", $commentId, PDO::PARAM_INT));

        $comment = new comment();
        $comment->lookupFromSQL($comments);

        $this->assertEquals("test", $comment->get("subject"));
        $this->assertEquals(1, $comment->getPhoto()->getId());

        $comment->delete();
    }

    /**
     * Test automatic rollback
     */
    public function testTransactionRollback() {

        $this->expectException(dbException::class);

        $comments = new insert("comments");
        $comments->addParam(new param(":user_id", 1, PDO::PARAM_INT));
        $comments->addParam(new param(":subject", "test", PDO::PARAM_STR));
        $comments->addParam(new param(":comment", "test", PDO::PARAM_STR));
        $comments->addParam(new param(":ipaddr", "test.zoph.org", PDO::PARAM_STR));

        $phComments = new insert("photo_comments_does_not_exist");
        $phComments->addParam(new param(":photo_id", 1, PDO::PARAM_INT));

        $tr = new transaction();
        $commentId = $tr->doQuery($comments);
        $phComments->addParam(new param(":comment_id", $commentId, PDO::PARAM_INT));
        $tr->doQuery($phComments);
        $tr->commit();

        // Even though the failure occured AFTER the comment was inserted
        // it should have been roll-backed... let's check...

        $comments = new select("comments");
        $comments->where(new clause("comment_id = :commentid"));
        $comments->addParam(new param(":commentid", $commentId, PDO::PARAM_INT));
        $comment = new comment();
        $comment->lookupFromSQL($comments);

        $this->assertNull($comment);
    }
}
