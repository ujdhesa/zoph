<?php
/**
 * Test migrations
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use db\clause;
use db\db;
use db\exception as dbException;
use db\insert;
use db\select;
use upgrade\migrations;
use upgrade\exception as migrationException;
use PHPUnit\Framework\TestCase;

/**
 * Test migrations
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class migrationTest extends TestCase {
    private static $migrations;

    public static function setUpBeforeClass(): void {
        static::$migrations=new migrations("../UnitTests/migrations");
    }
    public function testLoad() {
        $result=static::$migrations->check("v99.99.99.997");

        $this->assertEquals(true, $result);
    }

    public function testLoadAlreadyUpToDate() {
        $result=static::$migrations->check("v99.99.99.999");

        $this->assertEquals(false, $result);
    }

    public function testGetDesc() {
        $desc = array();
        $result=static::$migrations->check("v99.99.99.997");
        $this->assertEquals(true, $result);
        foreach (static::$migrations->get() as $migration) {
            $desc[]=$migration->getDesc();
        }

        $this->assertEquals(array("Test v99.99.99.998", "Test v99.99.99.999"), $desc);
    }

    public function testUp() {
        $result=static::$migrations->check("v99.99.99.997");
        $this->assertEquals(true, $result);

        foreach (static::$migrations->get() as $migration) {
            foreach ($migration->up() as $result) {
                $this->assertInstanceOf('upgrade\result', $result);
            }
        }

        // To test if the migration has worked, we'll insert some data in the
        // table that should have been created, and fetch it back out.
        $qry=new insert(array("test_v998"));
        $qry->addSet("test_char", "\"TEST\"");
        $qry->addSet("test", "\"TEST2\"");
        $id=$qry->execute();

        $qry=new select("test_v998");
        $qry->where(new clause("test_id=" . $id));

        $result = db::query($qry);
        $results=$result->fetchAll(PDO::FETCH_ASSOC);

        $exp=array(array(
            "test_id"   => $id,
            "test_char" => "TEST",
            "test"      => "TEST2"
        ));

        $this->assertEquals($exp, $results);
    }

    public function testDown() {
        $result=static::$migrations->check("v99.99.99.999");
        $this->assertEquals(false, $result);

        foreach (static::$migrations->getDown() as $migration) {
            foreach ($migration->down() as $result) {
                $this->assertInstanceOf('upgrade\result', $result);
            }
        }

        // To test if the migration has worked, we'll insert some data in the
        // table that should have been dropped, we should get an error.
        $this->expectException(dbException::class);
        $qry=new insert(array("test_v998"));
        $qry->addSet("test_char", "\"TEST\"");
        $qry->addSet("test", "\"TEST2\"");
        $id=$qry->execute();
    }

    /**
     * Test creating a migration
     * because the tests above use some prepared migrations, which are created before
     * running the actual test, the unit test coverage does not include some
     * parts of the migration creation. This is covered in these tests...
     */
    public function testCreateMigration() {
        $mig = new upgrade\zophtest998();
        $this->assertInstanceOf('upgrade\zophtest998', $mig);

        $this->assertEquals($mig->getDesc(), "Test v99.99.99.998");
        $this->assertEquals($mig->getTo(), "v99.99.99.998");
        $this->assertEquals($mig->getFrom(), "v99.99.99.997");
    }

    /**
     * Test adding a step with wrong 'direction'
     * steps should be either up or down...
     */
    public function testMigrationUnknownStep() {
        $mig = new upgrade\zophtest998();
        $this->assertInstanceOf('upgrade\zophtest998', $mig);

        $this->expectException(migrationException::class);
        $mig->addSidewaysStep();
    }

    /**
     * Test adding a step that causes an exception
     */
    public function testMigrationErroneousStep() {
        $mig = new upgrade\zophtest998();
        $this->assertInstanceOf('upgrade\zophtest998', $mig);

        foreach ($mig->up() as $result) {
            $this->assertInstanceOf('upgrade\result', $result);
        }

        $this->assertStringContainsString("class=\"result error\"", (string) $result);

    }
}

?>
