<?php
/**
 * Web "404 Not Found" View test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use PHPUnit\Framework\TestCase;
use web\request;
use web\view\notfound;

/**
 * Test view for "404 Not Found"
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class webViewNotfoundTest extends TestCase {

    /**
     * Test creating a View
     */
    public function testCreate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array(
                "SERVER_PROTOCOL" => "https"
            )
        ));

        $view = new notfound($request);
        $this->assertInstanceOf('\web\view\notfound', $view);
        return $view;
    }

    /**
     * Test view
     *
     * @depends testCreate
     */
    public function testView(notfound $view) {
        $tpl=$view->view();
        $this->assertInstanceOf('\template\template', $tpl);

        $headers=$view->getHeaders();

        $this->assertContains("https 404 Not Found", $headers);

        $response = $view->getResponseCode();
        $this->assertEquals(404, $response);
    }

    /**
     * Test create view customized error message
     */
    public function testCreateWithMsg() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array(
                "SERVER_PROTOCOL" => "https"
            )
        ));

        $view = new notfound($request);
        $this->assertInstanceOf('\web\view\notfound', $view);
        $view->setMessage("Test not found");
        return $view;
    }

    /**
     * Test view with customized error message
     *
     * @depends testCreateWithMsg
     */
    public function testViewWithMsg(notfound $view) {
        $tpl=$view->view();
        $this->assertInstanceOf('\template\template', $tpl);

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        // Checking for a few strings in the template, to make sure it built correctly:
        $this->assertStringContainsString('Test not found', $act);
    }
}
