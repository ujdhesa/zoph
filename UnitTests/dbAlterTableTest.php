<?php
/**
 * Test ALTER TABLE queries
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use db\alter;
use db\clause;
use db\column;
use db\db;
use db\delete;
use db\insert;
use db\param;
use db\select;

use db\exception\hasAlterationException as dbAlterTableHasAlterationException;
use db\exception\hasNoAlterationException as dbAlterTableHasNoAlterationException;
use db\exception\unsupportedEngineException as dbAlterTableUnsupportedEngineException;

use PHPUnit\Framework\TestCase;

/**
 * Test class that tests the database classes
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class dbAlterTableTest extends TestCase {

    public function testAlterQueryAddColumn() {
        $qry = new alter("test");
        $qry->addColumn((new column("column"))->varchar(30));
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test ADD COLUMN column VARCHAR(30);";
        $this->assertEquals($exp_sql, $sql);

        $qry = new alter("test");
        $qry->addColumn((new column("column"))->varchar(30))->first();
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test ADD COLUMN column VARCHAR(30) FIRST;";
        $this->assertEquals($exp_sql, $sql);

        $qry = new alter("test");
        $qry->addColumn((new column("column"))->varchar(30))->after("other");
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test ADD COLUMN column VARCHAR(30) AFTER other;";
        $this->assertEquals($exp_sql, $sql);
    }

    public function testAlterQueryModifyColumn() {
        $qry = new alter("test");
        $qry->modifyColumn((new column("column"))->varchar(30));
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test MODIFY COLUMN column VARCHAR(30);";
        $this->assertEquals($exp_sql, $sql);

        $qry = new alter("test");
        $qry->modifyColumn((new column("column"))->varchar(30))->first();
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test MODIFY COLUMN column VARCHAR(30) FIRST;";
        $this->assertEquals($exp_sql, $sql);

        $qry = new alter("test");
        $qry->modifyColumn((new column("column"))->varchar(30))->after("other");
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test MODIFY COLUMN column VARCHAR(30) AFTER other;";
        $this->assertEquals($exp_sql, $sql);
    }

    public function testAlterQueryChangeColumn() {
        $qry = new alter("test");
        $qry->changeColumn("column", (new column("new_name"))->varchar(30));
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test CHANGE COLUMN column new_name VARCHAR(30);";
        $this->assertEquals($exp_sql, $sql);

        $qry = new alter("test");
        $qry->changeColumn("column", (new column("new_name"))->varchar(30))->first();
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test CHANGE COLUMN column new_name VARCHAR(30) FIRST;";
        $this->assertEquals($exp_sql, $sql);

        $qry = new alter("test");
        $qry->changeColumn("column", (new column("new_name"))->varchar(30))->after("other");
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test CHANGE COLUMN column new_name VARCHAR(30) AFTER other;";
        $this->assertEquals($exp_sql, $sql);
    }

    public function testAlterQueryDropColumn() {
        $qry = new alter("test");
        $qry->dropColumn("column");
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test DROP COLUMN column;";
        $this->assertEquals($exp_sql, $sql);
    }

    public function testAlterQueryChangeEngine() {
        $qry = new alter("test");
        $qry->setEngine("InnoDB");
        $sql = (string) $qry;
        $exp_sql = "ALTER TABLE zoph_test ENGINE = \"InnoDB\";";
        $this->assertEquals($exp_sql, $sql);
    }

    public function testAlterQueryChangeEngineUnknownEngine() {
        $this->expectException(dbAlterTableUnsupportedEngineException::class);
        $qry = new alter("test");
        $qry->setEngine("ZophDB");
        $sql = (string) $qry;
    }

    public function testAlterQueryDuplicateAlterationEngine() {
        $this->expectException(dbAlterTableHasAlterationException::class);
        $qry = new alter("test");
        $qry->dropColumn("column");
        $qry->setEngine("MyISAM");
    }

    public function testAlterQueryNoAlteration() {
        $this->expectException(dbAlterTableHasNoAlterationException::class);
        $qry = new alter("test");
        $sql = (string) $qry;
    }

    public function testAlterQueryDuplicateAlterationAdd() {
        $this->expectException(dbAlterTableHasAlterationException::class);
        $qry = new alter("test");
        $qry->dropColumn("column");
        $qry->addColumn((new column("column"))->varchar(30));
    }

    public function testAlterQueryDuplicateAlterationiChange() {
        $this->expectException(dbAlterTableHasAlterationException::class);
        $qry = new alter("test");
        $qry->dropColumn("column");
        $qry->changeColumn("column", (new column("newname"))->varchar(30));
    }

    public function testAlterQueryDuplicateAlterationModify() {
        $this->expectException(dbAlterTableHasAlterationException::class);
        $qry = new alter("test");
        $qry->dropColumn("column");
        $qry->modifyColumn((new column("column"))->varchar(30));
    }

    public function testAlterQueryDuplicateAlterationDrop() {
        $this->expectException(dbAlterTableHasAlterationException::class);
        $qry = new alter("test");
        $qry->modifyColumn((new column("column"))->varchar(30));
        $qry->dropColumn("column");
    }
}
