<?php
/**
 * Unittests for rating class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";
use PHPUnit\Framework\TestCase;

/**
 * Test rating class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class ratingTest extends TestCase {

    /**
     * Test retrieving ratings from database
     * @dataProvider getRatings
     */
    public function testGetRating($photo_id, $user_id, $rating, $ip) {
        $user = new user($user_id);
        $user->lookup();
        user::setCurrent($user);

        $photo=new photo($photo_id);
        $photo->lookup();

        $objs = rating::getRatings($photo, $user);
        $this->assertCount(1, $objs);

        $obj=array_pop($objs);

        $this->assertInstanceOf("rating", $obj);

        $rate=$obj->get("rating");

        $this->assertEquals($rate, $rating);

        $ipaddr=$obj->get("ipaddress");
        $this->assertEquals($ip, $ipaddr);

        rating::getRatings($photo, $user)[0]->delete();
        user::setCurrent(new user(1));
    }

    /**
     * Test setting ratings and retrieving average ratings from database
     * @dataProvider getRatingsAverage
     */
    public function testSetRatingGetAverage(array $ratings, $avg) {
        global $_SERVER;
        $photo=new photo();
        $photo->insert();

        foreach ($ratings as $userId=>$rating) {
            $user = new user($userId);
            user::setCurrent($user);
            $_SERVER["REMOTE_ADDR"]=$user->getName() . ".zoph.org";
            $photo->rate($rating);
        }
        user::setCurrent(new user(1));

        $rate=rating::getAverage($photo);

        $this->assertEquals($avg, $rate);

        $photo->delete();
    }

    /**
     * Test multirating
     * @dataProvider getMultiRatings
     */
    public function testMultiRating($ratings, $user_id, $avg) {
        global $_SERVER;
        global $user;
        $user = new user($user_id);
        $user->lookup();

        $user->set("allow_multirating", true);
        $user->update();

        user::setCurrent($user);

        $photo=new photo();
        $photo->insert();

        foreach ($ratings as $nr=>$rating) {

            $_SERVER["REMOTE_ADDR"]=$user->getName() . $nr . ".zoph.org";

            $photo->rate($rating);
            $this->assertEquals($avg[$nr], rating::getAverage($photo));
        }

        user::setCurrent(new user(1));
        $photo->delete();
    }

    /**
     * Test rating when user is not allowed to rate
     */
    public function testDenyRating() {
        // Create a photo with a rating average of 5
        // this should stay this way!
        $photo=new photo();
        $photo->insert();

        $photo->rate(5);

        $user=new user(5);

        $user->set("allow_rating", false);
        $user->update();

        user::setCurrent($user);


        $this->assertEquals(5, $photo->getRating());

        $photo->rate(8);

        $this->assertEquals(5, $photo->getRating());

        // Reset changed values
        user::setCurrent(new user(1));
        $photo->delete();
        $user->set("allow_rating", true);
        $user->update();
    }

    /**
     * Test whether the functions still work when there are no ratings in the db
     */
    public function testNoRatings() {
        $user=new user(1);
        user::setCurrent($user);
        for ($r=1; $r<=19; $r++) {
            $rating=new rating($r);
            $rating->delete();
        }

        $ratings=rating::getGraphArray();
        $this->assertIsArray($ratings);

        $this->assertEquals($ratings[0]["count"], 12);

        for ($c=1; $c<sizeof($ratings); $c++) {
            $this->assertEquals($ratings[$c]["count"], 0);
        }

        // Restore original ratings
        require("UnitTests/createTestData/testData.php");
        $ratingdata=testData::getRatings();
        global $_SERVER;
        foreach ($ratingdata as $photoId=>$ratings) {
            $photo=new photo($photoId);
            foreach ($ratings as $userId=>$rating) {
                $user=new user($userId);
                $user->lookup();
                user::setCurrent($user);
                $_SERVER["REMOTE_ADDR"]=$user->getName() . ".zoph.org";
                $photo->rate($rating);
            }
        }
        user::setCurrent(new user(1));

    }

    /**
     * Test whether the functions still work when there are no photos in the db
     */
    /* With no automatic db-restore, this test turns out to be a little too destructive
    public function testNoPhotos() {
        $user=new user(1);
        user::setCurrent($user);

        for ($p=1; $p<=12; $p++) {
            $photo=new photo($p);
            $photo->delete();
        }
        $ratings=rating::getGraphArray();
        $this->assertIsArray($ratings);

        for ($c=0; $c<=10; $c++) {
            $this->assertEquals($ratings[$c]["count"], 0);
        }

    }*/

    /**
     * Test getting the array to build the graph on the
     * reports page.
     * @dataProvider getRatingArray
     * @todo At this moment only tests count, which is probably most important.
     */
    public function testGetGraphArray($user_id, $rating_array) {
        $user=new user($user_id);
        user::setCurrent($user);

        $ratings=rating::getGraphArray();

        $this->assertIsArray($ratings);
        foreach ($rating_array as $rating=>$count) {
            $this->assertEquals($count, $ratings[$rating]["count"]);
        }
        user::setCurrent(new user(1));
    }

    /**
     * Test getting the array to build the graph on the
     * user page.
     * @dataProvider getRatingArrayForUser
     * @todo At this moment only tests count, which is probably most important.
     */
    public function testGetGraphArrayForUser($user_id, $rating_array) {
        $user=new user($user_id);
        $ratings=rating::getGraphArrayForUser($user);
        $this->assertIsArray($ratings);
        foreach ($rating_array as $rating=>$count) {
            $this->assertEquals($count, $ratings[$rating]["count"]);


        }
        user::setCurrent(new user(1));
    }

    /**
     * Test the getDetails function
     * @dataProvider getDetailsArray
     */
    public function testGetDetails($photo_id, $det_array) {
        $photo=new photo($photo_id);
        $details=$photo->getRatingDetails();

        $this->assertInstanceOf("template\block", $details);

        $this->assertEquals($details->template, "templates/default/blocks/rating_details.tpl.php");
        $vars=$details->vars;

        $this->assertEquals($det_array[0], $vars["rating"]);

        // sort the array (by user_id), because we have no way of knowing
        // in which order the ratings will apear due to various changes
        // in other tests, where ratings are deleted and recreated or v.v.
        $userIds=array();
        foreach ($vars["ratings"] as $rating) {
            $userIds[]=$rating->get("user_id");
        }

        array_multisort($userIds, $vars["ratings"]);

        $this->assertIsArray($vars["ratings"]);
        foreach ($vars["ratings"] as $rating) {
            $this->assertInstanceOf("rating", $rating);

            $expected=array_shift($det_array[1]);

            $username=$rating->getUser()->getName();
            $this->assertEquals($expected[0], $username);
            $this->assertEquals($expected[1], $rating->get("rating"));
            $this->assertEquals($expected[2], $rating->get("ipaddress"));

        }



    }

    /**
     * Test deleting a rating
     * @dataProvider getRatingsToBeDeleted
     */
    public function testDeleteRating($photo_id, $user_id, $avg) {
        $photo=new photo($photo_id);
        $ratings=rating::getRatings($photo, new user($user_id));
        $rating=$ratings[0];
        $rating->lookup();
        $photo->lookup();
        $origRating = clone $rating;
        $origPhoto = clone $photo;

        $rating->delete();

        $new_avg=rating::getAverage($photo);
        $this->assertEquals($avg, $new_avg);

        $origRating->insert();
        $origPhoto->update();
    }

    public function getRatingsAverage() {
        return array(
            array([1=>5,2=>6,3=>7], 6),
            array([1=>2,3=>8,1=>7], 7.5),
            array([1=>8,2=>7,3=>7], 7.33),
            array([], null),
            array([1=>0], null),
            array([1=>null, 2=>8], 8),
        );
    }

    public function getMultiRatings() {
        // photo_id, rating, user_id, new avg
        return array(
            array(array(5, 1, 6, 8), 3, array(5,3,4,5)),
            array(array(7, 8, 8, 7), 6, array(7,7.5,7.67,7.5))
         );
    }

    public function getRatings() {
        // photo, user, rating, ip
        return array(
            array(1, 2, 8, "brian.zoph.org"),
            array(1, 5, 7, "freddie.zoph.org"),
            array(1, 7, 8, "roger.zoph.org"),
            array(1, 9, 7, "johnd.zoph.org"),
            array(2, 2, 5, "brian.zoph.org"),
            array(2, 5, 3, "freddie.zoph.org"),
            array(2, 7, 6, "roger.zoph.org"),
            array(2, 9, 3, "johnd.zoph.org"),
            array(3, 3, 5, "jimi.zoph.org"),
            array(4, 4, 7, "paul.zoph.org"),
            array(4, 8, 6, "johnl.zoph.org"),
            array(5, 6, 10, "phil.zoph.org"),
            array(5, 3, 9, "jimi.zoph.org"),
            array(6, 3, 7, "jimi.zoph.org"),
            array(6, 2, 5, "brian.zoph.org"),
            array(8, 4, 9, "paul.zoph.org"),
            array(8, 6, 3, "phil.zoph.org"),
            array(10, 6, 10, "phil.zoph.org"),
            array(10, 4, 1, "paul.zoph.org")
        );
    }

    public function getRatingArray() {
        // user, rating => count
        return array(
            array(1, array(
                0 => 4,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 1,
                5 => 1,
                6 => 3,
                7 => 1,
                8 => 1,
                9 => 0,
                10 => 1)
            ), array(3, array(
                0 => 1,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0,
                6 => 0,
                7 => 0,
                8 => 1,
                9 => 0,
                10 => 0)
            ),
            array(4, array(
                0 => 1,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 1,
                5 => 0,
                6 => 1,
                7 => 0,
                8 => 1,
                9 => 0,
                10 => 0)
            ));
    }

    public function getRatingArrayForUser() {
        // user, rating => count
        return array(
            array(2, array(
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 2,
                6 => 0,
                7 => 0,
                8 => 1,
                9 => 0,
                10 => 0)
            ), array(3, array(
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 1,
                6 => 0,
                7 => 1,
                8 => 0,
                9 => 1,
                10 => 0)
            ),
            array(1, array(
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0,
                6 => 0,
                7 => 0,
                8 => 0,
                9 => 0,
                10 => 0)
            ));
    }

    public function getDetailsArray() {
        // photo_id, array details
        return array(
            array(1, array(7.5,array(
                array("brian", 8, "brian.zoph.org"),
                array("freddie", 7, "freddie.zoph.org"),
                array("roger", 8, "roger.zoph.org"),
                array("johnd", 7, "johnd.zoph.org")))),
            array(3, array(5, array(
                array("jimi", 5, "jimi.zoph.org")))),
            array(7, array(0, array()))
        );
    }

    public function getRatingsToBeDeleted() {
        // photo_id, user_id, new_avg
        return array(
            array(1, 2, 7.33),
            array(2, 2, 4),
            array(3, 3, null),
            array(10, 6, 1)
        );
    }

}
