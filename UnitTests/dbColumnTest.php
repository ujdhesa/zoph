<?php
/**
 * Test db\column class
 *
 * also testing db\type
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use db\column;
use db\keyException as dbKeyException;
use PHPUnit\Framework\TestCase;

/**
 * Test db\column class
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class dbColumnTest extends TestCase {

    /**
     * Test column types
     * @param string type
     * @param string expected
     * @dataProvider getColumnTypes
     */
    public function testColumTypes($name, $type, $params, $expected) {
        $col = new column($name);
        call_user_func_array(array($col, $type), $params);

        $sql = (string) $col;

        $this->assertEquals($expected, $sql);
    }

    /**
     * Test UNSIGNED
     */
    public function testUnsigned() {
        $col = new column("test");
        $col->int()->unsigned();

        $sql = (string) $col;
        $this->assertEquals("test INT UNSIGNED", $sql);

        $col = new column("test");
        $col->float(5,2)->unsigned();

        $sql = (string) $col;
        $this->assertEquals("test FLOAT(5,2) UNSIGNED", $sql);

        // No such thing as unsigned chars...
        $col = new column("test");
        $col->char(10)->unsigned();

        $sql = (string) $col;
        $this->assertEquals("test CHAR(10)", $sql);
    }

    /**
     * Test AUTO_INCREMENT
     */
    public function testAutoIncrement() {
        $col = new column("test");
        $col->int()->autoIncrement();

        $sql = (string) $col;
        $this->assertEquals("test INT AUTO_INCREMENT", $sql);
        $this->assertEquals(true, $col->isKey());

        // Auto increment on FLOAT is mostly pointless, but MariaDB supports it...
        $col = new column("test");
        $col->float(5,2)->autoIncrement();

        $sql = (string) $col;
        $this->assertEquals("test FLOAT(5,2) AUTO_INCREMENT", $sql);
        $this->assertEquals(true, $col->isKey());

        // No such thing as auto_increment chars...
        $col = new column("test");
        $col->char(10)->autoIncrement();

        $sql = (string) $col;
        $this->assertEquals("test CHAR(10)", $sql);

        // Even with AUTO_INCREMENT unsupported on CHAR, it still gets marked as KEY
        $this->assertEquals(true, $col->isKey());
    }

    /**
     * Test NOT NULL
     */
    public function testNotNull() {
        $col = new column("test");
        $col->int()->notNull();

        $sql = (string) $col;
        $this->assertEquals("test INT NOT NULL", $sql);

        $col = new column("test");
        $col->float(5,2)->notNull();

        $sql = (string) $col;
        $this->assertEquals("test FLOAT(5,2) NOT NULL", $sql);

        $col = new column("test");
        $col->char(10)->notNull();

        $sql = (string) $col;
        $this->assertEquals("test CHAR(10) NOT NULL", $sql);
    }

    /**
     * Test Key
     */
    public function testKey() {
        $col = new column("test");
        $col->int()->setKey();
        $this->assertEquals(true, $col->isKey());

        # If a key is marked as both key and primary key, the latter takes precedence
        $col = new column("test");
        $col->int()->setKey()->setPrimaryKey();
        $this->assertEquals(false, $col->isKey());

        $col = new column("test");
        $col->int()->setKey();
        $this->assertEquals("test", $col->getKeyName());
        $this->assertEquals(0, $col->getKeyLength());

        $col = new column("test");
        $col->varchar(30)->setKey("column", 5);
        $this->assertEquals("column", $col->getKeyName());
        $this->assertEquals(5, $col->getKeyLength());
    }

    /**
     * Test getting key name when not a key
     */
    public function testKeyNameNoKey() {
        $col = new column("test");
        $this->expectException(dbKeyException::class);
        $name=$col->getKeyName();
    }

    /**
     * Test getting key length when not a key
     */
    public function testKeyLengthNoKey() {
        $col = new column("test");
        $this->expectException(dbKeyException::class);
        $len=$col->getKeyLength();
    }

    /**
     * Test Primary Key
     */
    public function testPrimaryKey() {
        $col = new column("test");
        $col->int()->setPrimaryKey();
        $this->assertEquals(true, $col->isPrimaryKey());
        $this->assertEquals(false, $col->isKey());
    }

    /**
     * Test DEFAULT
     */
    public function testDefault() {
        $col = new column("test");
        $col->datetime()->default("CURRENT_TIMESTAMP");
        $sql = (string) $col;
        $this->assertEquals("test DATETIME DEFAULT CURRENT_TIMESTAMP", $sql);

        $col = new column("test");
        $col->datetime()->default("NULL");
        $sql = (string) $col;
        $this->assertEquals("test DATETIME DEFAULT NULL", $sql);

        $col = new column("test");
        $col->datetime()->default("test");
        $sql = (string) $col;
        $this->assertEquals("test DATETIME DEFAULT \"test\"", $sql);
    }

    /**
     * Test ON UPDATE
     */
    public function testOnUPdate() {
        $col = new column("test");
        $col->datetime()->onUpdate("CURRENT_TIMESTAMP");
        $sql = (string) $col;
        $this->assertEquals("test DATETIME ON UPDATE CURRENT_TIMESTAMP", $sql);

        // Not really sure if anything other than CURRENT_TIMESTAMP is valid
        $col = new column("test");
        $col->datetime()->onUpdate("NULL");
        $sql = (string) $col;
        $this->assertEquals("test DATETIME ON UPDATE NULL", $sql);

        $col = new column("test");
        $col->datetime()->onUpdate("test");
        $sql = (string) $col;
        $this->assertEquals("test DATETIME ON UPDATE \"test\"", $sql);
    }

    public function getColumnTypes() {
        return array(
            array("test_tinyint",   "tinyint",  array(),    "test_tinyint TINYINT"),
            array("test_smallint",  "smallint", array(),    "test_smallint SMALLINT"),
            array("test_int",       "int",      array(),    "test_int INT"),
            array("test_float",     "float",    array(3,5), "test_float FLOAT(3,5)"),
            array("test_char",      "char",     array(10),  "test_char CHAR(10)"),
            array("test_varchar",   "varchar",  array(20),  "test_varchar VARCHAR(20)"),
            array("test_text",      "text",     array(),    "test_text TEXT"),
            array("test_blob",      "blob",     array(),    "test_blob BLOB"),
            array("test_datetime",  "datetime", array(),    "test_datetime DATETIME"),
            array("test_timestamp", "timestamp",array(),    "test_timestamp TIMESTAMP"),
            array("test_enum",      "enum",     array("red", "green", "blue"), "test_enum ENUM(\"red\", \"green\", \"blue\")")
        );
    }
}
