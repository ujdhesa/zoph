# Using the CLI tool to add or modify users #
As of Zoph v0.9.17, it is possible to add users via the CLI. This could for example help with deploying Zoph via a script or integrate it with other systems. It could also provide help if you have locked yourself out of Zoph and need a password reset.

This document only describes the usage of the CLI tool for user management, a detailed overview of all the options of the CLI can be found in [The Zoph CLI tool](CLI.md).

Just like the other uses of Zoph, you will need a valid [zoph.ini](CONFIGURATION.md#contents-of-zophini) file to work with the CLI client and you need specify the [--instance](CLI.md#--instance) CLI option if you have multiple Zoph installs on the same system.

To use the CLI tool for user management, specify `--user` or `-U` as the first option, followed by `add`, `delete`, `update` or `show`. You cannot combine user management and import functions.

### Adding a user ###
````
zoph --user add --username <username> --password <password>
````
You should keep in mind that this will store the password in your shell's history file.
Both username and password are required.

To make the user an admin user, add `--admin`
````
zoph --user add --username <username> --password <password> --admin
````

### Delete a user ###

````
zoph --user delete --username <username>
Are you sure you want to delete user <username>? [y/N] y
````
or
````
zoph --user delete --userid <userid>
Are you sure you want to delete user <username>? [y/N] y
````
Either username or userid are required.

By specifying `-f` you can skip the "Are you sure" question. This is required when running from a script.
````
zoph --user delete --username <username> -f
````

### Modify a user ### 
````
zoph --user update --username <username>
````
or
````
zoph --user update --userid <userid>
````
Either username or userid are required.

You can specify `--password` to change the user's password, `--access` to give a user a certain access right, `--no-access` to revoke a right, `--admin` to make a user admin, `--no-admin` to change an admin into a user, `--group` to add a user to a group and `--no-group` to remove a user from a group. 
Use `--user show` to find the different possible access rights.

````
zoph --user update --username <username> --password <password>
zoph --user update --username <username> --admin
zoph --user update --username <username> --no-admin
zoph --user update --username <username> --access <right>
zoph --user update --username <username> --no-access <right>
zoph --user update --username <username> --group <group>
zoph --user update --username <username> --no-group <group>
````
Various changes can be combined in one command. For example:

````
zoph --user update --username jeroen --password secret --no-admin --access download --access browse_places --no-access browse_tracks --group zoph_users --no-group windows_users
````

### Show a user ### 
````
zoph --user show --username <username>
````
or
````
zoph --user show --userid <userid>
````
Either username or userid are required.

Shows information about the user requested.

````
zoph --user show --userid 1
User ID:    1
Username:   admin
User class: Admin
Groups:
Last login: 2021-04-05 15:53:39

Access rights:
    view_all_photos     : 1
    delete_photos       : 1
    browse_people       : 1
    browse_places       : 1
    browse_tracks       : 1
    edit_organizers     : 1
    detailed_people     : 1
    see_hidden_circles  : 1
    detailed_places     : 1
    import              : 1
    download            : 1
    leave_comments      : 1
    allow_rating        : 1
    allow_multirating   : 0
    allow_share         : 0

````
Note that due to technical reasons, the access rights for an admin user may show "0" for some rights even though the admin user has these rights implicitly.
