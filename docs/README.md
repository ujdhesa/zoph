# Zoph Documentation #
http://www.zoph.org

1. [Requirements](REQUIREMENTS.md)
2. [Installation guide](INSTALL.md) 
3. [Configuration Instructions](CONFIGURATION.md)
4. [Using the web interface](WEBINTERFACE.md)
5. [Importing photos through the web interface](IMPORT-WEB.md)
6. [Importing photos using the CLI interface](IMPORT-CLI.md)
7. [XMP Support](XMP.md)
8. [Using the CLI tool](CLI.md)
9. [Upgrade Instructions](UPGRADE.md)
10. [Changelog](CHANGELOG.md)
