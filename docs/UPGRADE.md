# UPGRADE INSTRUCTIONS #
## Zoph 0.9.20 ##
A small change was made to the CLI client, if you use it, you should update the version in your `$PATH`.

As of this release, Zoph requires PHP 8.0 or later. 8.1 should work, but is not yet tested. PHP 7.4 or older are no longer supported.

## Zoph 0.9.18 and up ##
As of Zoph 0.9.18, database upgrades are included in the release. Simply copy the contents of the `php` directory into the webroot. Then log in to Zoph with an admin user and the GUI will guide you through the upgrade process. If you are running an older version of Zoph, please make sure you follow the upgrade instructions to v0.9.17 (below) prior to logging in to Zoph and performning the upgrade from there.

```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`.
```
    cp cli/zoph /usr/bin
```

## Zoph 0.9.15 or 0.9.16 to 0.9.17 ##
* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.15. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot. 
```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`.
```
    cp cli/zoph /usr/bin
```
### Database changes ###
Execute zoph-update-0.9.17.sql:
```
     mysql -u zoph_admin -p zoph < sql/zoph_update-0.9.17.sql
```
Changes this script makes:

* Add a `imported` field to `zoph_photos`, so Zoph stores the date and time a photo was imported
* Copy the date and time in `timestamp` to `imported`, so photos imported prior to this release have some data stored in here

## Zoph 0.9.13 or 0.9.14 to 0.9.15 or 0.9.16 ##
* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.14. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot. 
```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`. The CLI tool has undergone some changes in v0.9.13, so if you copied it previously, you should copy the new version in place.
```
    cp cli/zoph /usr/bin
```

### Database changes ###
* There are no database changes in v0.9.13 or 0.9.14.

## Zoph 0.9.12 to 0.9.13 ##

* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.12. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot. 
```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`
```
    cp cli/zoph /usr/bin
```

### Database changes ###
Execute zoph-update-0.9.13.sql:
```
     mysql -u zoph_admin -p zoph < sql/zoph_update-0.9.13.sql
```
Changes this script makes:

* Add a `row` field to `zoph_photo_people`, allowing you to specify multiple rows of people on a photo. 

## Zoph 0.9.11 to 0.9.12 ##

* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.11. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot. 
```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`
```
    cp cli/zoph /usr/bin
```

### Database changes ###
Execute zoph-update-0.9.12.sql:
```
     mysql -u zoph_admin -p zoph < sql/zoph_update-0.9.12.sql
```
Changes this script makes:

* Resize fields that store IP addresses to give them enough space to store IPv6 addresses

## Zoph 0.9.6-0.9.9 to 0.9.7-0.9.11 ##

* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.6. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot. 
```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`
```
    cp cli/zoph /usr/bin
```

### Database changes ###
* There are no database changes in v0.9.7, v0.9.8, v0.9.9, v0.9.10 and v0.9.11

### Deprecated configuration ###
I will be removing the `ssl.force`, `url.http` and `url.https` configuration option in v0.9.9. As of v0.9.8, Zoph will show a warning. If your setup requires setting these functions, please comment on [issue#100](http://github.com/jeroenrnl/zoph/issues/100)

![screenshot of the deprecated options](img/zoph-ssl-config.png)

## Zoph 0.9.5 to 0.9.6 ##
* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.5. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot.
```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`
```
    cp cli/zoph /usr/bin
```
### Database changes ###
Execute zoph-update-0.9.6.sql:
```
     mysql -u zoph_admin -p zoph < sql/zoph_update-0.9.6.sql
```
Changes this script makes:

* Give several timestamp fields a default value, because as of MySQL 5.7.4 "0000-00-00 00:00:00" is no longer a valid date in the default configuration (this was reverted in MySQL 5.7.8)
* Set `person_id` in the `zoph_users` table to have a default of `NULL` instead of `"0"`
* Drop the `column contact_type` from `zoph_places`, as it was not used as of Zoph 0.3.3 (!)

## Zoph 0.9.4 to 0.9.5 ##
* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.4. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot.
```
    cp -a php/* /var/www/html/zoph
```
If you use the CLI client, you should copy it to a path that's in your `$PATH`
```
    cp cli/zoph /usr/bin
```
### Database changes ###
There are no database changes for 0.9.5

## Zoph 0.9.3 to 0.9.4 ##
* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.3. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot.
```
    cp -a php/* /var/www/html/zoph
```

### Database changes ###
* Execute zoph-update-0.9.4.sql:
```
    mysql -u zoph_admin -p zoph < sql/zoph_update-0.9.4.sql
```
Changes this script makes:

* Add a field that stores whether or not new subalbums should be automatically granted permission
* Add new colour schemes

## Zoph 0.9.2 to 0.9.3 ##
* If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.2. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.

### Copy files ###

Copy the contents of the php directory, including all subdirs, into your webroot.
```
    cp -a php/* /var/www/html/zoph
```
## Database changes ##
* Execute zoph-update-0.9.3.sql:
```
    mysql -u zoph_admin -p zoph < sql/zoph_update-0.9.3.sql
```
Changes this script makes:

* Resize the `password` field to allow store bigger hashes
* Add fields to the `user` table to allow for new access rights
* Add `created_by` fields to the albums, categories, places, people and circles tables

## Zoph 0.9.1 to 0.9.2 ##
* *If you want to upgrade from an older version, first follow the instructions to upgrade to 0.9.1. It is not necessary to install older versions first, you can just install the current version and follow the upgrade instructions below.*

### Copy files ###
Copy the contents of the `php` directory, including all subdirs, into your webroot. 
```
     cp -a php/* /var/www/html/zoph
```
### Database changes ###
* Execute zoph-update-0.9.2.sql:
```
    mysql -u zoph_admin -p zoph < sql/zoph_update-0.9.2.sql
```
Changes this script makes:

* Add previously missing 'random' sortorder to preferences
* Resize Last IP address field so IPv6 addresses can be stored
* Database changes for 'circles' feature
* Create a VIEW on the database to speed up queries for non-admin users
